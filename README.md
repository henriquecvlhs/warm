WARM: WSN Application development and Resource Management.

WARM is a framework that makes use of the Software Defined Networking (SDN) paradigm with the objective of easing the application development and resource management of Wireless Sensor Networks (WSN).
