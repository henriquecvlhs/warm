###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import time
import threading

import network_serial
import task_mapping
import network_monitor
import API
from packets import *
import DAO
from dictionaries import Dict


class testThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        network_serial.loop()


def populateTestDatabase():
    # Devices
    for key, value in Dict.device_name.iteritems():
        device = DAO.Device(device_id=key, name=value, description=Dict.device_description.get(key,""))
        DAO.dao.add(device)

    # OSs
    for key, value in Dict.os_name.iteritems():
        os = DAO.Operating_System(os_id=key, name=value, description=Dict.os_description.get(key, ""))
        DAO.dao.add(os)

    # Task types
    for key, value in Dict.task_type_description.iteritems():
        task_type = DAO.Task_Type(task_type_id=key, description=value)
        DAO.dao.add(task_type)

    # Data Type
    for key, value in Dict.data_type_description.iteritems():
        data_type = DAO.Data_Type(data_type_id=key, description =value)
        DAO.dao.add(data_type)

    # Data Length
    for key, value in Dict.data_length_description.iteritems():
        data_length = DAO.Data_Length(data_length_id=key, description =value)
        DAO.dao.add(data_length)

    DAO.dao.commit()


def main():
    # Expects parameter from command line, usually:
    # python network_serial_test.py network@localhost:60001
    DAO.startDAO(True)
    populateTestDatabase()
    network_serial.debug_mode = True
    task_mapping.testing_mode = True
    network_monitor.testing_mode = True

    network_serial.initSerial("network@localhost:60003")

    loopThread = testThread()
    # The program will not be stopped by Ctrl C, the solution is to use:
    # Ctrl Z (suspend) kill %%
    loopThread.start()

    while True:
        # Sleep to leave time to receive packets from nodes
        time.sleep(60)

        with DAO.daoLock:
            print "\n>>>Setting periodic task..."
            print task_mapping.setPeriodicTask(1, 1, "temp", 10, 3600)
            print "<<<Periodic task set\n"

            print "\n>>>Setting instant task..."
            print task_mapping.setInstantTask(2, 1, "avg", "temp", 10)
            print "<<<Instant task set\n"

            print "\n>>>Setting instant task..."
            print task_mapping.setInstantTask(2, 1, "avg2", "temp2", 10)
            print "<<<Instant task set\n"

        with DAO.daoLock:
            try:
                nodes = DAO.dao.query(DAO.Sensor_Node).all()
                nodesjson = API.GetNodesJSON(nodes)
                for node in nodesjson:
                    print node
            except AttributeError:
                print "Some task is not properly initialized"


if __name__=="__main__":
    main()
