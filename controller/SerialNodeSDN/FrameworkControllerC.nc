/**
 * @file FrameworkControllerC.nc
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AM.h"
#include "printf.h"
#include "protocol.h"
#include "protocolPrint.h"
#include "ControllerMote.h"

module FrameworkControllerC {
  uses {
    // Interfaces for initialization:
    interface Boot;

    // Interfaces for communication, multihop:
    interface SDNSend as Send;
    interface SDNReceive as Receive;

    // Interfaces for serial communication
    interface AMSend  as SerialSender;
    interface Receive as SerialReceiver;
    interface Packet as SerialPacket;
    interface SplitControl as SerialControl;

    // Miscellaneous:
    interface Leds;
  }
}

implementation {
  message_t packet;
  uint8_t buf[130];

  event void Boot.booted() {
    //printf("Boot!\n");
    //printfflush();

    call SerialControl.start();
  }

  event void Receive.receive(uint16_t originId, uint16_t flowId, void *payload,
      uint8_t len) {
    uint8_t i;
    warm_serial_msg *msg;
    msg = (warm_serial_msg*) call SerialPacket.getPayload(&packet, sizeof(warm_serial_msg));
    msg->nodeId = originId;
    msg->flowId = flowId;
    msg->len = len;
    for (i = 0; i < len && i < 90; i++) {
      msg->data[i] = ((uint8_t *) payload)[i];
    }

    if (call SerialSender.send(AM_WARM_SERIAL_MSG, &packet,
                              sizeof(warm_serial_msg)) == SUCCESS) {
      //printNodeCharacteristicsDescription(msg->data);
    }
  }

  event message_t* SerialReceiver.receive(message_t *bufPtr, void *payload, uint8_t len) {
    warm_serial_msg *msg = (warm_serial_msg*) payload;
    uint8_t i;
    //printf("\nLen: %u, Nodeid: %u, Flowid: %u, Msglen: %u", len, msg->nodeId, msg->flowId, msg->len);
    for (i = 0; i < msg->len; i++) {
      buf[i] = msg->data[i];
    }
    //printTaskDescriptionRequest(snd);
    //printfflush();
    call Send.send(msg->flowId, buf, msg->len);

    return bufPtr;
  }

  event void SerialSender.sendDone(message_t *butPtr, error_t error) {}

  event void SerialControl.startDone(error_t err) {}

  event void SerialControl.stopDone(error_t err) {}
}
