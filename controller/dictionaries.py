###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

class Dict:
    """
    Description Dictionaries.
    """

    #################################################
    #  Always add these dictionaries to the database  #
    #################################################

    device_name = {1: 'Unknown', 2: 'Arduino', 3: 'TelosB'}
    device_description = {1: 'Unknown', 2: 'Arduino description', 3: 'TelosB description'}

    os_name = {1: 'Unknown', 2: 'TinyOS'}
    os_description = {1: 'Unknown', 2: 'TinyOS description'}

    task_type_description = {1: 'instant', 2: 'periodic'}

    data_type_description = {1: 'floating point', 2: 'fixed point', 3: 'integer', 4: 'unsigned integer', 5: 'boolean', 6: 'bit array'}
    data_length_description = {1: '8 bits', 2: '16 bits', 3: '32 bits', 4: '64 bits'}

    #################################################
    #   Only add to the database when it is needed  #
    #################################################

    #key is <task_id>
    task_description = {1: 'Temperature Sensor', 2: 'Average', 3: 'Button', 4: 'LED', 5: 'PrintData'}

    #key is <task_id>-<task_parameter_id>
    parameter_description = {'1-0': 'Result', '2-0': 'Input data reference', '2-1': 'Result', \
                             '3-0': 'Result', '4-0': 'Input data reference', '4-1': 'Result', \
                             '5-0': 'Input data reference', '5-1': 'Result'}
