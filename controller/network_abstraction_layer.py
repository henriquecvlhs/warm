###############################################################################
# Archive that contains functions and methods related to the controller network
# abstraction layer.
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import socket
import threading
from errors import *

defaultHost = "127.0.0.1"
defaultPort = 60005

# Socket used for communicating with the controller
# TODO: Ensure the socket is valid before using it
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Lock to ensure only one thread using the socket at a time
sockLock = threading.Lock()


def connectToController(host=None, port=None):
    """
    Function to connect the socket to the controller.

    Args:
        host(string): the address of the SDN controller.
        port(int): port the SDN controller is listening at.
    """

    ret = Err.ERR_OK
    sockLock.acquire()
    if host is None:
        host = defaultHost
    if port is None:
        port = defaultPort
    try:
        sock.connect((host, port))
    except Exception:
        # Failed to connect to controller
        ret = Err.ERR_FAILED_TO_CONNECT_TO_CONTROLLER
    sockLock.release()

    return ret

def createNewFlow(flowId, flow):
    """
    Function to send a new flow request to the SDN controller.

    Args:
        flowId (int): The flowId to be assigned to the new flow.
        flow (list[int]): List with the node path of the flow, the list must contain an ordered sequence of nodes from source to sink of the flow.
    """

    # TinySDN expects the flow creation request to be in the following format:
    # SET flow <flowId> list <edge list>
    # Where <edge list> is a list of edges obeying the format:
    # <edge list>: (srcNodeId1, dstNodeId1), (srcNodeId2, dstNodeId2)
    ret = Err.ERR_OK

    # Create the edge list string from the node list
    edgeList = ""
    for i in xrange(len(flow) - 1):
        if i > 0:
            edgeList = edgeList + ", "
        edgeList = edgeList + "(%d, %d)" % (flow[i], flow[i+1])

    commStr = "SET flow %d list %s\n" % (flowId, edgeList)

    # Acquire lock to use the socket
    sockLock.acquire()
    # Send the message
    try:
        sock.sendall(commStr)
        reply = sock.recv(2048)
        if reply != "Command successful\n":
            ret = Err.ERR_INVALID_CONTROLLER_MESSAGE
    except Exception:
        ret = Err.ERR_FAILED_TO_SEND_MESSAGE_TO_CONTROLLER
    # Release the lock
    sockLock.release()

    return ret

def requestNetworkInformation():
    """
    Function to request the network information to the SDN controller.

    Returns:
      (error code, edges)
    """

    # TinySDN expects the network information request to be on the
    # format: "GET graph"
    ret = Err.ERR_OK

    commStr = "GET graph\n"

    # Acquire lock to use the socket
    sockLock.acquire()
    # Send the message
    try:
        sock.sendall(commStr)
        reply = sock.recv(2048)
        if reply == "Invalid command\n":
            ret = Err.ERR_INVALID_CONTROLLER_MESSAGE
    except Exception:
        ret = Err.ERR_FAILED_TO_SEND_MESSAGE_TO_CONTROLLER
    # Release the lock
    sockLock.release()

    edges = []
    if ret == Err.ERR_OK:
        reply = reply.strip('\n')
        parts = reply.split(',')
        for e in parts:
            e1, e2 = e.split(":")
            e1, e2 = e1.strip("( "), e2.strip(") ")
            edges.append((int(e1), int(e2)))

    return (ret, edges)
