sphinx-apidoc ../ -o ./
make html
find . -type f -name '*.rst' -not -name 'index.rst' | xargs rm

