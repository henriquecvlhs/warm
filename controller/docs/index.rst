.. WARM documentation master file, created by
   sphinx-quickstart on Fri Nov  6 18:36:07 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WARM's documentation!
================================

Contents:

WARM was developed at Polytechnic School of University of São Paulo by Andre Hahn Pereira, Henrique Carvalho Silva and Yuka Kyushima Solano. The advisors of the project were Cíntia Borges Margi and Bruno Trevizan de Oliveira.

WARM is a framework for Wireless Sensor Networks (WSN) aiming at reducing the complexity of programming such networks.

Below you will find the documentation of the functions and classes used at the controller of the framework.

.. toctree::
   :maxdepth: 2

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

