###############################################################################
# Archive contains functions and methods related to the controller task mapping.
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from errors import *
from packets import *
import network_monitor
import network_abstraction_layer
import DAO
import network_serial

# TODO: Do a proper way to deal with testing
testing_mode = True


def getPath(source, dest):
    """
    Gets the shortest path from source to dest and returns it as a list.

    Args:
        source(int): the source of the path
        dest(int): the destination of the path
    """

    # Get the edges between nodes
    edges = DAO.dao.query(DAO.Node_Connection).all()
    # TODO: Sort the edges in ascending order by node_id

    visitedNodes = set()
    parentNode = dict()
    nodeQueue = [source]
    parentNode[source] = -1
    
    # Using a BFS for the path
    while len(nodeQueue) > 0:
        # TODO: Use deque instead of a list here, it is far more efficient
        curNode = nodeQueue.pop(0)
        visitedNodes.add(curNode)

        # Reached destination, stop searching
        if curNode == dest:
            break
        
        # TODO: Extremely inefficient! Looping through the whole graph!
        for edge in edges:
            if edge.node_id != curNode:
                continue

            if edge.neighbour_node_id not in parentNode:
                parentNode[edge.neighbour_node_id] = curNode
                nodeQueue.append(edge.neighbour_node_id)

    # Check if it is possible to get from source to destination
    if dest not in parentNode:
        return []
    
    # Generate the path
    path = []
    curNode = dest
    while curNode != -1:
        path.append(curNode)
        curNode = parentNode[curNode]
    path.reverse()

    return path


def updateNetworkTopology():
    """
    Updates the network topology with information from SDN controller.
    """
    if testing_mode == False:
        ret, edges = network_abstraction_layer.requestNetworkInformation()
    else:
        ret = Err.ERR_OK

        edges = [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7)]

    if ret != Err.ERR_OK:
        return

    newEdges = []
    outdatedEdges = []
    allEdges = DAO.dao.query(DAO.Node_Connection).all()

    # Check for edges which are in the db but not in the received graph
    # TODO: Update info about new edges at this loop as well
    for edge in allEdges:
        outdated = True
        for edge2 in edges:
            if edge.node_id == edge2[0] and edge.neighbour_node_id == edge2[1]:
                outdated = False
                break
        if outdated:
            outdatedEdges.append(edge)

    # The edges are represented by tuples (n1, n2)
    for edge in edges:
        query = (DAO.dao.query(DAO.Node_Connection)
                .filter_by(node_id=edge[0], neighbour_node_id=edge[1])
                .first())
        if query is None:
            newEdges.append(DAO.Node_Connection(node_id=edge[0],
                neighbour_node_id=edge[1]))
    
    for outdatedEdge in outdatedEdges:
        DAO.dao.delete(outdatedEdge)

    for newEdge in newEdges:
        DAO.dao.add(newEdge)

    DAO.dao.commit()


def defineFlow(source, dest):
    """
    Defines the flow from source to destination and returns a tuple,
    (flowId, path).

    Args:
        source(int): the source of the flow
        dest(int): the destination of the flow
    """

    # Get an updated network topology from the SDN controller
    if testing_mode == False:
        updateNetworkTopology()

    # New flow number
    maxFlowVal = 0
    # TODO: Do this in a non dumb way
    # TODO: Update to consider flows of the other table as well
    taskRelationships = DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship).all()
    for relation in taskRelationships:
        # Ignore 65535, as it is the drop flow
        if relation.sdn_flow_id != 65535:
            maxFlowVal = max(maxFlowVal, relation.sdn_flow_id)

    controlFlows = DAO.dao.query(DAO.Control_Flows).all()
    for flow in controlFlows:
        # Ignore 65535, as it is the drop flow
        if flow.sdn_flow_id != 65535:
            maxFlowVal = max(maxFlowVal, flow.sdn_flow_id)
    
    flowId = maxFlowVal + 1
    path = getPath(source, dest)
    return (flowId, path)


def setInstantTask(tid, nid, reference, dataSource, quantity, address=None):
    """
    Sets an instant task. Returns the scheduled id.

    Parameters:
        tid(int): the task id to set
        nid(int): the id of the node where the task is to be executed
        reference(string):  the reference id for the task
        dataSource(string): reference of the source data
        quantity(int): the amount of expected packets of the dataSource expected before the operation
        address(string): if defined, the data will be sent to this defined address
    """
    if testing_mode:
        print "tid: %d, nid: %d, reference: %s, dataSource: %s, quantity: %d, address:%s" % (tid, nid, reference, dataSource, quantity, address)
    # Ensure the node exists and that it is capable of executing tid
    node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=nid).first()
    if node is None:
        return Err.ERR_NODE_DOES_NOT_EXIST, None
    task = DAO.dao.query(DAO.Task).filter_by(task_id=tid).first()
    if task is None:
        return Err.ERR_TASK_DOES_NOT_EXIST, None
    if task.task_type_id != 1:
        return Err.ERR_TASK_NOT_INSTANT, None
    relation = (DAO.dao.query(DAO.Sensor_Node_Task_Relationship)
                    .filter_by(node_id=nid, task_id=tid).first())

    if relation is None:
        # Node can't execute task
        return Err.ERR_NODE_DOES_NOT_HAVE_TASK, None
    if (relation.currently_scheduled_task_instances is not None and
            relation.currently_scheduled_task_instances >= relation.max_scheduled_task_instances):
        # Already at maximum capacity for the given task
        return Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED, None

    # TODO: Filter triggers

    # Fetch data of tasks which generate dataSource data
    dataSourceNodes = []
    # TODO: Fix this, probably can do this with a single query
    dataSourceReference = (DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship)
            .filter_by(reference=dataSource)
            .all())
    # Check for parameters with input=False
    for ref in dataSourceReference:
        parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id, task_id=ref.task_id)
                .first())
        if parameter.input == False:
            if ref.sdn_flow_id != 65535:
                return Err.ERR_DATASOURCE_ALREADY_IN_USE, None
            sourceNodeId = ref.task_scheduling.node_id
            dataSourceNodes.append(sourceNodeId)
        else:
            # Another task already uses data source as input, cannot create
            # the task.
            return Err.ERR_DATASOURCE_ALREADY_IN_USE, None

    flowid_input = [65535] * len(dataSourceNodes)
    # TODO: Fix this, creating a different flow for each source, should be a
    # single flow for all of them
    # Define flows for the nodes
    indice = 0
    if len(dataSourceNodes) > 0:
        for node in dataSourceNodes:
            # Define the flows
            flowid_input[indice], path = defineFlow(node, nid)
            flowid_input[indice] += indice
            # Set flows
            ret = Err.ERR_OK
            if testing_mode == False:
                if path != []:
                    ret = network_abstraction_layer.createNewFlow(flowid_input[indice], path)
            else:
                print "sourceNode, flowid_input, path:", node, flowid_input[indice], path
            if ret != Err.ERR_OK:
                return ret, None
            indice += 1    

    #Find the output flow
    # Fetch data of tasks which use the reference as input
    sinkNode = None
    # TODO: Fix this, probably can do this with a single query
    taskReference = (DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship)
            .filter_by(reference=reference)
            .all())

    # Check for parameters with input=True
    for ref in taskReference:
        parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id, task_id=ref.task_id, input=True)
                .first())
        if parameter:
            if sinkNode:
                # More than one task with the same input parameter
                return Err.ERR_DATASOURCE_ALREADY_IN_USE, None
            else:    
                sinkNode = ref.task_scheduling.node_id

    #If we could not find reference as input, but it exist as output
    #there is no way we find out which is the dest node for this ref
    #So we return a error
    if not sinkNode:
        for ref in taskReference:
            parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id, task_id=ref.task_id, input=False)
                .first())
            if parameter and ref.sdn_flow_id != 65535:
                return Err.ERR_REFERENCE_ALREADY_IN_USE, None

    #If we could not find reference, let's use the address
    #TODO: convert address to node id
    if not sinkNode and address:
        #TODO: convert address to node id
        address_node_id = int(address)
        adress_node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=address_node_id).first()
        if node is None:
            return Err.ERR_ADDRESS_DOES_NOT_EXIST, None
        sinkNode = address_node_id


    # Define flows for the nodes 65535 (drop)
    flowid_output = 65535
    if sinkNode is not None:
        # Define the flows
        flowid_output, path = defineFlow(nid, sinkNode)
        flowid_output += indice
        # Set flows
        ret = Err.ERR_OK
        if testing_mode == False:
            if path != []:
                ret = network_abstraction_layer.createNewFlow(flowid_output, path)
        else:
            print "SinkNode, flowid_output, path:", sinkNode, flowid_output, path
        if ret != Err.ERR_OK:
            return ret, None    

    #TODO: Find proper way to get these data from user 
    triggered_by_trigger = 0        #triggered  by trigger (0: not activated)
    data_type_output =  0           #data type output (0: floating pointer)
    data_length_output = 0          #data length output (0: 8 bits)
    period_precision = 0            #period precision (0: ms)

    waiting_data_time = 10000       #max time windown for waiting data 
    input_parameter_qtty = 1        #quantity of input parameters
    ifi = flowid_input              #flow id of each input parameter
    pn = [quantity] *  len(dataSourceNodes)   #quantity of each parameter used

    arg = [0] * 32
    null = 0
 
    #Create msg request to send to node
    msgRequest = InstantaneousTaskSchedulingRequestMsg()
    msgRequest.pid = INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID
    msgRequest.tid = tid
    msgRequest.ipn = input_parameter_qtty        
    msgRequest.ta = triggered_by_trigger
    msgRequest.odt =  data_type_output
    msgRequest.odl = data_length_output
    msgRequest.p = period_precision
    msgRequest.ptw = waiting_data_time        
    msgRequest.ofi = flowid_output   
    msgRequest.LIST = parameterList(ifi, pn, arg, null) 
    
    # tin=-1: pending scheduling
    tin = -1

    # Update db
    # TODO: Add task_chain_id
    schedule = DAO.Task_Scheduling(node_id=nid, task_id=tid,
            scheduling_instance_number=tin)
    DAO.dao.add(schedule)

    # TODO: Fix this, find the proper task parameter id
    output_parameter_id = (DAO.dao.query(DAO.Task_In_Out_Parameter)
            .filter_by(task_id=tid, input=False)
            .first()
            .task_parameter_id)

    #Save output parameter
    output_parameter = DAO.Task_Scheduling_In_Out_Relationship(
            task_scheduling_id=schedule.task_scheduling_id,
            task_id=tid,
            task_parameter_id=output_parameter_id,
            sdn_flow_id=flowid_output,
            reference=reference,
            scheduled_data_type_id=data_type_output,
            scheduled_data_length_id=data_length_output)
    DAO.dao.add(output_parameter)

    #Retrieve the input parameter id
    input_parameter_id = (DAO.dao.query(DAO.Task_In_Out_Parameter)
            .filter_by(task_id=tid, input=True)
            .first()
            .task_parameter_id)

    #Save input parameter
    input_parameter = DAO.Task_Scheduling_In_Out_Relationship(
            task_scheduling_id=schedule.task_scheduling_id,
            task_id=tid,
            task_parameter_id=input_parameter_id,
            sdn_flow_id=65535,
            reference=dataSource,
            scheduled_data_type_id=data_type_output,
            scheduled_data_length_id=data_length_output)
    DAO.dao.add(input_parameter)

    #Update flow id for all input parameters
    indice = 0
    for ref in dataSourceReference:
        parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id, task_id=ref.task_id, input = False)
                .first())
        if parameter:
            ref.sdn_flow_id = flowid_input[indice]
            indice += 1
    
    #Update number of scheduled task instances
    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=nid, task_id=tid).first()
    relationship.currently_scheduled_task_instances += 1

    DAO.dao.commit()

    #Send msg, the response will be through a callback
    if testing_mode:
    #    print "setInstantTask::msgRequest " , msgRequest
    #    db_debug = DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship).all()
    #    print db_debug
    #else:
        network_serial.sendMessage(msgRequest, nid)

    return Err.ERR_OK, schedule.task_scheduling_id


def setPeriodicTask(tid, nid, reference, period, duration, address=None):
    """
    Sets a periodic task. Returns the scheduled id.

    Parameters:
        tid(int): the task id to set
        nid(int):  the id of the node where the task is to be executed
        reference(string):  the reference id for the task
        period(int): period between task executions, in seconds
        duration(int): for how long the task should be executed, in seconds
        address(string): if defined, the data will be sent to this defined address
    """
    # Ensure the node exists and that it is capable of executing tid
    if testing_mode:
        print "tid: %d, nid: %d, reference: %s, period: %d, duration: %d, address:%s" % (tid, nid, reference, period, duration,address)
    node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=nid).first()
    if node is None:
        return Err.ERR_NODE_DOES_NOT_EXIST, None
    task = DAO.dao.query(DAO.Task).filter_by(task_id=tid).first()
    if task is None:
        return Err.ERR_TASK_DOES_NOT_EXIST, None
    if task.task_type_id != 2:
        return Err.ERR_TASK_NOT_PERIODIC, None
    relation = (DAO.dao.query(DAO.Sensor_Node_Task_Relationship)
                        .filter_by(node_id=nid, task_id=tid).first())
    if relation is None:
        # Node can't execute task
        return Err.ERR_NODE_DOES_NOT_HAVE_TASK, None
    if (relation.currently_scheduled_task_instances is not None and
            relation.currently_scheduled_task_instances >= relation.max_scheduled_task_instances):
        # Already at maximum capacity for the given task
        return Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED, None

    # Fetch data of tasks which use the reference as input
    sinkNode = None
    # TODO: Fix this, probably can do this with a single query
    taskReference = (DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship)
            .filter_by(reference=reference)
            .all())

    # Check for parameters with input=True
    for ref in taskReference:
        parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id,
                    task_id=ref.task_id, input=True)
                .first())
        if parameter:
            if sinkNode:
                # More than one task with the same input parameter
                return Err.ERR_DATASOURCE_ALREADY_IN_USE, None
            else:    
                sinkNode = ref.task_scheduling.node_id

    #If we could not find reference, let's use the address
    #TODO: convert address to node id
    if not sinkNode and address:
        #TODO: convert address to node id
        address_node_id = int(address)
        adress_node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=address_node_id).first()
        if node is None:
            return Err.ERR_ADDRESS_DOES_NOT_EXIST, None
        sinkNode = address_node_id

    # Define flows for the nodes 65535
    flowid = 65535
    if sinkNode is not None:
        # Define the flows
        flowid, path = defineFlow(nid, sinkNode)
        # Set flows
        ret = Err.ERR_OK
        if testing_mode == False:
            if path != []:
                ret = network_abstraction_layer.createNewFlow(flowid, path)
        else:
            print "SinkNode, flowid, path:", sinkNode, flowid, path
        if ret != Err.ERR_OK:
            return ret, None

    #TODO: Find proper way to get these data from user 
    triggered_by_trigger = 0        #triggered  by trigger (0: not activated)
    data_type_output =  0           #data type output (0: floating pointer)
    data_length_output = 0          #data length output (0: 8 bits)
    period_precision = 0            #period precision (0: ms)
 
    #Create msg request to send to node
    msgRequest = PeriodicTaskSchedulingRequestMsg()
    msgRequest.pid = PERIODIC_TASK_SCHEDULING_REQUEST_PID
    msgRequest.tid = tid
    msgRequest.x = 0        #don't care
    msgRequest.ta = triggered_by_trigger
    msgRequest.odt =  data_type_output
    msgRequest.odl = data_length_output
    msgRequest.p = period_precision
    msgRequest.tp = period  
    msgRequest.ofi = flowid      

    # tin=-1: pending scheduling
    tin = -1

    # Update db
    # TODO: Add task_chain_id
    #Create new scheduling with tin=-1 to be replaced after confirmation with tin
    schedule = DAO.Task_Scheduling(node_id=nid, task_id=tid,
            scheduling_instance_number=tin,
            execution_period=period, parameter_validity_period=duration)
    DAO.dao.add(schedule)

    # TODO: Fix this, find the proper task type
    task_in_out_parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                                    .filter_by(task_id=tid, input=False)
                                    .first())

    in_out = DAO.Task_Scheduling_In_Out_Relationship(
            task_scheduling_id=schedule.task_scheduling_id,
            task_id=tid,
            task_parameter_id=task_in_out_parameter.task_parameter_id,
            sdn_flow_id=flowid,
            reference=reference,
            scheduled_data_type_id=data_type_output,
            scheduled_data_length_id=data_length_output)
    DAO.dao.add(in_out)

    #If we set the address, we should update all the scheduling
    #with the same reference, to go to this address
    if address:
        for ref in taskReference:
            parameter = (DAO.dao.query(DAO.Task_In_Out_Parameter)
                .filter_by(task_parameter_id=ref.task_parameter_id, task_id=ref.task_id, input=False)
                .first())

            if parameter:
                orig_node_id = ref.task_scheduling.node_id
                param_flowid, path = defineFlow(orig_node_id, sinkNode)
                # Set flows
                ret = Err.ERR_OK
                if testing_mode == False:
                    if path != []:
                        ret = network_abstraction_layer.createNewFlow(param_flowid, path)
                else:
                    print "SinkNode, param_flowid, path:", sinkNode, param_flowid, path
                if ret != Err.ERR_OK:
                    return ret, None

                ref.sdn_flow_id = param_flowid                          

    #Update number of scheduled task instances
    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=nid, task_id=tid).first()
    relationship.currently_scheduled_task_instances += 1

    DAO.dao.commit()
  
    #Send msg, the response will be through a callback
    if testing_mode:
    #    print "setPeriodicTask::msgRequest " , msgRequest
    #    db_debug = DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship).all()
    #    print db_debug
    #else:
        network_serial.sendMessage(msgRequest, nid)

    # TODO: Fix this, if there isn't ever a response this data isn't removed
    return Err.ERR_OK, schedule.task_scheduling_id


def cancelTask(sid):
    """
    Cancels the scheduled id, removing it from the active tasks.

    Parameters:
        sid (int): the scheduled task id of the scheduling to be cancelled
    """
    
    # Scheduling from DB
    schedule = DAO.dao.query(DAO.Task_Scheduling).filter_by(task_scheduling_id=sid).first()
    if not schedule:
        return Err.ERR_SCHEDULING_DOES_NOT_EXIST
    nid = schedule.node_id
    tid = schedule.task_id
    tin = schedule.scheduling_instance_number
    sid = schedule.task_scheduling_id

    msgRequest = ScheduledTaskCancelationRequestMsg()
    msgRequest.pid = SCHEDULED_TASK_CANCELATION_REQUEST_PID
    msgRequest.tid = tid
    msgRequest.tin = tin


    #Update DB
    for in_out in DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship).filter_by(task_scheduling_id=sid):
        DAO.dao.delete(in_out)
    DAO.dao.delete(schedule)

    #Update number of scheduled task instances
    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=nid, task_id=tid).first()
    relationship.currently_scheduled_task_instances -= 1
    DAO.dao.commit()

    if testing_mode:
        print "cancelTask::msgRequest " , msgRequest
        db_debug = DAO.dao.query(DAO.Task_Scheduling_In_Out_Relationship).all()
        print db_debug  
    else:
        network_serial.sendMessage(msgRequest, nid)

    return Err.ERR_OK


def parameterList(ifi, pn, arg, null):
    pList = []

    #ifi is a list of 2 bytes
    for flowid in ifi:
        pList.append(flowid >> 8)
        pList.append(flowid & ((1 << 8)-1))

    #pn is a list of 1 byte
    pList += pn

    #arg is a list of 1 byte
    pList += arg

    #null is 0x00
    pList.append(null)   

    return pList
