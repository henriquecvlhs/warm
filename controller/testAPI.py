###############################################################################
# Archive contains a simple test for API functions.
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from API import *
import DAO

testNodes = True
testTasks = True
testParameters = True
testSchedules = True

DAO.startDAO()


#Test GetNodes
if(testNodes):
	nodes = GetNodes(node_id=2)
	print "###id= 2"
	print nodes
	print GetNodesJSON(nodes)


	nodes = GetNodes(latitude=1, longitude=1, range=1)
	print "###area1 => only id=1 is inside"
	print nodes
	print GetNodesJSON(nodes)


	nodes = GetNodes(latitude=1, longitude=1, range=2)
	print "###area2 => all nodes are inside"
	print nodes
	print GetNodesJSON(nodes)


	nodes = GetNodes()
	print "###All"
	print nodes
	print GetNodesJSON(nodes)


#Test GetTasks
if(testTasks):
	tasks = GetTasks(task_id=2)
	print "###id= 2"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(task_id=1)
	print "###id= 1"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(latitude=1, longitude=1, range=1)
	print "###area1 => There is no task inside"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(latitude=1, longitude=1, range=2)
	print "###area2 => All tasks inside" 
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(task_type=1)
	print "###type=1"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks()
	print "###All"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(node_id=2)
	print "###node 2"
	print tasks
	print GetTasksJSON(tasks)

	tasks = GetTasks(node_id=1)
	print "###node 1"
	print tasks
	print GetTasksJSON(tasks)


#GetParameters

if(testParameters):
	parameters = GetParameters(task_id=1)
	print "###task=1"
	print parameters
	print GetParametersJSON(parameters)

	parameters = GetParameters(task_id=2)
	print "###task=2"
	print parameters
	print GetParametersJSON(parameters)

	parameters = GetParameters(task_parameter_id=1)
	print "###task_parameter_id=1"
	print parameters
	print GetParametersJSON(parameters)


#GetSchedules

if(testSchedules):
	schedules = GetSchedules(task_scheduling_id=1)
	print "#####schedule=1"
	print schedules
	print GetSchedulesJSON(schedules)


	schedules = GetSchedules()
	print "#####all schedules"
	print schedules
	print GetSchedulesJSON(schedules)
