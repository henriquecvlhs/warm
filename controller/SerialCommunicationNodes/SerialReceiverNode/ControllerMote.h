/**
 * @file ControllerMote.h
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTROLLER_MOTE_H
#define CONTROLLER_MOTE_H

enum {
  AM_WARM_SERIAL_MSG = 0x9D,
  AM_SERIAL_COMM = 0xF0
};

typedef nx_struct warm_serial_msg {
  nx_uint16_t nodeId;
  nx_uint16_t flowId;
  nx_uint8_t len;
  nx_uint8_t data[90];
} warm_serial_msg;

#endif // CONTROLLER_MOTE_H
