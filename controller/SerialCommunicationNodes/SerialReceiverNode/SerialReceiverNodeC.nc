/**
 * @file SerialReceiverNodeC.nc
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "printf.h"
#include "protocol.h"
#include "protocolPrint.h"
#include "ControllerMote.h"
#include "MiddlewareSettings.h"

module SerialReceiverNodeC {
  uses {
    // Interfaces for initialization:
    interface Boot;
    interface SplitControl as RadioControl;
    interface SplitControl as SerialControl;

    // Interfaces for radio communication
    interface AMSend as RadioSender;
    interface Receive as RadioReceiver;
    interface Packet as  RadioPacket;

    // Interfaces for serial communication
    interface AMSend  as SerialSender;
    interface Receive as SerialReceiver;
    interface Packet as SerialPacket;

    // Miscellaneous:
    interface Leds;
    interface Timer<TMilli>;
  }
}

implementation {
  message_t packet;
  uint8_t busy = 0;

/*******************************************************************/
/* Events */

  event void Boot.booted() {
    call SerialControl.start();
    call RadioControl.start();
  }

  event message_t* SerialReceiver.receive(message_t *bufPtr, void *payload, uint8_t len) {
    uint8_t i;
    warm_serial_msg *msg = (warm_serial_msg*) payload;
    warm_serial_msg *snd = (warm_serial_msg*) call RadioPacket.getPayload(&packet, sizeof(warm_serial_msg));

    snd->nodeId = msg->nodeId;
    snd->flowId = msg->flowId;
    snd->len = msg->len;


    for (i = 0; i < snd->len; i++) {
      snd->data[i] = msg->data[i];
    }

    if (busy==1) {
      return bufPtr;
    }

    if (call RadioSender.send(AM_BROADCAST_ADDR, &packet, sizeof(warm_serial_msg)) == SUCCESS) {
      busy = 1;
    }

    return bufPtr;
  }

  /* This node shouldn't receive radio communications. */
  event message_t* RadioReceiver.receive(message_t* bufPtr, void* payload, uint8_t len) {
    return bufPtr;
  }

  event void Timer.fired() {
  }

  event void SerialSender.sendDone(message_t *butPtr, error_t error) {}

  event void SerialControl.startDone(error_t err) {}

  event void SerialControl.stopDone(error_t err) {}

  event void RadioSender.sendDone(message_t* bufPtr, error_t error) {
    if (bufPtr == &packet) {
      busy = 0;
    }
  }

  event void RadioControl.startDone(error_t err) {}

  event void RadioControl.stopDone(error_t err) {}
}
