/**
 * @file SerialReceiverNodeAppC.nc
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ControllerMote.h"

configuration SerialReceiverNodeAppC {}
implementation {
  components MainC, SerialReceiverNodeC as App;
  components LedsC, new TimerMilliC();
  components PrintfC;

  components ActiveMessageC;
  components new AMSenderC(AM_SERIAL_COMM);
  components new AMReceiverC(AM_SERIAL_COMM);

  components SerialActiveMessageC as SerialTty;
   
  App.Boot -> MainC;
  App.Leds -> LedsC;
  App.Timer -> TimerMilliC;
  
  App.SerialPacket   -> SerialTty;
  App.SerialControl  -> SerialTty;
  App.SerialSender   -> SerialTty.AMSend[AM_WARM_SERIAL_MSG];
  App.SerialReceiver -> SerialTty.Receive[AM_WARM_SERIAL_MSG];

  App.RadioReceiver -> AMReceiverC;
  App.RadioPacket   -> AMSenderC;
  App.RadioSender   -> AMSenderC;
  App.RadioControl  -> ActiveMessageC;
}
