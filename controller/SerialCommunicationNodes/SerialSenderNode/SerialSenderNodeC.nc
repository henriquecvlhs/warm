/**
 * @file SerialSenderNodeC.h
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "printf.h"
#include "protocol.h"
#include "protocolPrint.h"
#include "ControllerMote.h"
#include "MiddlewareSettings.h"

module SerialSenderNodeC {
  uses {
    // Interfaces for initialization:
    interface Boot;
    interface SplitControl as RadioControl;
    interface SplitControl as SerialControl;

    // Interfaces for radio communication
    interface AMSend as RadioSender;
    interface Receive as RadioReceiver;
    interface Packet as  RadioPacket;

    // Interfaces for serial communication
    interface AMSend  as SerialSender;
    interface Receive as SerialReceiver;
    interface Packet as SerialPacket;

    // Miscellaneous:
    interface Leds;
    interface Timer<TMilli>;
  }
}

implementation {
  message_t packet;
  uint8_t busy = 0;

/*******************************************************************/
/* Package creation */
  void prepareTaskDescription(uint8_t *payload, uint8_t *request) {
    TaskDescriptionRequest_t *req = (TaskDescriptionRequest_t*)request;
    TaskDescriptionResponse_t *resp = (TaskDescriptionResponse_t*)payload;

    resp->pid = TASK_DESCRIPTION_RESPONSE_PID;
    resp->tid = req->tid;
    if (resp->tid == 1) {
      resp->tti = 2;
      resp->g = 0;
      resp->s = 0;
    }
    else {
      resp->tti = 1;
      resp->g = 1;
      resp->s = 0;
    }
    resp->msq = 5;
    resp->odf = 0x00CC;
    resp->ipn = 1;
    resp->d = 1;
    resp->a = 0;
    resp->iparam[0].ipq = 2;
    resp->iparam[0].ipf = 0x00CC;
  }

  void preparePeriodicTaskScheduling(uint8_t *payload, uint8_t *request) {
    PeriodicTaskSchedulingRequest_t *req = (PeriodicTaskSchedulingRequest_t*)request;
    TaskSchedulingConfirmation_t *resp = (TaskSchedulingConfirmation_t*)payload;

    resp->pid = PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID;
    resp->tid = req->tid;
    resp->tin = req->tid;
    resp->error.code = 0;
  }

  void prepareNodeCharacteristicsDescription(uint8_t *payloadBuffer) {
    NodeCharacteristicsDescription_t *nodeDescription;

    nodeDescription = (NodeCharacteristicsDescription_t *) payloadBuffer;

    /** Node characteristics */
    nodeDescription->pid = NODE_CHARACTERISTICS_DESCRIPTION_PID;
    nodeDescription->nid = NODE_ID;
    nodeDescription->m   = NODE_MOBILITY;
    nodeDescription->e   = NODE_ENERGY_SOURCE;
    nodeDescription->did = NODE_DEVICE_ID;
    nodeDescription->os  = NODE_OS;

    /** Node localization */
    nodeDescription->latitude  = NODE_LOCATION_LATITUDE;
    nodeDescription->longitude = NODE_LOCATION_LONGITUDE;
    nodeDescription->height    = NODE_LOCATION_HEIGHT;

    /** Node schedule settings */
    nodeDescription->ptq = MAX_SCHEDULED_PERIODIC_TASKS;
    nodeDescription->idq = MAX_SCHEDULED_NETWORK_INPUTS;
    nodeDescription->ntq = MAX_SCHEDULED_NETWORK_OUTPUTS;

    /** Loaded tasks */
    // Simulate the presence of 2 tasks
    nodeDescription->ntn = 2;
    nodeDescription->tid[0] = 1;
    nodeDescription->tid[1] = 2;
  }

/*******************************************************************/
/* Events */

  event void Boot.booted() {
    call RadioControl.start();
    call SerialControl.start();
    call Timer.startOneShot(5000);
  }

  /* This node shouldn't receive serial communications. */
  event message_t* SerialReceiver.receive(message_t *bufPtr, void *payload, uint8_t len) {
    return bufPtr;
  }

  event message_t* RadioReceiver.receive(message_t* bufPtr, void* payload, uint8_t len) {
    warm_serial_msg *msg = (warm_serial_msg*) payload;
    warm_serial_msg *response = call SerialPacket.getPayload(&packet, sizeof(warm_serial_msg));

    uint8_t pid = msg->data[0] >> 4;
    uint8_t sendResponse = 0;
    response->nodeId = NODE_ID;
    response->flowId = NODE_ID;

    switch (pid) {
      case NODE_ASSOCIATE_CONFIRMATION_PID:
        // print packet
        break;

      case TASK_DESCRIPTION_REQUEST_PID:
        // print packet
        prepareTaskDescription((uint8_t*)response->data, (uint8_t*)msg->data);
        sendResponse = 1;
        response->len = sizeof(TaskDescriptionResponse_t);
        break;

      case PERIODIC_TASK_SCHEDULING_REQUEST_PID:
        // print packet
        preparePeriodicTaskScheduling((uint8_t*)response->data, (uint8_t*)msg->data);
        sendResponse = 1;
        response->len = sizeof(TaskSchedulingConfirmation_t);
        break;

      default:
        break;
    }

    if (sendResponse) {
      if(call SerialSender.send(AM_WARM_SERIAL_MSG, &packet, sizeof(warm_serial_msg) == SUCCESS)) {
        busy = 1;
      }
    }

    return bufPtr;
  }

  event void Timer.fired() {
    warm_serial_msg *msg;
    msg = (warm_serial_msg*) call SerialPacket.getPayload(&packet, sizeof(warm_serial_msg));
    msg->nodeId = NODE_ID;
    msg->flowId = NODE_ID;
    msg->len = sizeof(NodeCharacteristicsDescription_t);
    prepareNodeCharacteristicsDescription((uint8_t*)msg->data);

    call SerialSender.send(AM_WARM_SERIAL_MSG, &packet,
                           sizeof(warm_serial_msg));
  }

  event void SerialSender.sendDone(message_t *bufPtr, error_t error) {
    if (bufPtr == &packet) {
      busy = 0;
    }
  }

  event void SerialControl.startDone(error_t err) {}

  event void SerialControl.stopDone(error_t err) {}

  event void RadioSender.sendDone(message_t* bufPtr, error_t error) {
  }

  event void RadioControl.startDone(error_t err) {}

  event void RadioControl.stopDone(error_t err) {}
}
