/**
 * @file protocolPrint.c
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "protocolPrint.h"
#include "protocol.h"

void printNodeCharacteristicsDescription(void *package) {
    uint8_t i;

    printf("\nPrinting Node Characteristics Description Package.");
    printf("\nPID: %d", ((NodeCharacteristicsDescription_t *) package)->pid);
    printf("\nNID: %d", ((NodeCharacteristicsDescription_t *) package)->nid);
    printf("\nM  : %d", ((NodeCharacteristicsDescription_t *) package)->m);
    printf("\nE  : %d", ((NodeCharacteristicsDescription_t *) package)->e);
    printf("\nDID: %d", ((NodeCharacteristicsDescription_t *) package)->did);
    printf("\nOS : %d", ((NodeCharacteristicsDescription_t *) package)->os);
    printf("\nLat: %lu", ((NodeCharacteristicsDescription_t *) package)->latitude);
    printf("\nLon: %lu", ((NodeCharacteristicsDescription_t *) package)->longitude);
    printf("\nHei: %lu", ((NodeCharacteristicsDescription_t *) package)->height);
    printf("\nPTQ: %d", ((NodeCharacteristicsDescription_t *) package)->ptq);
    printf("\nIDQ: %d", ((NodeCharacteristicsDescription_t *) package)->idq);
    printf("\nNTQ: %d", ((NodeCharacteristicsDescription_t *) package)->ntq);
    printf("\nNTN: %d", ((NodeCharacteristicsDescription_t *) package)->ntn);

    for (i = 0; i < ((NodeCharacteristicsDescription_t *) package)->ntn; i++) {
        printf("\nNode loaded task %d - Task ID: %d", i, ((NodeCharacteristicsDescription_t *) package)->tid[i]);
    }

    printf("\n");
}

void printNodeAssociateConfirmation(void *package) {
    printf("\nPrinting Node Associate Confirmation Package.");
    printf("\nPID: %d", ((NodeAssociateConfirmation_t *) package)->pid);
    printf("\nCFI: %u", ((NodeAssociateConfirmation_t *) package)->cfi);

    printf("\n");
}

void printTaskDescriptionRequest(void *package) {
    printf("\nPrinting Task Description Request Package.");
    printf("\nPID: %d", ((TaskDescriptionRequest_t *) package)->pid);
    printf("\nTID: %u", ((TaskDescriptionRequest_t *) package)->tid);

    printf("\n");
}

void printTaskDescriptionResponse(void *package) {
    uint8_t i;

    printf("\nPrinting Task Description Response Package.");
    printf("\nPID: %d", ((TaskDescriptionResponse_t *) package)->pid);
    printf("\nTID: %u", ((TaskDescriptionResponse_t *) package)->tid);
    printf("\nTTI: %d", ((TaskDescriptionResponse_t *) package)->tti);
    printf("\nMSQ: %d", ((TaskDescriptionResponse_t *) package)->msq);
    printf("\nODF: %d", ((TaskDescriptionResponse_t *) package)->odf);
    printf("\nIPN: %d", ((TaskDescriptionResponse_t *) package)->ipn);
    printf("\nD  : %d", ((TaskDescriptionResponse_t *) package)->d);
    printf("\nA  : %d", ((TaskDescriptionResponse_t *) package)->a);
    printf("\nG  : %d", ((TaskDescriptionResponse_t *) package)->g);
    printf("\nS  : %d", ((TaskDescriptionResponse_t *) package)->s);

    for (i = 0; i < ((TaskDescriptionResponse_t *) package)->ipn; i++) {
        printf("\nInput Parameter %d - IPQ: %d", i, ((TaskDescriptionResponse_t *) package)->iparam[i].ipq);
        printf("\nInput Parameter %d - IPF: %d", i, ((TaskDescriptionResponse_t *) package)->iparam[i].ipf);
    }

    printf("\n");
}

void printPeriodicTaskSchedulingRequest(void *package) {
    printf("\nPrinting Periodic Task Scheduling Request Package.");
    printf("\nPID: %d", ((PeriodicTaskSchedulingRequest_t *) package)->pid);
    printf("\nTID: %u", ((PeriodicTaskSchedulingRequest_t *) package)->tid);
    printf("\nTA : %d", ((PeriodicTaskSchedulingRequest_t *) package)->ta);
    printf("\nODT: %d", ((PeriodicTaskSchedulingRequest_t *) package)->odt);
    printf("\nODL: %d", ((PeriodicTaskSchedulingRequest_t *) package)->odl);
    printf("\nP  : %d", ((PeriodicTaskSchedulingRequest_t *) package)->p);
    printf("\nTP : %lu", ((PeriodicTaskSchedulingRequest_t *) package)->tp);
    printf("\nOFI: 0x%4.4X", ((PeriodicTaskSchedulingRequest_t *) package)->ofi);

    printf("\n");
}

void printInstantaneousTaskSchedulingRequest(void *package) {
    uint8_t i;

    printf("\nPrinting Instantaneous Task Scheduling Request Package.");
    printf("\nPID: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->pid);
    printf("\nTID: %u", ((InstantaneousTaskSchedulingRequest_t *) package)->tid);
    printf("\nIPN: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->ipn);
    printf("\nTA : %d", ((InstantaneousTaskSchedulingRequest_t *) package)->ta);
    printf("\nODT: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->odt);
    printf("\nODL: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->odl);
    printf("\nP  : %d", ((InstantaneousTaskSchedulingRequest_t *) package)->p);
    printf("\nPTW: %lu", ((InstantaneousTaskSchedulingRequest_t *) package)->ptw);
    printf("\nOFI: 0x%4.4X", ((InstantaneousTaskSchedulingRequest_t *) package)->ofi);

    for (i = 0; i < ((InstantaneousTaskSchedulingRequest_t *) package)->ipn; i++) {
        printf("\nInput Parameter %d - IFI: 0x%4.4X", i, ((InstantaneousTaskSchedulingRequest_t *) package)->input[i].ifi);
        printf("\nInput Parameter %d - PN : %d", i, ((InstantaneousTaskSchedulingRequest_t *) package)->input[i].pn);
    }

    printf("\nArgs: %s", (char *) ((InstantaneousTaskSchedulingRequest_t *) package)->args);

    printf("\n");
}

void printTriggerTaskSchedulingRequest(void *package) {
    printf("\nPrinting Trigger Task Scheduling Request Package.");
    printf("\nPID: %d", ((TriggerTaskSchedulingRequest_t *) package)->pid);
    printf("\nTID: %u", ((TriggerTaskSchedulingRequest_t *) package)->tid);
    printf("\nTMC: %d", ((TriggerTaskSchedulingRequest_t *) package)->tmc);
    printf("\nTSC: %d", ((TriggerTaskSchedulingRequest_t *) package)->tsc);
    printf("\nTCS: %d", ((TriggerTaskSchedulingRequest_t *) package)->tcs);
    printf("\nTRC: %d", ((TriggerTaskSchedulingRequest_t *) package)->trc);
    printf("\nTMI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tmi);
    printf("\nTSI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tsi);
    printf("\nTRI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tri);
    printf("\nOFI: 0x%4.4X", ((TriggerTaskSchedulingRequest_t *) package)->ofi);

    printf("\n");
}

void printTaskSchedulingConfirmation(void *package) {
    printf("\nPrinting Task Scheduling Confirmation Package.");
    printf("\nPID: %d", ((TaskSchedulingConfirmation_t *) package)->pid);
    printf("\nTID: %u", ((TaskSchedulingConfirmation_t *) package)->tid);
    printf("\nTIN: %d", ((TaskSchedulingConfirmation_t *) package)->tin);
    printf("\nError: 0x%2.2X", ((TaskSchedulingConfirmation_t *) package)->error.code);

    printf("\n");
}

void printTaskStatisticsRequest(void *package) {
    printf("\nPrinting Task Statistics Request Package.");
    printf("\nPID: %d", ((TaskStatisticsRequest_t *) package)->pid);
    printf("\nTID: %u", ((TaskStatisticsRequest_t *) package)->tid);
    printf("\nTIN: %d", ((TaskStatisticsRequest_t *) package)->tin);

    printf("\n");
}

void printTaskStatisticsResponse(void *package) {
    printf("\nPrinting Task Statistics Request Package.");
    printf("\nPID: %d", ((TaskStatisticsResponse_t *) package)->pid);
    printf("\nTID: %u", ((TaskStatisticsResponse_t *) package)->tid);
    printf("\nTIN: %d", ((TaskStatisticsResponse_t *) package)->tin);
    printf("\nTEN: %u", ((TaskStatisticsResponse_t *) package)->ten);

    printf("\n");
}

void printNodeStatisticsRequest(void *package) {
    printf("\nPrinting Node Statistics Request Package.");
    printf("\nPID: %d", ((NodeStatisticsRequest_t *) package)->pid);

    printf("\n");
}

void printNodeStatisticsResponse(void *package) {
    uint8_t i;

    printf("\nPrinting Node Statistics Response Package.");
    printf("\nPID: %d", ((NodeStatisticsResponse_t *) package)->pid);
    printf("\nCPQ: %d", ((NodeStatisticsResponse_t *) package)->cpq);
    printf("\nCIQ: %d", ((NodeStatisticsResponse_t *) package)->ciq);
    printf("\nCNQ: %d", ((NodeStatisticsResponse_t *) package)->cnq);
    printf("\nCBL: %d", ((NodeStatisticsResponse_t *) package)->cbl);
    printf("\nCRP: %d", ((NodeStatisticsResponse_t *) package)->crp);
    printf("\nLat: %lu", ((NodeStatisticsResponse_t *) package)->latitude);
    printf("\nLon: %lu", ((NodeStatisticsResponse_t *) package)->longitude);
    printf("\nHei: %lu", ((NodeStatisticsResponse_t *) package)->height);

    for (i = 0; i < sizeof(loadedTaskIds)/2; i++) {
        printf("\nLoaded Task %d - CSQ: %d", i, ((NodeStatisticsResponse_t *) package)->csq[i]);
    }

    printf("\n");
}

void printScheduledTaskCancellationRequest(void *package) {
    printf("\nPrinting Scheduled Task Cancellation Request Package.");
    printf("\nPID: %d", ((ScheduledTaskCancellationRequest_t *) package)->pid);
    printf("\nTID: %u", ((ScheduledTaskCancellationRequest_t *) package)->tid);
    printf("\nTIN: %d", ((ScheduledTaskCancellationRequest_t *) package)->tin);

    printf("\n");
}

void printScheduledTaskCancellationResponse(void *package) {
    printf("\nPrinting Scheduled Task Cancellation Request Package.");
    printf("\nPID: %d", ((ScheduledTaskCancellationResponse_t *) package)->pid);
    printf("\nTID: %u", ((ScheduledTaskCancellationResponse_t *) package)->tid);
    printf("\nTIN: %d", ((ScheduledTaskCancellationResponse_t *) package)->tin);
    printf("\nS  : %d", ((ScheduledTaskCancellationResponse_t *) package)->s);

    printf("\n");
}
