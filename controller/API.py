###############################################################################
# Archive contains functions and methods related to the controller API.
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import DAO
import task_mapping
from errors import *


def GetNodes(node_id=None, latitude=None, longitude=None, range=None):
    #TODO: add return type in the doc
    """
    GetNodes function

    Args:
        node_id (int)       : node identifier
        latitude (float)    : latitude of the area center  
        longitude (float)   : longitude of the area center
        range (float)       : range the area

    Returns:
        TODO: Add return type
    """   

    nodes = [] 
    
    with DAO.daoLock:
        #if id is given returns a list with one node 
        #if this node_id does not exist returns empty list
        if(node_id):
            node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=node_id).first()
            nodes.append(node)
        #if area is given returns list of nodes inside this area
        #if none is found returns empty list   
        elif(latitude and longitude and range):
            for node in DAO.dao.query(DAO.Sensor_Node).all():
                if (node.latitude - latitude)**2 + (node.longitude - longitude)**2 < range**2:
                    nodes.append(node)
        #if id and area are not given returns all nodes 
        else:
            for node in DAO.dao.query(DAO.Sensor_Node).all():
                nodes.append(node)  

    return nodes 


def GetConnections():
    """
    GetConnections function
    """  
    with DAO.daoLock:
        edges = DAO.dao.query(DAO.Node_Connection).all()
    return edges

def GetTasks(node_id=None, task_id=None, task_type=None, latitude=None, longitude=None, range=None):
    #TODO: add return type in the doc
    """
    GetTasks function

    Args:
        task_id (int)       : task identifier
        node_id (int)       : node identifier
        task_type (int)     : type of the task, can 1 (instant) or 2 (periodic)
        latitude (float)    : latitude of the area center  
        longitude (float)   : longitude of the area center
        range (float)       : range the area

    Returns:
        TODO: Add return type
    """

    tasks = []  

    with DAO.daoLock:
        #if task_id is given returns a list with one specific task 
        #if this id does not exist returns empty list
        if(task_id):  
            task = DAO.dao.query(DAO.Task).filter_by(task_id=task_id).first()
            tasks.append(task)  
        #if task_type is given returns a list with tasks of this type 
        #if this type does not exist or there is not a task of this type returns empty list
        elif(task_type):
            tasks = [task for task in DAO.dao.query(DAO.Task).filter_by(task_type_id=task_type)]
        #if node_id is given returns a list with all tasks that this node has 
        #if this node_id does not exist or the node has not tasks returns empty list
        elif(node_id):
            for relation in DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=node_id):
                task = DAO.dao.query(DAO.Task).filter_by(task_id=relation.task_id).first()
                if(task not in tasks):
                    tasks.append(task)
        #if area is given returns list of tasks inside this area
        #if none is found returns empty list   
        elif(latitude and longitude and range):
            for node in DAO.dao.query(DAO.Sensor_Node).all():
                if ((node.latitude - latitude)**2 + (node.longitude - longitude)**2 < range**2):
                    for relation in DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=node.node_id):
                        task = DAO.dao.query(DAO.Task).filter_by(task_id=relation.task_id).first()
                        if(task not in tasks):
                            tasks.append(task)
        #if none parameter is given returns all tasks                            
        else:
            tasks = DAO.dao.query(DAO.Task).all()
    
    return tasks


def GetParameters(task_id=None, task_parameter_id=None):
    """
    GetParameters function

    Args:
        task_id (int)               : task identifier
        task_parameter_id (int)     : task parameter identifier

    Returns:
        TODO: Add return type
    """

    parameters = []  

    with DAO.daoLock:
        #if id is given returns a list with one specific parameter 
        #if this id does not exist returns empty list
        if(task_parameter_id):  
            for parameter in DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_parameter_id=task_parameter_id):
                parameters.append(parameter)   
        #if task_id is given returns a list with one specific parameter 
        #if this id does not exist returns empty list
        elif(task_id):  
            for parameter in DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_id=task_id):
                parameters.append(parameter) 
                       
    return parameters


def GetSchedules(node_id=None, task_id=None, task_scheduling_id=None):
    """
    GetTasks function

    Args:
        task_id (int)               : task identifier
        node_id (int)               : node identifier
        task_scheduling_id (int)    : task_scheduling identifier

    Returns:
        TODO: Add return type
    """
    task_scheduling_list = []

    with DAO.daoLock:
        #if id is given returns a list with one specific schedule 
        #if this id does not exist returns empty list
        if(task_scheduling_id):
            task_scheduling = DAO.dao.query(DAO.Task_Scheduling).filter_by(task_scheduling_id=task_scheduling_id).first()
            task_scheduling_list.append(task_scheduling)
        #if task_id is given returns a list with all shedules of that task
        #if this id does not exist or the task has no schedule returns empty list
        elif(task_id):  
            for task_scheduling in DAO.dao.query(DAO.Task_Scheduling).filter_by(task_id=task_id):
                task_scheduling_list.append(task_scheduling) 
        #if node_id is given returns a list with all shedules of that node
        #if this id does not exist or the node has no schedule returns empty list
        elif(node_id):  
            for task_scheduling in DAO.dao.query(DAO.Task_Scheduling).filter_by(node_id=node_id):
                task_scheduling_list.append(task_scheduling) 
        #if none parameter is given returns all schedules 
        else:
            for task_scheduling in DAO.dao.query(DAO.Task_Scheduling).all():
                task_scheduling_list.append(task_scheduling)

    return task_scheduling_list


def SetSchedule(schedule): 
    #TODO: Add documentation to header
    with DAO.daoLock:
        if not schedule['tid'] or not schedule['ref'] or not schedule['nid']:
            ret = Err.ERR_TID_NID_SID_REFERENCE_NOT_GIVEN
            return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') not scheduled. Error ' + str(ret) + ': ' + Err().toStr(ret)}

        #Periodic task
        if schedule['period'] and schedule['duration']:
            #Schedule a periodic task
            ret,sid = task_mapping.setPeriodicTask(schedule['tid'], schedule['nid'], schedule['ref'], schedule['period'], schedule['duration'])
            if ret == Err.ERR_OK:
                return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') scheduled succesfully! Task Scheduling ID = ' + str(sid) }
            return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') not scheduled. Error ' + str(ret) + ': ' + Err().toStr(ret)}
            
        #Instant task
        if schedule['data'] and schedule['quantity']:
            #Schedule an instant task
            ret,sid = task_mapping.setInstantTask(schedule['tid'], schedule['nid'], schedule['ref'], schedule['data'], schedule['quantity'])
            if ret == Err.ERR_OK:
                return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') scheduled succesfully! Task Scheduling ID = ' + str(sid)}
            return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') not scheduled. Error ' + str(ret) + ': ' + Err().toStr(ret)}
        
    ret = ERR_NOT_ENOUGH_PARAMETERS
    return {'msg' : 'Task (tid=' + str(schedule['tid']) + ') not scheduled. Error ' + str(ret) + ': ' + Err().toStr(ret)}


def CancelSchedule(sid=None):
    #TODO: Add documentation to header
    with DAO.daoLock:
        if not sid:
            ret = Err.ERR_SID_NOT_GIVEN
            return {'msg' : 'Schedule (sid=' + sid + ') not cancelled. Error ' + str(ret) + ': ' + Err().toStr(ret)}

        ret = task_mapping.cancelTask(sid) 
        if ret == Err.ERR_OK:
            return {'msg' : 'Schedule (sid=' + sid + ') cancelled succesfully! ' }
    return {'msg' : 'Schedule (sid=' + sid + ') not cancelled. Error ' + str(ret) + ': ' + Err().toStr(ret)}
  

"""=================Auxiliar Functions======================="""
#TODO(YUKA): Find another place to put them


def GetNodesJSON(nodes=[]):
    nodesJSON = []

    with DAO.daoLock:
        for node in nodes:
            #Get list of task of this node
            tasksJSON = []
            for relationship in node.sensor_node_task_relationship:
                taskJSON = {
                    'task_id': relationship.task.task_id,
                    'description': relationship.task.description,
                    'generates_data': relationship.task.generates_data,
                    'controls_actuator': relationship.task.controls_actuator,
                    'aggregates_data':relationship.task.aggregates_data,
                    'data_sink': relationship.task.data_sink,
                    'type': {
                        'type_id': relationship.task.task_type.task_type_id,
                        'description': relationship.task.task_type.description
                    },
                    'currently_scheduled_task_instances': relationship.currently_scheduled_task_instances,
                    'max_scheduled_task_instances': relationship.max_scheduled_task_instances
                }
                tasksJSON.append(taskJSON)

            nodeJSON = {
                'node_id':node.node_id,  
                'device': { 
                    'device_id':node.device.device_id,
                    'name': node.device.name,
                    'description':node.device.description
                },
                'operating_system': { 
                    'os_id': node.operating_system.os_id,
                    'name': node.operating_system.name,
                    'description': node.operating_system.description
                },
                'mobility': node.mobility,
                'energy_autonomy': node.energy_autonomy,
                'latitude': node.latitude,
                'longitude': node.longitude,
                'height': node.height,
                'periodic_task_qtty': node.periodic_task_qtty,
                'max_periodic_task_qtty': node.max_periodic_task_qtty,
                'input_data_qtty': node.input_data_qtty,
                'max_input_data_qtty': node.max_input_data_qtty,
                'network_task_qtty': node.network_task_qtty,
                'max_network_task_qtty': node.max_network_task_qtty,
                'occupied_ram_percentage': node.occupied_ram_percentage,
                'current_battery_level': node.current_battery_level,
                'tasks': tasksJSON
            }
            nodesJSON.append(nodeJSON)

    return nodesJSON
    

def GetConnectionsJSON(edges=[], nodes=[]):
    edgesJSON = []
    with DAO.daoLock:
        for edge in edges:
            edgeJSON = {
                'from': edge.node_id, 
                'to': edge.neighbour_node_id
            }
            edgesJSON.append(edgeJSON)

        nodesJSON = []
        for node in nodes:
            nodeJSON = {
                'id': node.node_id, 
                'label': str(node.node_id)
            }
            nodesJSON.append(nodeJSON)
        connectionsJSON = {'edges':edgesJSON,'nodes':nodesJSON}

    return connectionsJSON


def GetTasksJSON(tasks=[]):
    tasksJSON = []
    with DAO.daoLock:
        for task in tasks:
            #Get list of nodes that have this task
            nodesJSON = []
            for relationship in task.sensor_node_task_relationship:
                nodeJSON = {
                    'node_id': relationship.node_id,
                    'currently_scheduled_task_instances': relationship.currently_scheduled_task_instances,
                    'max_scheduled_task_instances': relationship.max_scheduled_task_instances
                }
                nodesJSON.append(nodeJSON)

            taskJSON = {
                'task_id': task.task_id,
                'description': task.description,
                'generates_data': task.generates_data,
                'controls_actuator': task.controls_actuator,
                'aggregates_data': task.aggregates_data,
                'data_sink': task.data_sink,
                'type': {
                    'type_id': task.task_type.task_type_id,
                    'description': task.task_type.description
                },
                'nodes': nodesJSON
            }
            tasksJSON.append(taskJSON)  

    return tasksJSON


def GetParametersJSON(parameters=[]):
    parametersJSON = []
    with DAO.daoLock:
        for parameter in parameters:
            parameterJSON = {
                'task_id': parameter.task_id,
                'task_parameter_id': parameter.task_parameter_id,
                'description': parameter.description,
                'input' : parameter.input,
                'maximum_qtty_per_task_execution': parameter.maximum_qtty_per_task_execution,
                'support_floating_point': parameter.support_floating_point,
                'support_fixed_point': parameter.support_fixed_point,
                'support_integer': parameter.support_integer,
                'support_unsigned_integer': parameter.support_unsigned_integer,
                'support_bit_array': parameter.support_bit_array,
                'support_8bits': parameter.support_8bits,
                'support_16bits': parameter.support_16bits,
                'support_32bits': parameter.support_32bits,
                'support_64bits': parameter.support_64bits,
            }
            parametersJSON.append(parameterJSON)  
    return parametersJSON


def GetSchedulesJSON(schedules=[]):
    schedulesJSON = []
    with DAO.daoLock:
        for schedule in schedules:
            scheduleJSON = {
                'task_scheduling_id': schedule.task_scheduling_id,
                'scheduling_instance_number': schedule.scheduling_instance_number,
                'task_chain_id': schedule.task_chain_id,
                'task_id': schedule.task_id,
                'node_id': schedule.node_id,
                'execution_period': schedule.execution_period,
                'parameter_validity_period': schedule.parameter_validity_period,
                'period_precision_millis_micro': schedule.period_precision_millis_micro
            }
            schedulesJSON.append(scheduleJSON)  
    return schedulesJSON


