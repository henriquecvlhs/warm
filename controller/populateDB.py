###############################################################################
# Populate database with fake data for tests
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import DAO
from dictionaries import *

DAO.startDAO()

def populateTestDatabase(useFakeData=True):

    # Devices
    for key, value in Dict.device_name.iteritems():
        device = DAO.Device(device_id=key, name=value, description=Dict.device_description.get(key,""))
        DAO.dao.add(device)

    # OSs
    for key, value in Dict.os_name.iteritems():
        os = DAO.Operating_System(os_id=key, name=value, description=Dict.os_description.get(key, ""))
        DAO.dao.add(os)

    # Task types
    for key, value in Dict.task_type_description.iteritems():
        task_type = DAO.Task_Type(task_type_id=key, description=value)
        DAO.dao.add(task_type)

    # Data Type
    for key, value in Dict.data_type_description.iteritems():
        data_type = DAO.Data_Type(data_type_id=key, description =value)
        DAO.dao.add(data_type)

    # Data Length
    for key, value in Dict.data_length_description.iteritems():
        data_length = DAO.Data_Length(data_length_id=key, description =value)
        DAO.dao.add(data_length)




    ######################################
    #           Fake Data                #
    ######################################

    if useFakeData:
    # Nodes
        node1 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True,
                energy_autonomy=False, latitude=0.5, longitude=1.5, height=1.5)
        node2 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=False,
                energy_autonomy=False, latitude=2.0, longitude=1.0, height=1.0)
        node3 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
                energy_autonomy=True, latitude=2.0, longitude=2.0, height=2.0)
        node4 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
                energy_autonomy=True, latitude=6.0, longitude=2.0, height=2.0)
        node5 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
                energy_autonomy=True, latitude=5.0, longitude=8.0, height=1.0)
        DAO.dao.add(node1)
        DAO.dao.add(node2)
        DAO.dao.add(node3)
        DAO.dao.add(node4)
        DAO.dao.add(node5)

        # Node connections
        nodeConn1 = DAO.Node_Connection(node_id=1, neighbour_node_id=2)
        nodeConn2 = DAO.Node_Connection(node_id=2, neighbour_node_id=3)
        nodeConn3 = DAO.Node_Connection(node_id=4, neighbour_node_id=3)
        nodeConn4 = DAO.Node_Connection(node_id=5, neighbour_node_id=4)
        nodeConn5 = DAO.Node_Connection(node_id=5, neighbour_node_id=1)
        DAO.dao.add(nodeConn1)
        DAO.dao.add(nodeConn2)
        DAO.dao.add(nodeConn3)
        DAO.dao.add(nodeConn4)
        DAO.dao.add(nodeConn5)

        # Tasks relationships
        nodetask1 = DAO.Sensor_Node_Task_Relationship(node_id=1, task_id=1,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=5)
        nodetask2 = DAO.Sensor_Node_Task_Relationship(node_id=2, task_id=1,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=2)
        nodetask3 = DAO.Sensor_Node_Task_Relationship(node_id=2, task_id=2,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=2)
        nodetask4 = DAO.Sensor_Node_Task_Relationship(node_id=4, task_id=4,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=2)
        nodetask5 = DAO.Sensor_Node_Task_Relationship(node_id=5, task_id=1,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=2)
        nodetask6 = DAO.Sensor_Node_Task_Relationship(node_id=5, task_id=3,
                currently_scheduled_task_instances=0,
                max_scheduled_task_instances=2)
        DAO.dao.add(nodetask1)
        DAO.dao.add(nodetask2)
        DAO.dao.add(nodetask3)
        DAO.dao.add(nodetask4)
        DAO.dao.add(nodetask5)
        DAO.dao.add(nodetask6)

        # Tasks
        task1 = DAO.Task(description=Dict.task_description.get(1, ""), task_type_id=2)
        task2 = DAO.Task(description=Dict.task_description.get(2, ""), task_type_id=1)
        task3 = DAO.Task(description=Dict.task_description.get(3, ""), task_type_id=2)
        task4 = DAO.Task(description=Dict.task_description.get(4, ""), task_type_id=1)
        DAO.dao.add(task1)
        DAO.dao.add(task2)
        DAO.dao.add(task3)
        DAO.dao.add(task4)

        # Task parameters
        taskparam1 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=1,
                input=False, support_16bits=True, support_integer=True)
        taskparam2 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=2,
                input=True, support_16bits=True, support_integer=True)
        taskparam3 = DAO.Task_In_Out_Parameter(task_parameter_id=1, task_id=2,
                input=False, support_16bits=True, support_integer=True)
        taskparam4 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=3,
                input=False, support_16bits=True, support_integer=True)
        taskparam5 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=4,
                input=True, support_16bits=True, support_integer=True)
        taskparam6 = DAO.Task_In_Out_Parameter(task_parameter_id=1, task_id=4,
                input=False, support_16bits=True, support_integer=True)
        DAO.dao.add(taskparam1)
        DAO.dao.add(taskparam2)
        DAO.dao.add(taskparam3)
        DAO.dao.add(taskparam4)
        DAO.dao.add(taskparam5)
        DAO.dao.add(taskparam6)

        # Commit all the changes
        DAO.dao.commit()

if __name__== '__main__':
    populateTestDatabase()
