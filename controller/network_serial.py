###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import threading
import thread
import time

from errors import *
import tos
import network_monitor
import task_mapping
import network_abstraction_layer
from packets import *


am = None 
serialNodeId = 3

debug_mode = False

serialLock = threading.Lock()

class workerThread(threading.Thread):
    def __init__(self, data, nodeid):
        threading.Thread.__init__(self)
        self.data = data
        self.nodeid = nodeid

    def run(self):
        network_monitor.read(self.data, self.nodeid)


def initSerial(sourceSerial=None):
    # Creates the am instance responsible for receiving and sending data
    global am
    am = tos.AM(sourceSerial, messageProcessingHook)

    # TODO: Initialize which one is the serial node


def messageProcessingHook(packet):
    """
    Function responsible for data extracting from the TinySDN packet.

    Args:
        packet(Msg): The TinySDN packet with nodeid, flowid and payload
    
    Returns:
        (nodeid, packet)
    """
    if packet is None:
        return None

    pack = SerialMessagePacket(packet.data)

    if pack.nodeid > 20 or pack.len > 100:
        print "Invalid packet"
        print pack
        return None

    if debug_mode:
        print "Received data"
        print "NodeId, flowId, len:", pack.nodeid, pack.flowid, pack.len,
        print "pid:", network_monitor.getPID(pack.data) 

    #thread.start_new_thread(network_monitor.read, (pack.data, pack.nodeid))
    thre = workerThread(pack.data, pack.nodeid)
    thre.start()


def loop():
    """
    Listens for messages in the serial connection. The message processing is
    done through callbacks.
    """
    while True:
        # Acquire serial lock
        with serialLock:
            # Wait 1 second for a reply
            am.read(1)

        # Give some time for other threads to acquire the lock
        time.sleep(0.05)
        # The message processing is done through callbacks


# waitReply is currently being ignored, the messages are processed through
# callbacks
def sendMessage(msg, nodeId, waitReply=True):
    """
    Sends message msg through the serial connection.

    Args:
        msg(Packet): The packet to be sent through the serial connection.
        nodeId(int): The id of the destination node
        waitReply(Bool): Whether the sending entity expects for a reply

    Returns:
        Either (replyNode, packet) or (None, None)
    """

    # Define the flow.
    #flowId, path = task_mapping.defineFlow(serialNodeId, nodeId)
    #ret = network_abstraction_layer.createNewFlow(flowId, path)
    #if ret == Err.ERR_OK:
    #    controlFlow = DAO.Control_Flows(node_id=nodeId, sdn_flow_id=flowId,
    #                                    incoming_outgoing=True)
    #    DAO.dao.add(controlFlow)
    #    DAO.dao.commit()

    pack = SerialMessagePacket()
    pack.nodeid = nodeId
    # TODO: Fix this
    pack.flowid = nodeId
    #pack.flowid = flowId
    pack.len = len(msg.payload())
    pack.data = msg.payload()

    # Acquire serial lock
    with serialLock:
        if debug_mode:
            print "Sending packet",
            print "Nodeid:", pack.nodeid, "pid:", network_monitor.getPID(pack.data)
            print pack
        am.write(packet=pack, amId=0x9D, timeout=5, blocking=True)
        print "Sent packet"

    return None
