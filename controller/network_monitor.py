###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from packets import *
from errors import *
import DAO
import network_serial
from dictionaries import *


# TODO: Do a proper way to deal with testing
testing_mode = False

def getPID(payload):
    """
    Function returns the payload pid.
    """
    return Msg(payload).pid


def read(payload, nid):
    """
    Given the payload, this function identifies the type of packet
    and executes what is needed in each case

    Args:
        payload(Msg): the payload to process
        nid(int): the source node of the payload
    """
    pid = getPID(payload)
    if testing_mode:
        print "pid =", pid
    
    # Acquire lock and release it when the context is over
    with DAO.daoLock:
        #Node characteristics description
        if pid == NODE_CHARACTERISTICS_DESCRIPTION_PID:
            msg = NodeCharacteristicsDescriptionMsg(payload)
            if testing_mode:
                print "Node characteristics message:"
                print msg
            addNewNode(msg)

            #Send confirmation 
            msgResponse = NodeAssociateConfirmationMsg()
            msgResponse.pid = NODE_ASSOCIATE_CONFIRMATION_PID
            # TODO: Proper flowid
            msgResponse.flowid = 999
            if testing_mode:
                print "NodeCharacteristicsDescriptionMsg: msgResponse =", msgResponse

            network_serial.sendMessage(msgResponse, int(nid), False)

            #Send the request for all task characteristics
            for task_id in msg.tid:
                if task_id == 0:
                    break
                msgRequest = TaskDescriptionRequestMsg()
                msgRequest.pid = TASK_DESCRIPTION_REQUEST_PID
                msgRequest.tid = task_id
                if testing_mode:
                    print "TaskDescriptionRequestMsg: msgRequest =", msgRequest
                network_serial.sendMessage(msgRequest, int(nid), False)

            return Err.ERR_OK

        #Task description response
        elif pid == TASK_DESCRIPTION_RESPONSE_PID:
            msg = TaskDescriptionResponseMsg(payload)
            addNewTask(msg, nid)
            return Err.ERR_OK

        #Task scheduling confirmation
        elif (pid == PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID or
                pid == INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID or
                pid == TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID):
            if testing_mode:
                print "Task scheduling confirmation"
            msg = TaskSchedulingConfirmationMsg(payload)
            return confirmScheduling(msg, nid) 

        #Scheduled task cancelation response
        elif pid == SCHEDULED_TASK_CANCELATION_RESPONSE_PID:
            msg = ScheduledTaskCancelationResponseMsg(payload)
            if not msg.s: 
                return Err.ERR_CANCELLING_TASK_IN_NODE, msg.tin
            return Err.ERR_OK, msg.tin

        #Task statistics response
        elif pid == TASK_STATISTICS_RESPONSE_PID:
            msg = TaskStatisticsResponseMsg(payload)
            updateScheduling(msg, nid)
            return Err.ERR_OK

        #Node statistics response
        elif pid == NODE_STATISTICS_RESPONSE_PID:
            msg = NodeStatisticsResponseMsg(payload)
            updateNode(msg, nid)
            return Err.ERR_OK

        else:
            return Err.ERR_PID_DOES_NOT_EXIST


def addNewNode(msg):
    """
    Adds new node to the database.

    Args:
        msg(Msg): The message sent by the node with its information
    """

	#If node_id does not exist, create new node
    node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=msg.nid).first()
    if not node:
        node = DAO.Sensor_Node(node_id=msg.nid)
        DAO.dao.add(node)

    #Map msg to class Sensor_Node
    node.device_id = msg.did
    node.os_id = msg.os
    node.mobility = msg.m
    node.energy_autonomy = msg.e
    node.latitude = msg.latitude
    node.longitude = msg.longitude
    node.height = msg.height
    node.periodic_task_qtty = 0
    node.max_periodic_task_qtty = msg.ptq
    node.input_data_qtty = 0
    node.max_input_data_qtty = msg.idq
    node.network_task_qtty = 0
    node.max_network_task_qtty = msg.ntq
    DAO.dao.commit()

    #Add relationship node-task
    for task_id in msg.tid:
        if task_id == 0:
            break
        relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=msg.nid, task_id=task_id).first()
        if not relationship:
            relationship = DAO.Sensor_Node_Task_Relationship(node_id=msg.nid, task_id=task_id)
            DAO.dao.add(relationship)

    DAO.dao.commit()	


def addNewTask(msg, nid):
    """
    Adds new task to the database.

    Args:
        msg(Msg): The message with the task's information
        nid(int): The node id of the sender node
    """

    if testing_mode:
        print "Adding new task:", msg.tid
    #If task_id does not exist, create new task
    task = DAO.dao.query(DAO.Task).filter_by(task_id=msg.tid).first()
    if not task:
        task = DAO.Task(task_id=msg.tid)
        DAO.dao.add(task)

    #Map msg to class Task
    task.description = Dict.task_description.get(msg.tid, "")
    task.task_type_id = msg.tti
    task.generates_data = msg.d
    task.controls_actuator = msg.a
    task.aggregates_data = msg.g
    task.data_sink = msg.s
    DAO.dao.commit()

    #Update max number of scheduled task instances
    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=nid, task_id=msg.tid).first()
    if not relationship:
        relationship = DAO.Sensor_Node_Task_Relationship(node_id=nid, task_id=msg.tid)	
        DAO.dao.add(relationship)

    relationship.max_scheduled_task_instances = msg.msq
    relationship.currently_scheduled_task_instances = 0
    DAO.dao.commit()

    for i in range(0, msg.ipn):
        inputParameter = DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_id=msg.tid, task_parameter_id=i).first()
        if not inputParameter:
            inputParameter = DAO.Task_In_Out_Parameter(task_id=msg.tid, task_parameter_id=i, input=True)
            DAO.dao.add(inputParameter)  

        ipq = msg.ipq_ipf[i] >> 10
        ipf = msg.ipq_ipf[i] & ((1 << 10) - 1)

        inputParameter.maximum_qtty_per_task_execution = ipq
        inputParameter.support_floating_point = 	(ipf >> 9) & 1 
        inputParameter.support_fixed_point =  		(ipf >> 8) & 1
        inputParameter.support_integer = 			(ipf >> 7) & 1
        inputParameter.support_unsigned_integer = 	(ipf >> 6) & 1
        inputParameter.support_bit_array =			(ipf >> 5) & 1
        inputParameter.support_boolean = 			(ipf >> 4) & 1
        inputParameter.support_8bits = 				(ipf >> 3) & 1
        inputParameter.support_16bits = 			(ipf >> 2) & 1
        inputParameter.support_32bits = 			(ipf >> 1) & 1
        inputParameter.support_64bits = 			(ipf >> 1) & 1	

        inputParameter.description = Dict.parameter_description.get("%d-%d" % (msg.tid, i), "")
        DAO.dao.commit()

    outputParameter =  DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_id=msg.tid, task_parameter_id=msg.ipn).first()
    if not outputParameter:
        outputParameter = DAO.Task_In_Out_Parameter(task_id=msg.tid, task_parameter_id=msg.ipn, input=False)
        DAO.dao.add(outputParameter)

    outputParameter.maximum_qtty_per_task_execution = 1
    outputParameter.support_floating_point = 	(msg.odf << 9) & 1
    outputParameter.support_fixed_point =  		(msg.odf << 8) & 1
    outputParameter.support_integer = 			(msg.odf << 7) & 1
    outputParameter.support_unsigned_integer = 	(msg.odf << 6) & 1
    outputParameter.support_bit_array = 		(msg.odf << 5) & 1
    outputParameter.support_boolean = 			(msg.odf << 4) & 1
    outputParameter.support_8bits = 			(msg.odf << 3) & 1
    outputParameter.support_16bits = 			(msg.odf << 2) & 1
    outputParameter.support_32bits = 			(msg.odf << 1) & 1
    outputParameter.support_64bits = 			(msg.odf << 0) & 1

    outputParameter.description = Dict.parameter_description.get("%d-%d" % (msg.tid, msg.ipn), "")
    DAO.dao.commit()


def updateScheduling(msg):
    """
    Update scheduling statistics to the database.

    Args:
        msg(Msg): The message with the scheduling statistics information
    """

    #Update Scheduling
    scheduling = DAO.dao.query(DAO.Task_Scheduling).filter_by(scheduling_instance_number=msg.tin).first()
    if  scheduling:
        scheduling.execution_qtty = msg.ten
        DAO.dao.commit()


def confirmScheduling(msg, nid):
    """
    Process the task scheduling response packet.

    Args:
        msg(Msg): The message with the task scheduling information
        nid(int): The node id

    Returns:
        (Error, tin)
    """
    tid = msg.tid
    tin = msg.tin

    if testing_mode:
        print "Scheduling confirmation msg:", msg

    # If s is not zero then an error occurred
    if not msg.s:
        # TODO: Remove already added data from the db
        if msg.t:
            return Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED, tin
        if msg.i:
            return Err.ERR_INPUT_TASK, tin
        if msg.n:
            return Err.ERR_OUTPUT_TASK, tin
        if msg.p:
            return Err.ERR_MAX_PERIODIC_TASK_REACHED, tin
        if msg.d:
            return Err.ERR_MAX_INSTANT_TASK_REACHED, tin
        if msg.r:
            return Err.ERR_MAX_TRIGGER_TASK_REACHED, tin

    # Reached here, scheduling successful
    # Update db

    # Scheduling was already created, update it now it was successful
    schedule = DAO.dao.query(DAO.Task_Scheduling).filter_by(node_id=nid, task_id=tid, scheduling_instance_number=-1).first()
    schedule.scheduling_instance_number=tin

    DAO.dao.commit()

    return (Err.ERR_OK, tin)


def updateNode(msg, nid):
    """
    Update node statistics to the database.

    Args:
        msg(Msg): The message with the node statistics
        nid(int): The node id
    """

    #Update node 
    node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=nid).first()
    if node: 
        node.latitude = msg.latitude
        node.longitude = msg.longitude
        node.height = msg.height
        node.periodic_task_qtty = msg.cpq
        node.input_data_qtty = msg.ciq
        node.network_task_qtty = msg.cnq 
        node.occupied_ram_percentage = msg.crp
        node.current_battery_level = msg.cbl

        DAO.dao.commit()

    #Update relationship node-task
    i = 0
    for relationship in DAO.Sensor_Node_Task_Relationship.filter(Sensor_Node_Task_Relationship.node_id==nid).order_by(Sensor_Node_Task_Relationship.task_id):
        relationship.currently_scheduled_task_instances = msg.csq[i]
        i += 1
    DAO.dao.commit()

