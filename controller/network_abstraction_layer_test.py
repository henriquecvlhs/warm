###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import network_abstraction_layer
import errors

def initializeTests():
    # These tests require the controller to be up and running
    network_abstraction_layer.connectToController()


def createNewFlowTest():
    flowId = 1
    flow = [1, 2, 3]
    print network_abstraction_layer.createNewFlow(flowId, flow)

    flowId = 3
    flow = [1, 3, 5, 7, 9]
    print network_abstraction_layer.createNewFlow(flowId, flow)

    flowId = -1
    flow = [1, 3, 5, 7, 9]
    print network_abstraction_layer.createNewFlow(flowId, flow)

    flowId = 9
    flow = [1, 3, 5, 7, 9, 1, 1]
    print network_abstraction_layer.createNewFlow(flowId, flow)

def requestNetworkInformationTest():
    print network_abstraction_layer.requestNetworkInformation()

if __name__=="__main__":
    initializeTests()
    createNewFlowTest()
    requestNetworkInformationTest()
