#!flask/bin/python
###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from flask import Flask, jsonify
from flask import request

import API


app = Flask(__name__)

def start_flask():
    app.run(debug=True)


def str2float(s):
    try:
        if(s):
            n = float(s)
            return n
        else:
            return None    
    except ValueError:
        return None


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE')
    return response
    

@app.route('/todo/api/v1.0/nodes', methods=['GET'])
def get_nodes():

    #Get parameters
    node_id = str2float(request.args.get('node_id'))
    latitude = str2float(request.args.get('latitude'))
    longitude =  str2float(request.args.get('longitude'))
    range = str2float(request.args.get('range'))

    #Call API
    nodes = API.GetNodes(node_id=node_id, latitude=latitude, longitude=longitude, range=range)
    return jsonify({'nodes': API.GetNodesJSON(nodes)})   

@app.route('/todo/api/v1.0/connections', methods=['GET'])
def get_connections():

    #Call API
    edges = API.GetConnections()
    nodes = API.GetNodes()
    return jsonify(API.GetConnectionsJSON(edges, nodes))  


@app.route('/todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():

    #Get the parameters
    node_id = str2float(request.args.get('node_id'))
    task_id = str2float(request.args.get('task_id'))
    task_type = str2float(request.args.get('task_type'))
    latitude = str2float(request.args.get('latitude'))
    longitude =  str2float(request.args.get('longitude'))
    range = str2float(request.args.get('range'))

    #Call API       
    tasks = API.GetTasks(node_id=node_id, task_id=task_id, task_type=task_type, latitude=latitude, longitude=longitude, range=range)           
    return jsonify({'tasks': API.GetTasksJSON(tasks)})


@app.route('/todo/api/v1.0/parameters', methods=['GET'])
def get_parameters():

    #Get parameters
    task_parameter_id = str2float(request.args.get('task_parameter_id'))
    task_id = str2float(request.args.get('task_id'))

    #Call API
    parameters = API.GetParameters(task_parameter_id=task_parameter_id, task_id=task_id)
    return jsonify({'parameters': API.GetParametersJSON(parameters)})   


@app.route('/todo/api/v1.0/schedules', methods=['GET'])
def get_schedules():

    #Get parameters
    node_id = str2float(request.args.get('node_id'))
    task_id = str2float(request.args.get('task_id'))
    task_scheduling_id = str2float(request.args.get('task_scheduling_id'))

    #Call API
    schedules = API.GetSchedules(node_id=node_id, task_id=task_id, task_scheduling_id=task_scheduling_id)
    return jsonify({'schedules': API.GetSchedulesJSON(schedules)})


#TODO: arrumar para novo formato de dados
@app.route('/todo/api/v1.0/schedules', methods=['POST'])
def post_schedules():
    if not request.json: 
        abort(400)

    print request.json    
    schedule = {
            'tid': str2float(request.json.get('tid', "")),
            'nid': str2float(request.json.get('nid', "")),
            'address': request.json.get('address', ""),
            'ref': request.json.get('ref', ""),
            'period': str2float(request.json.get('period', "")),
            'duration': str2float(request.json.get('duration', "")),
            'data': request.json.get('data', ""),
            'quantity': str2float(request.json.get('quantity', ""))
    }

    print schedule    
    return jsonify(API.SetSchedule(schedule))    


#TODO: arrumar para novo formato de dados
@app.route('/todo/api/v1.0/schedules', methods=['DELETE'])
def delete_schedules():
    sid = request.args.get('sid', "")
    print sid   
       
    return jsonify(API.CancelSchedule(sid))    
