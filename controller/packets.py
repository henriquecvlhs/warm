###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import sys
import tos

NODE_CHARACTERISTICS_DESCRIPTION_PID            = 0
NODE_ASSOCIATE_CONFIRMATION_PID                 = 1
TASK_DESCRIPTION_REQUEST_PID                    = 2
TASK_DESCRIPTION_RESPONSE_PID                   = 2
PERIODIC_TASK_SCHEDULING_REQUEST_PID            = 3
PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID       = 3
INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID       = 4
INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID  = 4
TRIGGER_TASK_SCHEDULING_REQUEST_PID             = 5
TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID        = 5
SCHEDULED_TASK_CANCELATION_REQUEST_PID          = 6
SCHEDULED_TASK_CANCELATION_RESPONSE_PID         = 6
TASK_STATISTICS_REQUEST_PID                     = 7
TASK_STATISTICS_RESPONSE_PID                    = 7
NODE_STATISTICS_REQUEST_PID                     = 8
NODE_STATISTICS_RESPONSE_PID                    = 8


#default msg
class Msg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4)], 
                            packet)  

class NodeCharacteristicsDescriptionMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                             ('nid', 'bint', 10),
                             ('m', 'bint', 1),
                             ('e', 'bint', 1),
                             ('did', 'bint', 16),
                             ('os',  'bint', 8),
                             ('latitude',  'bint', 32), 
                             ('longitude', 'bint', 32),
                             ('height', 'bint', 32),
                             ('ptq', 'bint', 8), 
                             ('idq', 'bint', 8),
                             ('ntq', 'bint', 8),
                             ('ntn', 'bint', 5),
                             ('x', 'bint', 3),
                             ('tid', 'list', 2)],
                            packet)

class NodeAssociateConfirmationMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('flowid', 'bint', 16)],
                            packet)

class TaskDescriptionRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid',  'bint', 16)],
                            packet)  


class TaskDescriptionResponseMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid',  'bint', 16),
                            ('tti',  'bint', 2),
                            ('x1', 'bint', 2),
                            ('msq', 'bint', 8),
                            ('odf', 'bint', 10),
                            ('ipn', 'bint', 4),
                            ('x2', 'bint', 2),
                            ('d', 'bint', 1),
                            ('a', 'bint', 1),
                            ('g', 'bint', 1),
                            ('s', 'bint', 1),
                            ('x3', 'bint', 12),
                            ('ipq_ipf', 'list', 2)],
                            packet)  

class PeriodicTaskSchedulingRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('x', 'bint', 4),
                            ('ta', 'bint', 2),
                            ('odt', 'bint', 3),
                            ('odl', 'bint', 2),
                            ('p', 'bint', 1),
                            ('tp', 'bint', 32),
                            ('ofi', 'bint', 16)],
                            packet)

#TODO: how to deal with LIST: IFI[xIPN] PN[xIPN] ARG[x31] 0x00
class InstantaneousTaskSchedulingRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('ipn', 'bint', 4),
                            ('ta', 'bint', 2),
                            ('odt',  'bint', 3),
                            ('odl', 'bint', 2),
                            ('p', 'bint', 1),
                            ('ptw', 'bint', 32),
                            ('ofi', 'bint', 16),
                            ('LIST', 'blob', None)],
                            packet)

class TriggerTaskSchedulingRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('tmc', 'bint', 3),
                            ('tsc', 'bint', 3),
                            ('tcs',  'bint', 3),
                            ('trc', 'bint', 3),
                            ('tmi', 'bint', 64),
                            ('tsi', 'bint', 64),
                            ('tri', 'bint', 64),
                            ('ofi', 'bint', 16)],
                            packet)

class TaskSchedulingConfirmationMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('x1', 'bint', 4),
                            ('tin', 'bint', 8),
                            ('s', 'bint', 1), 
                            ('t', 'bint', 1), 
                            ('i', 'bint', 1), 
                            ('n', 'bint', 1), 
                            ('p', 'bint', 1), 
                            ('d', 'bint', 1), 
                            ('r', 'bint', 1), 
                            ('x2', 'bint', 1)],
                            packet)
 

class ScheduledTaskCancelationRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('tin', 'bint', 8)],
                            packet)


class ScheduledTaskCancelationResponseMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('tin', 'bint', 8),
                            ('s', 'bint', 1)],
                            packet)

class TaskStatisticsRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('x', 'bint', 4),
                            ('tin', 'bint', 8)],
                            packet)

class TaskStatisticsResponseMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('tid', 'bint', 16),
                            ('x', 'bint', 4),
                            ('tin', 'bint', 8),
                            ('ten', 'bint', 16)],
                            packet)


class NodeStatisticsRequestMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4)],
                            packet) 

class NodeStatisticsResponseMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('pid', 'bint', 4),
                            ('cpq', 'bint', 8),
                            ('ciq', 'bint', 8),
                            ('cnq', 'bint', 8),
                            ('x', 'bint', 4),
                            ('cbl', 'bint', 8),
                            ('crp', 'bint', 8),
                            ('latitude', 'bint', 32),
                            ('longitude', 'bint', 32),
                            ('height', 'bint', 32),
                            ('csq', 'blob', None)],
                            packet)

class SerialMessagePacket(tos.Packet):
    def __init__(self, packet=None):
        tos.Packet.__init__(self,
                            [('nodeid', 'bint', 16),
                            ('flowid', 'bint', 16),
                            ('len', 'bint', 8),
                            ('data', 'blob', None)],
                            packet)
