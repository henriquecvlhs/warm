###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

class Err:
    """
    Error codes from the controller.
    """
    ERR_OK = 0
    ERR_NODE_DOES_NOT_EXIST = -1
    ERR_TASK_DOES_NOT_EXIST = -2
    ERR_NODE_DOES_NOT_HAVE_TASK = -4
    ERR_MAX_NUMBER_TASK_INSTANCES_REACHED = -5
    ERR_DATASOURCE_ALREADY_IN_USE = -6
    ERR_FAILED_TO_SEND_MESSAGE_TO_CONTROLLER = -7
    ERR_INVALID_CONTROLLER_MESSAGE = -8
    ERR_FAILED_TO_CONNECT_TO_CONTROLLER = -9
    ERR_TASK_NOT_PERIODIC = -10
    ERR_TASK_NOT_INSTANT = -11
    ERR_PID_DOES_NOT_EXIST = -13
    ERR_SCHEDULING_DOES_NOT_EXIST = -14
    ERR_INPUT_TASK = -15
    ERR_OUTPUT_TASK = -16
    ERR_MAX_PERIODIC_TASK_REACHED = -17
    ERR_MAX_INSTANT_TASK_REACHED = -18
    ERR_MAX_TRIGGER_TASK_REACHED = -19
    ERR_FAILED_TO_SCHEDULE_TASK = -20
    ERR_FAILED_TO_CANCEL_TASK = -21
    ERR_DATA_SOURCE_DOES_NOT_EXIST = -22
    ERR_REFERENCE_ALREADY_EXIST = -23
    ERR_ADDRESS_DOES_NOT_EXIST = -24
    ERR_REFERENCE_ALREADY_IN_USE = -25
    ERR_TID_NID_SID_REFERENCE_NOT_GIVEN = -26
    ERR_NOT_ENOUGH_PARAMETERS = -27
    ERR_SID_NOT_GIVEN = -28


    err_dict = {                    \
    '0': 'OK',                      \
    '-1': 'Node does not exist.',    \
    '-2': 'Task does not exist.',    \
    '-4': 'Node does not have this task.',    \
    '-5': 'Maximum number of task instances reached.',    \
    '-6': 'Data source already in use.',    \
    '-7': 'Failed to send message to controller.',    \
    '-8': 'Invalid controller message.',    \
    '-9': 'Failed to connect to controller.',    \
    '-10': 'Task is not periodic.',    \
    '-11': 'Task is not instantaneous.',    \
    '-13': 'PID does not exist.',    \
    '-14': 'Scheduling does not exist.',    \
    '-15': 'Input task is wrong.',    \
    '-16': 'Output task is wrong.',    \
    '-17': 'Maximum number of periodic task reached.',    \
    '-18': 'Maximum number of instantaneous task reached.',    \
    '-19': 'Maximum number of trigger task reached.',    \
    '-20': 'Failed to schedule task.',    \
    '-21': 'Failed to cancel task.',    \
    '-22': 'Data source does not exist.',    \
    '-23': 'Reference does not exist.',    \
    '-24': 'Address does not exist.',    \
    '-25': 'Reference already in use.',    \
    '-26': 'TID, NID or Reference not given.',    \
    '-27': 'There is no enough parameters.',    \
    '-28': 'SID not given.',    \
     }

    def toStr(self, err=0):
        return self.err_dict.get(str(err), "")
 
