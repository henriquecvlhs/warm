###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import task_mapping
import network_monitor
from packets import *
from errors import *
import DAO

from populateDB import *




def initializeTests():
    DAO.startDAO(True)
    populateTestDatabase(True)
    network_monitor.testing_mode=True

def testNode():
    msg = NodeCharacteristicsDescriptionMsg()
    msg.pid = 0
    msg.nid = 11
    msg.m = 1
    msg.e = 1
    msg.did = 7
    msg.os = 7
    msg.latitude = 16
    msg.longitude = 16
    msg.height = 16
    msg.ptq = 7
    msg.idq = 7
    msg.ntq = 7
    msg.ntn = 0
    msg.x = 0
    msg.tid = [1, 2, 3]
    payload = msg.payload()
    print "NodeCharacteristicsDescriptionMsg: test payload = ", payload
    network_monitor.read(payload, msg.nid)
    node = DAO.dao.query(DAO.Sensor_Node).filter_by(node_id=msg.nid).first()
    if not node:
        print "ERRO: NodeCharacteristicsDescriptionMsg: Node was not created"
        return
    if node.latitude != msg.latitude:
        print "ERRO: NodeCharacteristicsDescriptionMsg: Attribute latitude is wrong"
        return

    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=msg.nid).all()
    for r in relationship:
        if r.task_id not in msg.tid:
            print "ERRO: NodeCharacteristicsDescriptionMsg: relationship is wrong"
    print "NodeCharacteristicsDescriptionMsg: done testing!"


def testTask():
    nid = 11
    msg = TaskDescriptionResponseMsg()
    msg.pid = TASK_DESCRIPTION_RESPONSE_PID
    msg.tid = 1
    msg.tti = 1
    msg.msq = 13
    msg.odf = 518 #0bx1000000110, 0x206
    msg.ipn = 1
    msg.d = 1
    msg.a = 0 
    msg.g = 0
    msg.s = 0
    msg.ipq_ipf = [5638] #ipq = 5 => 0b101 / ipf = 206 => 0bx1000000110
    msg.x1 = 0
    msg.x2 = 0
    msg.x3 = 0
    payload = msg.payload()
    print "TaskDescriptionResponseMsg: test payload = ", payload
    network_monitor.read(payload, nid)
    task = DAO.dao.query(DAO.Task).filter_by(task_id=msg.tid).first()
    if not task:
        print "ERRO: TaskDescriptionResponseMsg: Task was not created"
        return
    if task.task_type_id != msg.tti:
        print "ERRO: TaskDescriptionResponseMsg: Attribute task_type_id is wrong"
      
    relationship = DAO.dao.query(DAO.Sensor_Node_Task_Relationship).filter_by(node_id=nid, task_id=msg.tid).first()
    if not relationship:
        print "ERRO: TaskDescriptionResponseMsg: relationship was not created"
        return

    if relationship.max_scheduled_task_instances != msg.msq:
        print "ERRO: TaskDescriptionResponseMsg: relationship is wrong"

    parameters = DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_id=msg.tid).all()
    if len(parameters) != msg.ipn + 1:
        print "ERRO: TaskDescriptionResponseMsg: number of parameters created is wrong"

    inputParameter =  DAO.dao.query(DAO.Task_In_Out_Parameter).filter_by(task_id=msg.tid, task_parameter_id=0).first() 
    if not inputParameter:
        print "ERRO: TaskDescriptionResponseMsg: inputParameter was not created"
        return

    if inputParameter.maximum_qtty_per_task_execution != 5:
        print "ERRO: TaskDescriptionResponseMsg: maximum_qtty_per_task_execution is wrong"
    if inputParameter.support_16bits != True:
        print "ERRO: TaskDescriptionResponseMsg: support_16bits is wrong"
    if inputParameter.support_unsigned_integer != False:
        print "ERRO: TaskDescriptionResponseMsg: support_unsigned_integer is wrong"

    print "TaskDescriptionResponseMsg: done testing!"
     

def testSchedule():
    nid = 11
    msg = TaskSchedulingConfirmationMsg()
    msg.pid = INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID
    msg.tid = 1
    msg.tin = 1
    msg.s = 1
    msg.t = 0
    msg.i = 0
    msg.n = 0
    msg.p = 0 
    msg.d = 0
    msg.r = 0
    msg.x1 = 0 
    msg.x2 = 0
    
    payload = msg.payload()
    print "TaskSchedulingConfirmationMsg: test payload = ", payload
    erro, tin = network_monitor.read(payload, nid)
    if erro != Err.ERR_OK:
        print "ERRO: TaskSchedulingConfirmationMsg: ERR_OK is wrong"

    #Change msg
    msg.s = 0
    msg.i = 1
    msg.d = 1
   
    payload = msg.payload()
    print "TaskSchedulingConfirmationMsg: test payload = ", payload
    erro, tin = network_monitor.read(payload, nid)
    if erro != Err.ERR_INPUT_TASK:
        print "ERRO: TaskSchedulingConfirmationMsg: ERR_INPUT_TASK is wrong"    
    print "TaskSchedulingConfirmationMsg: done testing!"

def testCancelation():
    nid = 5

    msg = ScheduledTaskCancelationResponseMsg()
    msg.pid = SCHEDULED_TASK_CANCELATION_RESPONSE_PID
    msg.tid = 1
    msg.tin = 1
    msg.s = 0
    
    payload = msg.payload()
    print "ScheduledTaskCancelationResponseMsg: test payload = ", payload
    erro, tin = network_monitor.read(payload, nid)
    if erro == Err.ERR_OK:
        print "ERRO: ScheduledTaskCancelationResponseMsg: ERR_OK is wrong"

    #Check if schedule was not deleted 
    #schedule = DAO.dao.query(DAO.Task_Scheduling).filter_by(node_id=nid, task_id=msg.tid, scheduling_instance_number=msg.tin).first()
    #if not schedule:
    #    print "ERRO: ScheduledTaskCancelationResponseMsg: schedule was deleted" 


    msg = ScheduledTaskCancelationResponseMsg()
    msg.pid = SCHEDULED_TASK_CANCELATION_RESPONSE_PID
    msg.tid = 1
    msg.tin = 1
    msg.s = 1
    
    payload = msg.payload()
    print "ScheduledTaskCancelationResponseMsg: test payload = ", payload
    erro, tin = network_monitor.read(payload, nid)
    if erro != Err.ERR_OK:
        print "ERRO: ScheduledTaskCancelationResponseMsg: ERR_OK is wrong"

    #Check if schedule was deleted 
    #schedule = DAO.dao.query(DAO.Task_Scheduling).filter_by(node_id=nid, task_id=msg.tid, scheduling_instance_number=msg.tin).first()
    #if schedule:
    #    print "ERRO: ScheduledTaskCancelationResponseMsg: schedule was not deleted" 

    print "ScheduledTaskCancelationResponseMsg: done testing!" 

def testRoutine():
    initializeTests()
    #testNode()
    testTask()
    testSchedule()
    testCancelation()


if __name__=='__main__':
    testRoutine()
