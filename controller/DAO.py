###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from sqlalchemy import create_engine, Column, Integer, Boolean, String, Float, ForeignKey, and_
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
import threading


Base = declarative_base()


"""=====================DAO======================="""

class DAO:  

    """DAO"""
    def __init__(self, debug=False):
        if debug:
            self.engine = create_engine('sqlite:///test_db.db',
                    connect_args={'check_same_thread':False}, echo=False)
            Base.metadata.drop_all(self.engine)
        else:
            self.engine = create_engine('sqlite:///warm_v1.db',
                    connect_args={'check_same_thread':False}, echo=False)
        Base.metadata.create_all(self.engine)

    def GetSession(self):
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        return self.session


"""==================Entities===================="""


"""-----------------Node Group-------------------"""

class Sensor_Node(Base):

	"""Sensor_Node"""

	__tablename__ = 'sensor_node'

	node_id = Column(Integer, primary_key=True)
	device_id = Column(Integer, ForeignKey('device.device_id'))
	os_id = Column(Integer, ForeignKey('operating_system.os_id'))
	mobility = Column(Boolean)
	energy_autonomy = Column(Boolean)
	latitude = Column(Float)
	longitude = Column(Float)
	height = Column(Float)
	periodic_task_qtty = Column(Integer)
	max_periodic_task_qtty = Column(Integer)
	network_task_qtty = Column(Integer)
	max_network_task_qtty = Column(Integer)
	input_data_qtty = Column(Integer)
	max_input_data_qtty = Column(Integer)
	occupied_ram_percentage  = Column(Integer)
	current_battery_level = Column(Integer)

	sensor_node_task_relationship = relationship("Sensor_Node_Task_Relationship", backref="node")
	task_scheduling = relationship("Task_Scheduling", backref="node")
	control_flows = relationship("Control_Flows", backref="node")

	def __repr__(self):
		return "<Sensor_Node(node_id=%d)>" % (self.node_id)




class Device(Base):

	"""Device"""

	__tablename__ = 'device'

	device_id = Column(Integer, primary_key=True)
	name = Column(String)
	description = Column(String)	

	node = relationship("Sensor_Node", backref="device")
	
	def __repr__(self):
		return "<Device(device_id=%d, name=%s)>" % (self.device_id, self.name)



class Operating_System(Base):

	"""Operating_System"""

	__tablename__ = 'operating_system'

	os_id = Column(Integer, primary_key=True)
	name = Column(String)
	description = Column(String)	

	node = relationship("Sensor_Node", backref="operating_system")

	def __repr__(self):
		return "<Operating_System(os_id=%d, name=%s)>" % (self.os_id, self.name)


class Node_Connection(Base):

    """Node_Connection"""

    __tablename__ = 'node_connection'

    node_id = Column(Integer, ForeignKey(Sensor_Node.node_id), primary_key=True)
    neighbour_node_id = Column(Integer, ForeignKey(Sensor_Node.node_id), primary_key=True)
    link_strength = Column(Integer)	

    node_from = relationship("Sensor_Node",
            foreign_keys='Node_Connection.node_id')
    node_to = relationship("Sensor_Node",
            foreign_keys='Node_Connection.neighbour_node_id')

    def __repr__(self):
        return "<Node_Connection(node_id=%d, neighbour_node_id=%d)>" % (self.node_id, self.neighbour_node_id)


"""--------------------Task Group-----------------------"""

class Sensor_Node_Task_Relationship(Base):

	"""Sensor_Node_Task_Relationship"""

	__tablename__ = 'sensor_node_task_relationship'

	node_id = Column(Integer, ForeignKey('sensor_node.node_id'), primary_key=True)
	task_id = Column(Integer, ForeignKey('task.task_id'), primary_key=True)
	currently_scheduled_task_instances = Column(Integer)	
	max_scheduled_task_instances = Column(Integer)

	def __repr__(self):
		return "<Sensor_Node_Task_Relationship(node_id=%d, task_id=%d)>" % (self.node_id, self.task_id)

class Task(Base):

	"""Task"""

	__tablename__ = 'task'

	task_id = Column(Integer, primary_key=True)
	description = Column(String)
	task_type_id = Column(Integer, ForeignKey('task_type.task_type_id'))
	generates_data = Column(Boolean)	
	controls_actuator = Column(Boolean)
	aggregates_data = Column(Boolean)
	data_sink = Column(Boolean)

	sensor_node_task_relationship = relationship("Sensor_Node_Task_Relationship", backref="task")
	task_in_out_parameter = relationship("Task_In_Out_Parameter", backref="task")
	task_scheduling_in_out_relationship = relationship("Task_Scheduling_In_Out_Relationship", backref="task")
	task_scheduling = relationship("Task_Scheduling", backref="task")

	def __repr__(self):
		return "<Task(task_id=%d, description=%s)>" % (self.task_id, self.description)


class Task_Type(Base):

	"""Task_Type"""

	__tablename__ = 'task_type'

	task_type_id = Column(Integer, primary_key=True)
	description = Column(String)

	task = relationship("Task", backref="task_type")	

	def __repr__(self):
		return "<Task_Type(task_type_id=%d, description=%s)>" % (self.task_type_id, self.description)


class Task_In_Out_Parameter(Base):

	"""Task_In_Out_Parameter"""

	__tablename__ = 'task_in_out_parameter'

	task_id = Column(Integer, ForeignKey('task.task_id'), primary_key=True)
	task_parameter_id = Column(Integer, primary_key=True)
	description = Column(String)
	input = Column(Boolean)
	maximum_qtty_per_task_execution = Column(Integer)
	support_floating_point = Column(Boolean)
	support_fixed_point = Column(Boolean)
	support_integer = Column(Boolean)
	support_unsigned_integer = Column(Boolean)
	support_boolean = Column(Boolean)
	support_bit_array = Column(Boolean)
	support_8bits = Column(Boolean)
	support_16bits = Column(Boolean)
	support_32bits = Column(Boolean)
	support_64bits = Column(Boolean)

	def __repr__(self):
		return "<Task_In_Out_Parameter(task_id=%d, task_parameter_id=%d)>" % (self.task_id, self.task_parameter_id)



"""-----------------Scheduling Group--------------------"""

class Task_Scheduling(Base):

	"""Task_Scheduling"""

	__tablename__ = 'task_scheduling'

	task_scheduling_id = Column(Integer, primary_key=True)
	scheduling_instance_number = Column(Integer)
	task_chain_id = Column(Integer, ForeignKey('task_chain.task_chain_id'))
	task_id = Column(Integer, ForeignKey('task.task_id'))
	node_id = Column(Integer, ForeignKey('sensor_node.node_id'))
	exection_qtty = Column(Integer)
	execution_period = Column(Integer)
	parameter_validity_period = Column(Integer)
	period_precision_millis_micro = Column(Boolean)
	trigger_comparison_id = Column(Integer, ForeignKey('trigger_comparison.trigger_comparison_id'))
	trigger_minuend_rule_id = Column(Integer, ForeignKey('trigger_rule.trigger_rule_id'))
	trigger_subtrahend_rule_id = Column(Integer, ForeignKey('trigger_rule.trigger_rule_id'))
	trigger_reference_rule_id = Column(Integer, ForeignKey('trigger_rule.trigger_rule_id'))
	trigger_minuend_init = Column(Integer)
	trigger_subtrahend_init = Column(Integer)
	trigger_reference_init = Column(Integer)

	task_scheduling_in_out_relationship = relationship("Task_Scheduling_In_Out_Relationship", backref="task_scheduling")

	trigger_minuend_rule = relationship('Trigger_Rule', foreign_keys=trigger_minuend_rule_id)
	trigger_subtrahend_rule = relationship('Trigger_Rule', foreign_keys=trigger_subtrahend_rule_id)
	trigger_reference_rule = relationship('Trigger_Rule', foreign_keys=trigger_reference_rule_id)

	def __repr__(self):
		return "<Task_Scheduling(task_scheduling_id=%d)>" % (self.task_scheduling_id)


class Trigger_Rule(Base):

	"""Trigger_Rule"""

	__tablename__ = 'trigger_rule'

	trigger_rule_id = Column(Integer, primary_key=True)
	description = Column(String)

	#task_scheduling = relationship("Task_Scheduling", backref="trigger_rule", foreign_keys=)		

	def __repr__(self):
		return "<Trigger_Rule(trigger_rule_id=%d, description=%s)>" % (self.trigger_rule_id, self.description)

class Trigger_Comparison(Base):

	"""Trigger_Comparison"""

	__tablename__ = 'trigger_comparison'

	trigger_comparison_id = Column(Integer, primary_key=True)
	description = Column(String)
	
	task_scheduling = relationship("Task_Scheduling", backref="trigger_comparison")	

	def __repr__(self):
		return "<Trigger_Comparison(trigger_comparison_id=%d, description=%s)>" % (self.trigger_comparison_id, self.description)

class Task_Scheduling_In_Out_Relationship(Base):

	"""Task_Scheduling_In_Out_Relationship"""

	__tablename__ = 'task_scheduling_in_out_relationship'

	task_scheduling_id = Column(Integer, ForeignKey('task_scheduling.task_scheduling_id'), primary_key=True)
	task_id = Column(Integer, ForeignKey('task.task_id'), primary_key=True)
	task_parameter_id = Column(Integer, ForeignKey('task_in_out_parameter.task_parameter_id'), primary_key=True)
	sdn_flow_id = Column(Integer)
	reference = Column(String)
	qtty_per_task_execution = Column(Integer)
	scheduled_data_type_id = Column(Integer, ForeignKey('data_type.data_type_id'))
	scheduled_data_length_id = Column(Integer, ForeignKey('data_length.data_length_id'))
		

	def __repr__(self):
		return "<Task_Scheduling_In_Out_Relationship(task_scheduling_id=%d, task_id=%d, task_parameter_id=%d, sdn_flow_id=%d, reference=%s)>\n" % (self.task_scheduling_id, self.task_id, self.task_parameter_id, self.sdn_flow_id, self.reference)


class Data_Type(Base):

	"""Data_Type"""

	__tablename__ = 'data_type'

	data_type_id = Column(Integer, primary_key=True)
	description = Column(String)

	task_scheduling_in_out_relationship = relationship("Task_Scheduling_In_Out_Relationship", backref="data_type")
		

	def __repr__(self):
		return "<Data_Type(data_type_id=%d, description=%s)>" % (self.data_type_id, self.description)


class Data_Length(Base):

	"""Data_Length"""

	__tablename__ = 'data_length'

	data_length_id = Column(Integer, primary_key=True)
	description = Column(String)

	task_scheduling_in_out_relationship = relationship("Task_Scheduling_In_Out_Relationship", backref="data_length")
		

	def __repr__(self):
		return "<Data_Length(data_length_id=%d, description=%s)>" % (self.data_length_id, self.description)


class Task_Chain(Base):

	"""Task_Chain"""

	__tablename__ = 'task_chain'

	task_chain_id = Column(Integer, primary_key=True)
	description = Column(String)

	task_scheduling = relationship("Task_Scheduling", backref="task_chain")
	application_task_chain_relationship = relationship("Application_Task_Chain_Relationship", backref="task_chain")	

	def __repr__(self):
		return "<Task_Chain(task_chain_id=%d, description=%s)>" % (self.task_chain_id, self.description)


class Application_Task_Chain_Relationship(Base):

	"""Application_Task_Chain_Relationship"""

	__tablename__ = 'application_task_chain_relationship'

	application_id = Column(Integer, ForeignKey('application.application_id'), primary_key=True)
	task_chain_id = Column(Integer,ForeignKey('task_chain.task_chain_id'), primary_key=True)
	

	def __repr__(self):
		return "<Application_Task_Chain_Relationship(application_id=%d, task_chain_id=%d)>" % (self.application_id, self.task_chain_id)


class Application(Base):

	"""Application"""

	__tablename__ = 'application'

	application_id = Column(Integer, primary_key=True)
	description = Column(String)

	application_task_chain_relationship = relationship("Application_Task_Chain_Relationship", backref="application")
		

	def __repr__(self):
		return "<Application(application_id=%d, description=%s)>" % (self.application_id, self.description)


class Control_Flows(Base):

	"""Control_Flows"""

	__tablename__ = 'control_flows'

	node_id = Column(Integer, ForeignKey('sensor_node.node_id'), primary_key=True)
	sdn_flow_id = Column(Integer, primary_key=True)
	incoming_outrgoing = Column(Boolean)
		

	def __repr__(self):
		return "<Control_Flows(node_id=%d, sdn_flow_id=%d)>" % (self.node_id, self.sdn_flow_id)

dao = []
daoLock = threading.RLock()
def startDAO(debug=False):
    global dao
    dao = DAO(debug=debug).GetSession()
