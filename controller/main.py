###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import REST_server
import network_abstraction_layer
import sys
import DAO

import task_mapping_test

def initModules():
    """
    Initializes the controller's modules.
    """
    # Start DAO
    DAO.startDAO()
    # Start network abstraction layer
    # TODO: Receive parameters for connection to the SDN Controller
    # TODO: Check for errors when trying to connect to the controller
    network_abstraction_layer.connectToController()
    # Start REST server
    REST_server.start_flask()
    # TODO: Start network interface


if __name__ == '__main__':
    initModules()
