###############################################################################
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import task_mapping
import errors
import DAO

def populateTestDatabase():
    # Devices
    device1 = DAO.Device(name="Unknown", description="unknown")
    device2 = DAO.Device(name="Arduino", description="arduino")
    DAO.dao.add(device1)
    DAO.dao.add(device2)

    # OSs
    os1 = DAO.Operating_System(name="Unknown", description="unknown")
    os2 = DAO.Operating_System(name="TinyOS", description="tinyos")
    DAO.dao.add(os1)
    DAO.dao.add(os2)

    # Nodes
    node1 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True,
            energy_autonomy=False, latitude=0.5, longitude=1.5, height=1.5)
    node2 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=False,
            energy_autonomy=False, latitude=2.0, longitude=1.0, height=1.0)
    node3 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
            energy_autonomy=True, latitude=2.0, longitude=2.0, height=2.0)
    node4 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
            energy_autonomy=True, latitude=6.0, longitude=2.0, height=2.0)
    node5 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
            energy_autonomy=True, latitude=5.0, longitude=8.0, height=1.0)
    node6 = DAO.Sensor_Node(device_id=1, os_id=1, mobility=True, 
            energy_autonomy=True, latitude=5.0, longitude=8.0, height=1.0)
    DAO.dao.add(node1)
    DAO.dao.add(node2)
    DAO.dao.add(node3)
    DAO.dao.add(node4)
    DAO.dao.add(node5)
    DAO.dao.add(node6)
    # Node connections updateNetworkTopology do that
    nodeConn1 = DAO.Node_Connection(node_id=1, neighbour_node_id=2)
    nodeConn2 = DAO.Node_Connection(node_id=2, neighbour_node_id=5)
    nodeConn3 = DAO.Node_Connection(node_id=3, neighbour_node_id=4)
    nodeConn4 = DAO.Node_Connection(node_id=1, neighbour_node_id=3)
    nodeConn5 = DAO.Node_Connection(node_id=5, neighbour_node_id=4)
    DAO.dao.add(nodeConn1)
    DAO.dao.add(nodeConn2)
    DAO.dao.add(nodeConn3)
    DAO.dao.add(nodeConn4)
    DAO.dao.add(nodeConn5)

    # Task types
    task_type1 = DAO.Task_Type(description="instant")
    task_type2 = DAO.Task_Type(description="periodic")
    DAO.dao.add(task_type1)
    DAO.dao.add(task_type2)

    # Tasks
    task1 = DAO.Task(description="Average", task_type_id=1)
    task2 = DAO.Task(description="Temperature Sensor", task_type_id=2)
    DAO.dao.add(task1)
    DAO.dao.add(task2)

    # Tasks relationships
    # There is already an average task running on the node
    nodetask1 = DAO.Sensor_Node_Task_Relationship(node_id=5, task_id=1,
            currently_scheduled_task_instances=1,
            max_scheduled_task_instances=2)
    nodetask2 = DAO.Sensor_Node_Task_Relationship(node_id=1, task_id=2,
            currently_scheduled_task_instances=0,
            max_scheduled_task_instances=3)
    nodetask3 = DAO.Sensor_Node_Task_Relationship(node_id=2, task_id=2,
            currently_scheduled_task_instances=0,
            max_scheduled_task_instances=3)
    nodetask4 = DAO.Sensor_Node_Task_Relationship(node_id=3, task_id=2,
            currently_scheduled_task_instances=0,
            max_scheduled_task_instances=3)
    nodetask5 = DAO.Sensor_Node_Task_Relationship(node_id=4, task_id=2,
            currently_scheduled_task_instances=0,
            max_scheduled_task_instances=3)
    nodetask6 = DAO.Sensor_Node_Task_Relationship(node_id=5, task_id=2,
            currently_scheduled_task_instances=0,
            max_scheduled_task_instances=2)
    DAO.dao.add(nodetask1)
    DAO.dao.add(nodetask2)
    DAO.dao.add(nodetask3)
    DAO.dao.add(nodetask4)
    DAO.dao.add(nodetask5)
    DAO.dao.add(nodetask6)

    # Task parameters
    taskparam1 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=1,
            input=True, support_16bits=True, support_integer=True)
    taskparam2 = DAO.Task_In_Out_Parameter(task_parameter_id=1, task_id=1,
            input=False, support_16bits=True, support_integer=True)
    taskparam3 = DAO.Task_In_Out_Parameter(task_parameter_id=0, task_id=2,
            input=False, support_16bits=True, support_integer=True)
    DAO.dao.add(taskparam1)
    DAO.dao.add(taskparam2)
    DAO.dao.add(taskparam3)

    # Task scheduling
    # A single average task
    taskscheduling1 = DAO.Task_Scheduling(task_id=1, node_id=5)
    DAO.dao.add(taskscheduling1)

    # Task Scheduling Relationship
    taskrelation1 = DAO.Task_Scheduling_In_Out_Relationship(
            task_scheduling_id=1, task_id=1, task_parameter_id=0,
            sdn_flow_id=-1, reference="temp1")
    taskrelation2 = DAO.Task_Scheduling_In_Out_Relationship(
            task_scheduling_id=1, task_id=1, task_parameter_id=1,
            sdn_flow_id=1, reference="avg")
    DAO.dao.add(taskrelation1)
    DAO.dao.add(taskrelation2)

    # Commit all the changes
    DAO.dao.commit()


def initializeTests():
    DAO.startDAO(True)
    populateTestDatabase()
    task_mapping.testing_mode=True


def getPathTest():
    assert(task_mapping.getPath(1, 4) == [1, 3, 4])
    assert(task_mapping.getPath(1, 5) == [1, 2, 5])
    assert(task_mapping.getPath(3, 1) == [])


def defineFlowTest():
    assert(task_mapping.defineFlow(1, 4) == (2, [1, 3, 4]))
    assert(task_mapping.defineFlow(1, 5) == (2, [1, 2, 5]))
    assert(task_mapping.defineFlow(3, 1) == (2, []))


def setPeriodicTaskTest():
    # Standard task mapping, should create a flow from 2 to 5 and update db
    res, sid = task_mapping.setPeriodicTask(2, 2, "temp1", 10, 3600)
    assert(res == errors.Err.ERR_OK)
    # Standard task mapping, should create a flow from 1 to 5 and update db
    res, sid  = task_mapping.setPeriodicTask(2, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_OK)
    # Trying to schedule a task that doesn't exist
    res, sid  = task_mapping.setPeriodicTask(3, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_TASK_DOES_NOT_EXIST)
    # Trying to schedule a task for a node that doesn't exist
    res, sid  = task_mapping.setPeriodicTask(3, 9, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_NODE_DOES_NOT_EXIST)
    # Trying to schedule a task that is not periodic
    res, sid  = task_mapping.setPeriodicTask(1, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_TASK_NOT_PERIODIC)
    # Standard task mapping, should create a flow from 1 to 5 and update db
    res, sid  = task_mapping.setPeriodicTask(2, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_OK)
    # Standard task mapping, should create a flow from 1 to 5 and update db
    res, sid  = task_mapping.setPeriodicTask(2, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_OK)
    # Standard task mapping, should return error because max amount of tasks exceeded
    res, sid  = task_mapping.setPeriodicTask(2, 1, "temp1", 20, 3600)
    assert(res == errors.Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED)
    # Standard task mapping, should not create a flow and update db
    res, sid  = task_mapping.setPeriodicTask(2, 2, "temp2", 20, 3600)
    assert(res == errors.Err.ERR_OK)
    # Standard task mapping, should not create a flow and update db
    res, sid  = task_mapping.setPeriodicTask(2, 3, "temp3", 20, 3600)
    assert(res == errors.Err.ERR_OK)
    # Use address and update other references
    res, sid  = task_mapping.setPeriodicTask(2, 4, "temp3", 20, 3600, "6")
    assert(res == errors.Err.ERR_OK)
    # Use address with diferent reference
    res, sid  = task_mapping.setPeriodicTask(2, 3, "temp4", 20, 3600, "6")
    assert(res == errors.Err.ERR_OK)
    # Should not use the same reference when that reference goes to 
    # a specific address
    res, sid  = task_mapping.setPeriodicTask(2, 4, "temp4", 20, 3600)
    assert(res == errors.Err.ERR_REFERENCE_ALREADY_IN_USE)
    # Standard task mapping, should not create a flow and update db
    res, sid  = task_mapping.setPeriodicTask(2, 2, "temp5", 20, 3600)
    assert(res == errors.Err.ERR_OK)



def setInstantTaskTest():

    # Trying to schedule a task that doesn't exist
    res, sid  = task_mapping.setInstantTask(3, 1, "avgRes1", "temp2", 3)
    assert(res == errors.Err.ERR_TASK_DOES_NOT_EXIST)
    # Trying to schedule a task for a node that doesn't exist
    res, sid  = task_mapping.setInstantTask(1, 9, "avgRes1", "temp2", 3)
    assert(res == errors.Err.ERR_NODE_DOES_NOT_EXIST)
    # Trying to schedule a task for a node that doesn't support it
    res, sid  = task_mapping.setInstantTask(1, 1, "avgRes1", "temp2", 3)
    assert(res == errors.Err.ERR_NODE_DOES_NOT_HAVE_TASK)

    # Try to schedule task with dataSource already in use 
    res, sid  = task_mapping.setInstantTask(1, 5, "avgRes1", "temp1", 3)
    assert(res == errors.Err.ERR_DATASOURCE_ALREADY_IN_USE)

    # TODO: Fix this test case, not working because the flows currently only
    # have one source and one destination.
    # Schedule task with different dataSource
    res, sid  = task_mapping.setInstantTask(1, 5, "avgRes1", "temp2", 3)
    assert(res == errors.Err.ERR_OK)

    # Try to schedule task with different dataSource, exceed amount
    res, sid  = task_mapping.setInstantTask(1, 5, "avgRes1", "temp3", 3)
    assert(res == errors.Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED)

    # Try to schedule task with different dataSource, exceed amount
    res, sid  = task_mapping.setInstantTask(1, 5, "avgRes1", "temp3", 3)
    assert(res == errors.Err.ERR_MAX_NUMBER_TASK_INSTANCES_REACHED)

    #Cancel tasks
    res = task_mapping.cancelTask(11)
    assert(res == errors.Err.ERR_OK)

    #Use address
    res, sid  = task_mapping.setInstantTask(1, 5, "avgRes1", "temp5", 3, "6")
    assert(res == errors.Err.ERR_OK)



def testRoutine():
    initializeTests()
    getPathTest()
    defineFlowTest()
    setPeriodicTaskTest()
    setInstantTaskTest()


if __name__=='__main__':
    testRoutine()
