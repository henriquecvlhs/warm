/**
 * @file SDNAbstractionLayerP.nc
 * @brief File implementing the SDN Abstraction Layer component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CC2420.h"
#include "Topology.h"

/**
 * @brief Module that implements SDN Abstraction Layer component's interfaces.
 *
 * @note This component abstracts an interface with the TinySDN middleware.
 */
module SDNAbstractionLayerP {
    provides {
        interface SDNReceive;
        interface SDNSend;
    }

    uses {
        interface Boot;

        interface SplitControl as RadioControl;
        interface StdControl as RoutingControl;

        interface AMSend as Send;
        interface Receive;
        interface TinySdnPacket;
    }
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

/** Buffer for sending packages */
message_t packageBuffer;

/** Lock for managing buffer */
uint8_t bufferLock = 0;

/************************************************************************
 * Auxiliary functions
 ************************************************************************/


/************************************************************************
 * Boot Interface 
 ************************************************************************/
    event void Boot.booted() {
        /** Start radio */
        call RadioControl.start();
    }

/************************************************************************
 * SDN Message Interface 
 ************************************************************************/

    command void SDNSend.send(uint16_t flowId, 
                              void *payload, 
                              uint8_t length) {
        error_t err;
        uint8_t i;
        uint8_t *payloadPointer;

        /** Send only if buffer is not already in use */
        if (bufferLock) {
            return;
        }

        /** Get pointer to payload inside of package buffer */
        payloadPointer = (uint8_t *) call Send.getPayload(&packageBuffer, length);

        if (payloadPointer == NULL) {
            return;
        }

        /** Copy payload to package buffer */
        for (i = 0; i < length; i++) {
            payloadPointer[i] = ((uint8_t *) payload)[i];
        }
        
        err = call Send.send(flowId, &packageBuffer, length);

        if (err == SUCCESS) {
            /** Hold lock */
            bufferLock = 1;
        }
    }

/************************************************************************
 * Radio Control Interface Events
 ************************************************************************/

    event void RadioControl.startDone(error_t err) {
        if (err == SUCCESS) {
            /** Start Routing Control */
            call RoutingControl.start();
        }
        else {
            /** Try to start radio again */
            call RadioControl.start();
        }
    }

    event void RadioControl.stopDone(error_t err) {
        /** Do nothing */
    }

/************************************************************************
 * Send Interface Events
 ************************************************************************/

    event void Send.sendDone(message_t *bufPtr, error_t error) {
        /** Release lock */
        bufferLock = 0;
    }

/************************************************************************
 * Receive Interface Events
 ************************************************************************/

    event message_t *Receive.receive(message_t *bufPtr,
                                     void *payload,
                                     uint8_t len) {
        uint16_t originId = call TinySdnPacket.getOrigin(bufPtr);
        uint16_t flowId = call TinySdnPacket.getDestination(bufPtr);

        signal SDNReceive.receive(originId, flowId, payload, len);
        return bufPtr;
    }
}
