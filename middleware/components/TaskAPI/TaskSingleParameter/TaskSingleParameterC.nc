/**
 * @file TaskSingleParameterC.nc
 * @brief File implementing the Task Single Parameter component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Check whether given task instance parameter is holding any data.
 *
 * @param taskInstance Task instance number.
 * @return Boolean informing parameter availability.
 */
#define AVAILABLE(taskInstance) \
        (availability[taskInstance >> 3] & (1 << (taskInstance & 0x07)))

/**
 * @brief Mark given task instance parameter as available (i.e. not holding
 *        any data).
 *
 * @param taskInstance Task instance number.
 */
#define MAKE_AVAILABLE(taskInstance) \
        (availability[taskInstance >> 3] |= (1 << (taskInstance & 0x07)))

/**
 * @brief Mark given task instance parameter as available (i.e. holding data).
 *
 * @param taskInstance Task instance number.
 */
#define MAKE_UNAVAILABLE(taskInstance) \
        (availability[taskInstance >> 3] &= ~(1 << (taskInstance & 0x07)))

/**
 * @brief Module that implements task single parameter interfaces.
 *
 * This module implements a single task input parameter, which is like
 * an input variable to be used by task instances during their execution.
 *
 * One of this components must be instanced and wired for every single input
 * parameter (i.e. input variable) that a task should receive from the network.
 * 
 * Once the component is instanced, every task instance is able to receive data
 * from the network through it and draw this data when executed.
 *
 * @param[in] parameter_t Input parameter type (e.g. uint8_t, int16_t).
 * @param[in] numberOfTaskInstances Maximum number of task instances to be
 *                                  supported by the input parameter.
 */
generic module TaskSingleParameterC(typedef parameter_t, 
                                    uint8_t numberOfTaskInstances) {
    provides {
        interface TaskSingleParameterAccess<parameter_t>;
        interface TaskParameterInput;
    }
}

implementation {
    /**
     * Stores values for this parameter for each of the task instances.
     */
    parameter_t currentValue[numberOfTaskInstances];

    /**
     * Indicates the availability for this parameter for each instance of it.
     * The (taskInstance%8)th bit in position taskInstance/8 of the array will
     * correspond to the availability of instance parameter.
     * If the bit is set, it means the parameter is available for usage.
     * If the bit is not set, it means it is empty (unavailable for usage).
     */
    uint8_t availability[1 + (numberOfTaskInstances - 1)/8];

    /**
     * Keeps track of the task input storage policy.
     */
    TaskInputParameterStoragePolicy_t storagePolicy = STORE_LEAST_RECENT_INPUT;

    command void TaskParameterInput.init() {
        memset(availability, 0, sizeof(availability));
    }

    command void TaskParameterInput.setInstanceSize(uint8_t taskInstance,
                                                    uint8_t size) {
        /** Do nothing since this is not an array of parameters */
        (void) taskInstance;
        (void) size;
    }
    
    command void TaskParameterInput.discard(uint8_t taskInstance) {
        availability[taskInstance >> 3] &= ~(1 << (taskInstance & 0x07));
    }

    command TaskInputParameterStatus_t TaskParameterInput.status(uint8_t taskInstance) {
        if (AVAILABLE(taskInstance)) {
            return INPUT_STATUS_READY;
        }
        else {
            return INPUT_STATUS_NOT_READY;
        }
    }

    command void TaskParameterInput.configureStoragePolicy(
                                            TaskInputParameterStoragePolicy_t policy) {
        storagePolicy = policy;
    }

    command void TaskParameterInput.receive(uint8_t taskInstance, 
                                            void *inputData) {
        /** Get data with correct data type */
        parameter_t data = *((parameter_t *) inputData);

        switch (storagePolicy)
        {
            case STORE_LEAST_RECENT_INPUT:
            default:
                /** Store only if nothing has been stored yet. */
                if (!AVAILABLE(taskInstance)) {
                    currentValue[taskInstance] = data;
                }
                break;

            case STORE_MOST_RECENT_INPUT:
                /** Always store incoming data */
                currentValue[taskInstance] = data;
                break;
        }

        MAKE_AVAILABLE(taskInstance);
        signal TaskParameterInput.ready(taskInstance);
    }

    command void TaskSingleParameterAccess.set(uint8_t taskInstance, parameter_t value) {
        currentValue[taskInstance] = value;
        MAKE_AVAILABLE(taskInstance);
    }

    command parameter_t TaskSingleParameterAccess.get(uint8_t taskInstance) {
        MAKE_UNAVAILABLE(taskInstance);
        return currentValue[taskInstance];
    }
}
