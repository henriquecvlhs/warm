/**
 * @file TaskAPIP.nc
 * @brief File implementing the Task API component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Task API component's interfaces.
 */
module TaskAPIP {
    provides {
        interface TaskScheduler;
        interface TaskInput;
        interface TaskOutput;
        interface TaskStatus;
        interface TaskDescription;
    }

    provides interface Init;

    uses interface Task[uint16_t taskId];
}
implementation {
    LOADED_TASK_IDS_ARRAY

/************************************************************************
 * Init Interface 
 ************************************************************************/
    command error_t Init.init() {
        uint16_t i;

        for (i = 0; i < sizeof(loadedTaskIds); i++) {
            call Task.init[loadedTaskIds[i]]();
        }

        return SUCCESS;
    }

/************************************************************************
 * Task Scheduler Interface 
 ************************************************************************/

    command uint8_t 
    TaskScheduler.schedulePeriodicTask(uint16_t taskId, 
                                       uint8_t *taskInstance,
                                       TaskTriggerPolicy_t triggerPolicy) {
        return call Task.schedule[taskId](taskInstance, triggerPolicy);
    }

    command uint8_t 
    TaskScheduler.scheduleInstantaneousTask(uint16_t taskId, 
                                            uint8_t *taskInstance,
                                            TaskTriggerPolicy_t triggerPolicy,
                                            uint8_t *inputSize,
                                            char *argumentString) {
        uint8_t inputId;
        uint8_t result;
        uint8_t numberOfInputs;

        /** Schedule */
        result = call Task.schedule[taskId](taskInstance, triggerPolicy);

        /** Configure input sizes */
        numberOfInputs = 
          ((TaskDescriptionResponse_t *) call Task.getDescription[taskId]())->ipn;

        for (inputId = 0; inputId < numberOfInputs; inputId++) {
            call Task.configureMaxInput[taskId](*taskInstance, 
                                                inputId, 
                                                inputSize[inputId]);
        }


        /** Configure argument string */
        if (argumentString) {
            call Task.configureArgumentString[taskId](*taskInstance, 
                                                      argumentString);
        }

        return result;
    }

    command uint8_t 
    TaskScheduler.scheduleTriggerTask(uint16_t taskId, 
                                      uint8_t *taskInstance,
                                      TaskTriggerComparisonRule_t triggerRule,
                                      TaskTriggerStorageRule_t minuendRule,
                                      TaskTriggerStorageRule_t subtrahendRule,
                                      TaskTriggerStorageRule_t referenceRule,
                                      void *minuendInitialization,
                                      void *subtrahendInitialization,
                                      void *referenceInitialization) {
        // TODO
        return 1;
    }

    command void TaskScheduler.cancelTask(uint16_t taskId, 
                                          uint8_t taskInstance) {
        call Task.cancel[taskId](taskInstance);
    }

    command void TaskScheduler.runTask(uint16_t taskId, uint8_t taskInstance) {
        call Task.run[taskId](taskInstance);
    }

    command void TaskScheduler.discardTaskData(uint16_t taskId, 
                                               uint8_t taskInstance) {
        call Task.discardData[taskId](taskInstance);
    }
    
/************************************************************************
 * Task Input Interface 
 ************************************************************************/

    command void TaskInput.data(uint16_t taskId, 
                                uint8_t taskInstance,
                                uint8_t inputId,
                                void *value) {
        call Task.receiveData[taskId](taskInstance, inputId, value);
    }

    command void TaskInput.trigger(uint16_t taskId, uint8_t taskInstance) {
        call Task.trigger[taskId](taskInstance);
    }

/************************************************************************
 * Task Output Interface 
 ************************************************************************/

/** This interface is composed only of event methods */

/************************************************************************
 * Task Status Interface 
 ************************************************************************/

    command uint16_t TaskStatus.getExecutionStatus(uint16_t taskId,
                                                   uint8_t taskInstance) {
        return call Task.getExecutionStatus[taskId](taskInstance);
    }

    command uint8_t TaskStatus.getScheduleStatus(uint16_t taskId, 
                                                 uint8_t taskInstance) {
        return call Task.isScheduled[taskId](taskInstance);
    }

/************************************************************************
 * Task Description Interface 
 ************************************************************************/

    command void *TaskDescription.getDescription(uint16_t taskId) {
        return call Task.getDescription[taskId]();
    }

    command uint16_t *TaskDescription.getLoadedTasks(
                                        uint8_t *numberOfLoadedTasks) {
        *numberOfLoadedTasks = (sizeof(loadedTaskIds)/2);
        return loadedTaskIds;
    }

/************************************************************************
 * Task Interface Event Implementations
 ************************************************************************/

    event   void Task.outputReady[uint16_t taskId](uint8_t taskInstance, 
                                                   void* value) {
        signal TaskOutput.ready(taskId, taskInstance, value);
    }

    event   void Task.readyToRun[uint16_t taskId](uint8_t taskInstance, 
                                                  TaskType_t taskType) {
        signal TaskScheduler.taskReadyToRun(taskId, 
                                            taskInstance, 
                                            taskType);
    }

/************************************************************************
 * Default Task Interface Implementations
 ************************************************************************/

    default command void 
    Task.run[uint16_t taskId](uint8_t taskInstance) {
    }

    default command void 
    Task.init[uint16_t taskId](void) {
    }

    default command void 
    Task.receiveData[uint16_t taskId](uint8_t taskInstance, 
                                      uint8_t parameterId, 
                                      void *value) {
    }

    default command void 
    Task.discardData[uint16_t taskId](uint8_t taskInstance) {
    }

    default command uint8_t 
    Task.schedule[uint16_t taskId](uint8_t *taskInstance, 
                                   TaskTriggerPolicy_t triggerPolicy) {
        return 1;
    }

    default command void 
    Task.configureMaxInput[uint16_t taskId](uint8_t taskInstance, 
                                            uint8_t parameterId, 
                                            uint8_t inputMaxSize) {
    }

    default command void 
    Task.configureArgumentString[uint16_t taskId](uint8_t taskInstance, 
                                                  char *argumentString) {
    }

    default command void 
    Task.cancel[uint16_t taskId](uint8_t taskInstance) {
    }

    default command void 
    Task.trigger[uint16_t taskId](uint8_t taskInstance) {
    }

    default command void 
    *Task.getDescription[uint16_t taskId](void) {
        return NULL;
    }

    default command uint16_t 
    Task.getExecutionStatus[uint16_t taskId](uint8_t taskInstance) {
        return 0;
    }

    default command uint8_t 
    Task.isScheduled[uint16_t taskId](uint8_t taskInstance) {
        return 0;
    }
}
