/**
 * @file TaskArrayParameterC.nc
 * @brief File implementing the Task Array Parameter component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Check whether given task instance array parameter index is holding 
 *        data.
 *
 * @param taskInstance Task instance number.
 * @param position Array index.
 * @return Boolean informing parameter availability.
 */
#define INDEX_AVAILABLE(taskInstance, position) \
    (availability[taskInstance][position >> 3] & (1 << (position & 0x07)))

/**
 * @brief Mark given task instance array parameter index as available
 *        (i.e. not holding any data).
 *
 * @param taskInstance Task instance number.
 * @param position Array index.
 */
#define MAKE_INDEX_AVAILABLE(taskInstance, position) \
    (availability[taskInstance][position >> 3] |= (1 << (position & 0x07)))

/**
 * @brief Mark given task instance array parameter index as unavailable 
 *        (i.e. holding data).
 *
 * @param taskInstance Task instance number.
 * @param position Array index.
 */
#define MAKE_INDEX_UNAVAILABLE(taskInstance, position) \
    (availability[taskInstance][position >> 3] &= ~(1 << (position & 0x07)))

/**
 * @brief Module that implements task array parameter interfaces.
 *
 * This module implements an array of task input parameters, which is like
 * an input array to be used by task instances during their execution.
 *
 * One of this components must be instanced and wired for every array of input
 * parameters (i.e. input array) that a task should receive from the network.
 * 
 * Once the component is instanced, every task instance is able to receive data
 * from the network through it and draw this data when executed.
 *
 * @param[in] parameter_t Input parameter type (e.g. uint8_t, int16_t).
 * @param[in] numberOfTaskInstances Maximum number of task instances to be
 *                                  supported by the input parameter.
 * @param[in] arraySize Maximum array size supported by input array parameter.
 */
generic module TaskArrayParameterC(typedef parameter_t,
                                   uint8_t numberOfTaskInstances,
                                   uint8_t arraySize) {
    provides {
        interface TaskArrayParameterAccess<parameter_t>;
        interface TaskParameterInput;
    }
}

implementation {
    /**
     * Stores arrays for this parameter for each of the task instances.
     */
    parameter_t array[numberOfTaskInstances][arraySize];
    
    /**
     * Indicates availability for every position of each array instance.
     * Availability is represented with bits: a set bit means the parameter
     * is available for usage, a clear bit means the parameter is not
     * available for usage.
     */
    uint8_t availability[numberOfTaskInstances][1 + (arraySize -1)/8];

    /**
     * Keeps track of the task input storage policy.
     */
    TaskInputParameterStoragePolicy_t storagePolicy = STORE_LEAST_RECENT_INPUT;

    /**
     * Keeps track of least recent value's position for each instance.
     */
    uint8_t leastRecentPosition[numberOfTaskInstances];

    /**
     * Keeps track of current max size for an instance's array.
     * When an array is filled up to its current max size, it will be 
     * considered as full.
     */
    uint8_t currentMaxSize[numberOfTaskInstances];

    command void TaskParameterInput.init(void) {
        uint8_t i;
        for (i = 0; i < numberOfTaskInstances; i++) {
            memset(availability[i], 0, sizeof(availability[i]));
        }

        memset(currentMaxSize, arraySize, sizeof(currentMaxSize));
        memset(leastRecentPosition, 0, sizeof(leastRecentPosition));
    }

    command void TaskParameterInput.setInstanceSize(uint8_t taskInstance, 
                                                    uint8_t size) {
        if (size <= arraySize) {
            currentMaxSize[taskInstance] = size;
        }
    }

    command void TaskParameterInput.discard(uint8_t taskInstance) {
        memset(availability[taskInstance], 0, sizeof(availability[taskInstance]));
        leastRecentPosition[taskInstance] = 0;
    }

    command TaskInputParameterStatus_t TaskParameterInput.status(
                                                         uint8_t taskInstance) {
        /** Count free positions. */
        uint8_t filledPositions = 0;
        uint8_t i;
        for (i = 0; i < currentMaxSize[taskInstance]; i++) {
            if (INDEX_AVAILABLE(taskInstance, i)) {
                filledPositions++;
            }
        }

        if (filledPositions == (currentMaxSize[taskInstance])) {
            return INPUT_STATUS_READY;
        }
        else {
            return INPUT_STATUS_NOT_READY;
        }
    }

    command void TaskParameterInput.configureStoragePolicy(
                                     TaskInputParameterStoragePolicy_t policy) {
        storagePolicy = policy;
    }

    command void TaskParameterInput.receive(uint8_t taskInstance, 
                                            void *inputData) {
        /** Get input data with correct data type */
        parameter_t data = *((parameter_t *) inputData);

        /** Search for first free position. */
        uint8_t freePosition = 0;
        uint8_t foundFreePosition = 0;
        uint8_t filledPositions = 0;
        uint8_t i;
        for (i = 0; i < currentMaxSize[taskInstance]; i++) {
            if (INDEX_AVAILABLE(taskInstance, i)) {
                filledPositions++;
            }
            else if (!foundFreePosition) {
                freePosition = i;
                foundFreePosition = 1;
            }
        }

        /** Check if array is full. */
        if (filledPositions == currentMaxSize[taskInstance]) {
            /** Store only if STORE_MOST_RECENT_INPUT policy is used. */
            if (storagePolicy == STORE_MOST_RECENT_INPUT) {
                array[taskInstance][leastRecentPosition[taskInstance]] = data;

                /** Update least recent position. */
                leastRecentPosition[taskInstance] += 1;
                leastRecentPosition[taskInstance] %= currentMaxSize[taskInstance];
            }

            /** Array is full, so signal parameter as ready */
            signal TaskParameterInput.ready(taskInstance);
            return;
        }
        else if (foundFreePosition) {
            /** Array is not full, so store value into free position */
            array[taskInstance][freePosition] = data;

            /** Keep track of availability. */
            MAKE_INDEX_AVAILABLE(taskInstance, freePosition);
        }

        /** If array became full with this input, signal it as ready. */
        if (filledPositions == (currentMaxSize[taskInstance] - 1)) {
            signal TaskParameterInput.ready(taskInstance);
        }
    }

    command void TaskArrayParameterAccess.set(uint8_t taskInstance, 
                                              uint8_t position, 
                                              parameter_t value) {
        if (position < currentMaxSize[taskInstance]) {
            array[taskInstance][position] = value;
            /** Set availability */
            MAKE_INDEX_AVAILABLE(taskInstance, position);
        }
    }

    command parameter_t TaskArrayParameterAccess.get(uint8_t taskInstance, 
                                                     uint8_t position) {
        /** Use module in case an invalid position is passed. */
        uint8_t usedPosition = position % currentMaxSize[taskInstance];
        MAKE_INDEX_UNAVAILABLE(taskInstance, usedPosition);

        return array[taskInstance][usedPosition];
    }

    command uint8_t TaskArrayParameterAccess.getInstanceSize(
                                                         uint8_t taskInstance) {
        return currentMaxSize[taskInstance];
    }
}
