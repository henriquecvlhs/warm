/**
 * @file TaskAPIC.nc
 * @brief File implementing the Task API component configuration.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskConfiguration.h"

/**
 * @brief Configuration for Task API component.
 */
configuration TaskAPIC {
    provides {
        interface TaskScheduler;
        interface TaskInput;
        interface TaskOutput;
        interface TaskStatus;
        interface TaskDescription;
    }
}
implementation {
    components MainC;
    components TaskAPIP;

    TASK_API_TASK_COMPONENTS

    MainC.SoftwareInit -> TaskAPIP.Init;

    TaskScheduler = TaskAPIP.TaskScheduler;
    TaskInput = TaskAPIP.TaskInput;
    TaskOutput = TaskAPIP.TaskOutput;
    TaskStatus = TaskAPIP.TaskStatus;
    TaskDescription = TaskAPIP.TaskDescription;

    TASK_API_TASK_INTERFACE_WIRINGS
}
