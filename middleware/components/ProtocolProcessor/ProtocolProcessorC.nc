/**
 * @file ProtocolProcessorC.nc
 * @brief File implementing the Protocol Processor component configuration.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Interval for trying to send association request */
#define ASSOCIATION_REQUEST_TRIAL_TIME_INTERVAL 6000

/** Mximum number of tries to send association request */
#define MAX_ASSOCIATION_REQUEST_TRIES 2

/**
 * @brief Configuration for Protocol Processor component.
 */
configuration ProtocolProcessorC {
    provides{
        interface ProtocolInput;
        interface ProtocolOutput;
    }

    uses {
        interface ProtocolScheduler;
        interface SchedulerStatus;

        interface TaskDescription;
        interface TaskStatus;

        interface ReceiverStatus;
        interface EmitterStatus;
    }
}
implementation {
    components MainC;
    components new VoltageC();
    components new TimerMilliC();

    components ProtocolProcessorP;

    ProtocolProcessorP.Boot -> MainC.Boot;
    ProtocolProcessorP.batteryVoltage -> VoltageC.Read;
    ProtocolProcessorP.timer -> TimerMilliC.Timer;

    ProtocolInput = ProtocolProcessorP.ProtocolInput;
    ProtocolOutput = ProtocolProcessorP.ProtocolOutput;

    ProtocolScheduler = ProtocolProcessorP.ProtocolScheduler;
    SchedulerStatus = ProtocolProcessorP.SchedulerStatus;

    TaskDescription = ProtocolProcessorP.TaskDescription;
    TaskStatus = ProtocolProcessorP.TaskStatus;

    ReceiverStatus = ProtocolProcessorP.ReceiverStatus;
    EmitterStatus = ProtocolProcessorP.EmitterStatus;
}

