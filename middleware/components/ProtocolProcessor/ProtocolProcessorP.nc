/**
 * @file ProtocolProcessorP.nc
 * @brief File implementing the Protocol Processor component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MiddlewareSettings.h"
#include "protocol.h"

/**
 * @brief Module that implements Protocol Processor component's interfaces.
 */
module ProtocolProcessorP {
    provides{
        interface ProtocolInput;
        interface ProtocolOutput;
    }

    uses {
        interface ProtocolScheduler;
        interface SchedulerStatus;

        interface TaskDescription;
        interface TaskStatus;

        interface ReceiverStatus;
        interface EmitterStatus;
    }

    uses interface Boot;
    uses interface Read<uint16_t> as batteryVoltage;
    uses interface Timer<TMilli> as timer;
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

/** Keep track of controller association */
uint8_t associated = 0;
uint8_t associationTrial = 0;

/** Keep track of associated controller's flow ID */
uint16_t controllerFlowId;

/** Keep track of battery level */
uint8_t batteryLevel;

/** Buffer for protocol response */
uint8_t outputBuffer[MAX_PAYLOAD_SIZE];

/************************************************************************
 * Auxiliary functions
 ************************************************************************/

/** Functions for handling protocol requests and preparing responses */
void prepareNodeCharacteristicsDescription(uint8_t *payloadBuffer) {
    uint16_t *loadedTaskIds;
    uint8_t numberOfLoadedTasks;
    uint8_t i;

    NodeCharacteristicsDescription_t *nodeDescription;

    nodeDescription = (NodeCharacteristicsDescription_t *) payloadBuffer;

    /** Node characteristics */
    nodeDescription->pid = NODE_CHARACTERISTICS_DESCRIPTION_PID;
    nodeDescription->nid = NODE_ID;
    nodeDescription->m   = NODE_MOBILITY;
    nodeDescription->e   = NODE_ENERGY_SOURCE;
    nodeDescription->did = NODE_DEVICE_ID;
    nodeDescription->os  = NODE_OS;

    /** Node localization */
    nodeDescription->latitude  = NODE_LOCATION_LATITUDE;
    nodeDescription->longitude = NODE_LOCATION_LONGITUDE;
    nodeDescription->height    = NODE_LOCATION_HEIGHT;

    /** Node schedule settings */
    nodeDescription->ptq = MAX_SCHEDULED_PERIODIC_TASKS;
    nodeDescription->idq = MAX_SCHEDULED_NETWORK_INPUTS;
    nodeDescription->ntq = MAX_SCHEDULED_NETWORK_OUTPUTS;

    /** Loaded tasks */
    loadedTaskIds = call TaskDescription.getLoadedTasks(&numberOfLoadedTasks);

    nodeDescription->ntn = numberOfLoadedTasks;

    for (i = 0; (i < numberOfLoadedTasks) && (i < MAX_LOADED_TASKS); i++) {
        nodeDescription->tid[i] = loadedTaskIds[i];
    }
}

void handleNodeAssociateConfirmation(void *request) {

    /** Just register the SDN flow ID we should use to send protocol
        responses to the controller */
    /** We are using the default flow forever */
    //controllerFlowId = ((NodeAssociateConfirmation_t *) request)->cfi;
    /** Update association status */
    associated = 1;
}

void handleTaskDescriptionRequest(void *request, uint8_t *payloadBuffer) {
    uint8_t i;

    void *taskDescription;
    
    uint16_t requestedTaskId;

    /** Get the task ID for the task whose description is wanted */
    requestedTaskId = ((TaskDescriptionRequest_t *) request)->tid;

    /** Get description from Task API */
    taskDescription = call TaskDescription.getDescription(requestedTaskId);

    /** Copy description to output buffer */
    for (i = 0; i < sizeof(TaskDescriptionResponse_t); i++) {
        payloadBuffer[i] = ((uint8_t *) taskDescription)[i];
    }
}

void handlePeriodicTaskSchedulingRequest(void *request, uint8_t *payloadBuffer) {
    uint8_t errorCode;
    uint8_t scheduledInstance;

    TaskSchedulingConfirmation_t *schedulingResponse;
    PeriodicTaskSchedulingRequest_t *schedulingRequest;

    /** Request scheduling for periodic task */
    schedulingRequest = (PeriodicTaskSchedulingRequest_t *) request;

    call ProtocolScheduler.schedulePeriodicTask(
                                schedulingRequest->tid,
                                &scheduledInstance,
                                (uint8_t) schedulingRequest->ta,
                                schedulingRequest->tp,
                                (uint8_t) schedulingRequest->p,
                                (uint8_t) schedulingRequest->odt,
                                (uint8_t) schedulingRequest->odl,
                                schedulingRequest->ofi,
                                &errorCode);

    /** Prepare response */
    schedulingResponse = (TaskSchedulingConfirmation_t *) payloadBuffer;

    schedulingResponse->pid = PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID;
    schedulingResponse->tid = schedulingRequest->tid;
    schedulingResponse->tin = scheduledInstance;
    schedulingResponse->error.code = errorCode;
}

void handleInstantaneousTaskSchedulingRequest(void *request, uint8_t *payloadBuffer) {
    uint8_t errorCode;
    uint8_t scheduledInstance;
    uint8_t i;

    uint16_t inputFlowIds[MAX_NUMBER_OF_TASK_INPUTS];
    uint8_t  inputSizes[MAX_NUMBER_OF_TASK_INPUTS];
    char     argumentString[MAX_ARGUMENT_STRING_LENGTH];

    TaskSchedulingConfirmation_t *schedulingResponse;
    InstantaneousTaskSchedulingRequest_t *schedulingRequest;

    schedulingRequest = (InstantaneousTaskSchedulingRequest_t *) request;

    /** Copy scheduling parameters */
    for (i = 0; i < schedulingRequest->ipn; i++) {
        inputFlowIds[i] = schedulingRequest->input[i].ifi;
        inputSizes[i] = schedulingRequest->input[i].pn;
    }

    for (i = 0; i < MAX_ARGUMENT_STRING_LENGTH; i++) {
        argumentString[i] = (char) schedulingRequest->args[i];
        if (schedulingRequest->args[i] == 0x00) {
            break;
        }
    }

    /** Request scheduling for instantaneous task */
    call ProtocolScheduler.scheduleInstantaneousTask(
                                schedulingRequest->tid,
                                &scheduledInstance,
                                (uint8_t) schedulingRequest->ta,
                                schedulingRequest->ptw,
                                (uint8_t) schedulingRequest->p,
                                (uint8_t) schedulingRequest->odt,
                                (uint8_t) schedulingRequest->odl,
                                schedulingRequest->ofi,
                                schedulingRequest->ipn,
                                inputFlowIds,
                                inputSizes,
                                argumentString,
                                &errorCode);

    /** Prepare response */
    schedulingResponse = (TaskSchedulingConfirmation_t *) payloadBuffer;

    schedulingResponse->pid = INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID;
    schedulingResponse->tid = schedulingRequest->tid;
    schedulingResponse->tin = scheduledInstance;
    schedulingResponse->error.code = errorCode;
}

void handleTriggerTaskSchedulingRequest(void *request, uint8_t *payloadBuffer) {
    uint8_t errorCode;
    uint8_t scheduledInstance;

    uint64_t minuendInitialization;
    uint64_t subtrahendInitialization;
    uint64_t referenceInitialization;

    TaskSchedulingConfirmation_t *schedulingResponse;
    TriggerTaskSchedulingRequest_t *schedulingRequest;

    /** Request scheduling for trigger task */
    schedulingRequest = (TriggerTaskSchedulingRequest_t *) request;

    minuendInitialization = schedulingRequest->tmi;
    subtrahendInitialization = schedulingRequest->tsi;
    referenceInitialization = schedulingRequest->tri;

    call ProtocolScheduler.scheduleTriggerTask(
                                schedulingRequest->tid,
                                &scheduledInstance,
                                schedulingRequest->ofi,
                                schedulingRequest->tmc,
                                schedulingRequest->tsc,
                                schedulingRequest->tcs,
                                schedulingRequest->trc,
                                (void *) &minuendInitialization,
                                (void *) &subtrahendInitialization,
                                (void *) &referenceInitialization,
                                &errorCode);

    /** Prepare response */
    schedulingResponse = (TaskSchedulingConfirmation_t *) payloadBuffer;

    schedulingResponse->pid = TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID;
    schedulingResponse->tid = schedulingRequest->tid;
    schedulingResponse->tin = scheduledInstance;
    schedulingResponse->error.code = errorCode;
}

void handleScheduledTaskCancellationRequest(void *request, uint8_t *payloadBuffer) {
    uint16_t cancelledTaskId;
    uint8_t cancelledInstance;

    ScheduledTaskCancellationResponse_t *cancellationResponse;

    cancelledTaskId = ((ScheduledTaskCancellationRequest_t *) request)->tid;
    cancelledInstance = ((ScheduledTaskCancellationRequest_t *) request)->tin;

    /** Request task instance cancellation */
    call ProtocolScheduler.cancelSchedule(cancelledTaskId, cancelledInstance);

    /** Prepare response */
    cancellationResponse = (ScheduledTaskCancellationResponse_t *) payloadBuffer;

    cancellationResponse->pid = SCHEDULED_TASK_CANCELLATION_RESPONSE_PID;
    cancellationResponse->tid = cancelledTaskId;
    cancellationResponse->tin = cancelledInstance;
    cancellationResponse->s   = 1;
}

void handleTaskStatisticsRequest(void *request, uint8_t *payloadBuffer) {
    uint16_t requestedTaskId;
    uint8_t  requestedInstance;

    uint16_t executionNumber;

    TaskStatisticsResponse_t *statisticsResponse;

    /** Get target task instance from request */
    requestedTaskId = ((TaskStatisticsRequest_t *) request)->tid;
    requestedInstance = ((TaskStatisticsRequest_t *) request)->tin;

    /** Get statistics from target task instance */
    executionNumber = call TaskStatus.getExecutionStatus(requestedTaskId,
                                                         requestedInstance);

    /** Prepare response */
    statisticsResponse = (TaskStatisticsResponse_t *) payloadBuffer;

    statisticsResponse->pid = TASK_STATISTICS_RESPONSE_PID;
    statisticsResponse->tid = requestedTaskId;
    statisticsResponse->tin = requestedInstance;
    statisticsResponse->ten = executionNumber;
}

void handleNodeStatisticsRequest(void *request, uint8_t *payloadBuffer) {
    uint8_t i, j;
    uint8_t numberOfScheduledPeriodicTasks;
    uint8_t numberOfScheduledNetworkInputs;
    uint8_t numberOfScheduledNetworkOutputs;
    uint8_t taskScheduleCounter;
    uint8_t taskMaximumInstances;
    uint8_t numberOfLoadedTasks;
    uint16_t *loadedTaskIds;

    NodeStatisticsResponse_t *nodeStatistics;

    /** Node Statistics Request has no useful information */
    (void) request;

    /** Prepare response */
    nodeStatistics = (NodeStatisticsResponse_t *) payloadBuffer;
    nodeStatistics->pid = NODE_STATISTICS_RESPONSE_PID;

    /** Get current number of schedules */
    numberOfScheduledPeriodicTasks = call 
                SchedulerStatus.getPeriodicSchedulesStatus();

    numberOfScheduledNetworkInputs = call
                ReceiverStatus.getInputSchedulesStatus();

    numberOfScheduledNetworkOutputs = call
                EmitterStatus.getOutputSchedulesStatus();

    nodeStatistics->cpq = numberOfScheduledPeriodicTasks;
    nodeStatistics->ciq = numberOfScheduledNetworkInputs;
    nodeStatistics->cnq = numberOfScheduledNetworkOutputs;

    /** Get current battery level and RAM usage percnetages */
    // TODO: get RAM usage level
    nodeStatistics->crp = 0;
    nodeStatistics->cbl = batteryLevel;

    /** Get node location */
    // TODO: no GPS technology is currently supported */
    nodeStatistics->latitude = NODE_LOCATION_LATITUDE;
    nodeStatistics->longitude = NODE_LOCATION_LONGITUDE;
    nodeStatistics->height = NODE_LOCATION_HEIGHT;

    /** Get scheduling quantities for each task */
    loadedTaskIds = call TaskDescription.getLoadedTasks(&numberOfLoadedTasks);

    for (i = 0; (i < numberOfLoadedTasks) && (i < MAX_LOADED_TASKS); i++) {
        taskMaximumInstances = 
            ((TaskDescriptionResponse_t *) call 
                TaskDescription.getDescription(loadedTaskIds[i]))->msq;

        taskScheduleCounter = 0;
        for (j = 0; j < taskMaximumInstances; j++) {
            if (call TaskStatus.getScheduleStatus(loadedTaskIds[i], j)) {
                taskScheduleCounter++;
            }
        }

        nodeStatistics->csq[i] = taskScheduleCounter;
    }
}

/************************************************************************
 * Init Interface 
 ************************************************************************/
    event void Boot.booted() {
        /** Set controller flow ID to broadcast */
        controllerFlowId = DEFAULT_CONTROLLER_SDN_FLOW_ID;

        /** Call ADC to read current battery level */
        call batteryVoltage.read();

        /** Send node characteristics description */
        prepareNodeCharacteristicsDescription(outputBuffer);
        
        /** Commented for testing purposes
        signal ProtocolOutput.ready(controllerFlowId, outputBuffer,
                                    sizeof(NodeCharacteristicsDescription_t)); */

        /** Start timer for periodically trying to send node 
            characteristics description */
        call timer.startOneShot(ASSOCIATION_REQUEST_TRIAL_TIME_INTERVAL);
    }


/************************************************************************
 * Protocol Input Interface 
 ************************************************************************/
    command void ProtocolInput.data(void *protocolRequest)
    {
        uint8_t requestPid;
        uint8_t responseLength;
        uint8_t invalidRequest = 0;

        /** Monitor battery level at every request */
        call batteryVoltage.read();

        /** Get request PID from resquest payload */
        requestPid = *((uint8_t *) protocolRequest) >> 4;

        /** Handle each request appropriately */
        switch(requestPid) {
            case NODE_ASSOCIATE_CONFIRMATION_PID:
                handleNodeAssociateConfirmation(protocolRequest); 
                /** We won't send anything in this case */
                return;

            case TASK_DESCRIPTION_REQUEST_PID:
                handleTaskDescriptionRequest(protocolRequest, outputBuffer);
                responseLength = sizeof(TaskDescriptionResponse_t);
                break;

            case PERIODIC_TASK_SCHEDULING_REQUEST_PID:
                handlePeriodicTaskSchedulingRequest(protocolRequest, 
                                                    outputBuffer);
                responseLength = sizeof(TaskSchedulingConfirmation_t);
                break;

            case INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID:
                handleInstantaneousTaskSchedulingRequest(protocolRequest,
                                                         outputBuffer);
                responseLength = sizeof(TaskSchedulingConfirmation_t);
                break;

            case TRIGGER_TASK_SCHEDULING_REQUEST_PID:
                handleTriggerTaskSchedulingRequest(protocolRequest,
                                                   outputBuffer);
                responseLength = sizeof(TaskSchedulingConfirmation_t);
                break;

            case SCHEDULED_TASK_CANCELLATION_REQUEST_PID:
                handleScheduledTaskCancellationRequest(protocolRequest,
                                                       outputBuffer);
                responseLength = sizeof(ScheduledTaskCancellationResponse_t);
                break;

            case TASK_STATISTICS_REQUEST_PID:
                handleTaskStatisticsRequest(protocolRequest, outputBuffer);
                responseLength = sizeof(TaskStatisticsResponse_t);
                break;

            case NODE_STATISTICS_REQUEST_PID:
                handleNodeStatisticsRequest(protocolRequest, outputBuffer);
                responseLength = sizeof(NodeStatisticsResponse_t);
                break;

            default:
                invalidRequest = 1;
                break;
        }

        if (invalidRequest) {
            return;
        }

        /** Signal protocol response output to Emitter */
        signal ProtocolOutput.ready(controllerFlowId, 
                                    outputBuffer, 
                                    responseLength);
    }

/************************************************************************
 * Read Interface Events
 ************************************************************************/
    
    event void batteryVoltage.readDone(error_t result, uint16_t data) {
        if (!result) {
            //batteryLevel = (100 * data) >> 12;
            /** We believe data is already a percentage */
            batteryLevel = data;
        }
    }

/************************************************************************
 * Timer Interface Events
 ************************************************************************/
    
    event void timer.fired() {
        /** Send node characteristics description */
        prepareNodeCharacteristicsDescription(outputBuffer);

        /** Commented for testing purposes
        signal ProtocolOutput.ready(controllerFlowId, outputBuffer,
                                    sizeof(NodeCharacteristicsDescription_t)); */

        associationTrial++;

        /** Try to send once more */
        if (!associated && (associationTrial < MAX_ASSOCIATION_REQUEST_TRIES)) {
            call timer.startOneShot(ASSOCIATION_REQUEST_TRIAL_TIME_INTERVAL);
        }
    }
}
