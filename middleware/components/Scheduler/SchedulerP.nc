/**
 * @file SchedulerP.nc
 * @brief File implementing the Scheduler component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Scheduler.h"
#include "MiddlewareSettings.h"

/**
 * @brief Module that implements Scheduler component's interfaces.
 */
module SchedulerP {
    provides{
        interface ProtocolScheduler;
        interface SchedulerStatus;
        interface Init;
    }

    uses {
        interface InputScheduler;
        interface EmitterScheduler;
        interface TaskScheduler;
    }

    uses interface Timer<TMilli> as MilliPeriodicTimer;
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

/** Keep track of scheduled periodic tasks */
PeriodicTaskSchedule_t scheduledPeriodicTasks[MAX_SCHEDULED_PERIODIC_TASKS];

/** Keep track of execution events for periodic tasks which run on
    millisecond time scale. */
TimerQueueEntry_t milliPeriodicTasksQueue[MAX_SCHEDULED_PERIODIC_TASKS];
uint8_t nextMilliPeriodicTaskToRun;

/************************************************************************
 * Auxiliary functions
 ************************************************************************/

/**
 * @brief Find free index of the periodic task schedule table.
 *
 * @param[out] scheduleIndex Pointer to the free position found.
 *
 * @return Operation status.
 * @retval 0 Successful operation.
 * @retval 1 Unsuccessful operation.
 */
uint8_t findFreePeriodicTaskSchedule(uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_PERIODIC_TASKS; i++) {
        if (scheduledPeriodicTasks[i].scheduled == 0) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/**
 * @brief Find table index of a periodic task schedule.
 *
 * @param[in] taskId ID of the task whose index is to be found.
 * @param[in] taskInstance Instance number of the task to be found.
 * @param[out] scheduleIndex Table index corresponding to the task instance.
 *
 * @return Operation status.
 * @retval 0 Index was successfully found.
 * @retval 1 No index was found.
 */
uint8_t findPeriodicTaskScheduleIndex(uint16_t taskId, 
                                      uint8_t taskInstance,
                                      uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_PERIODIC_TASKS; i++) {
        if ((scheduledPeriodicTasks[i].taskId == taskId) &&
            (scheduledPeriodicTasks[i].instanceNumber == taskInstance)) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/**
 * @brief Add a periodic task instance which is ready to run to the 
 *        appropriate timer queue.
 *
 * @param[in] taskId ID of the ready task instance.
 * @param[in] taskInstance Instance number of the ready task.
 */
void addReadyPeriodicTaskToQueue(uint16_t taskId, uint8_t taskInstance) {
    uint8_t readyTaskIndex;

    if (findPeriodicTaskScheduleIndex(taskId, 
                                      taskInstance, 
                                      &readyTaskIndex)) {
        /** If the ready task is periodic but is not registered,
            something wrong is going on */
        return;
    }

    /** If the periodic task is to be run only once, then run it and 
        cancel it */
    if (scheduledPeriodicTasks[readyTaskIndex].runOnlyOnce) {
        call TaskScheduler.runTask(taskId, taskInstance);

        call ProtocolScheduler.cancelSchedule(taskId, taskInstance);
        return;
    }

    scheduledPeriodicTasks[readyTaskIndex].readyToRun = 1;

    /** If the periodic task is ready to run and should not be executed
        only once, then it should be added to the proper execution queue */
    if (scheduledPeriodicTasks[readyTaskIndex].periodScale) {
        // TODO: Microsecond scale tasks.
    }
    else {
        milliPeriodicTasksQueue[readyTaskIndex].scheduled = 1;
        milliPeriodicTasksQueue[readyTaskIndex].timeUntilNextEvent =
                     scheduledPeriodicTasks[readyTaskIndex].executionPeriod;

        /** Task will only start after next periodic task execution, so
            we need to start timer if it is not running yet */
        if (!(call MilliPeriodicTimer.isRunning())) {
            nextMilliPeriodicTaskToRun = readyTaskIndex;
            call MilliPeriodicTimer.startOneShot(
                    scheduledPeriodicTasks[readyTaskIndex].executionPeriod);
        }
    }
}

/**
 * @brief Update a timer queue and find its next event.
 *
 * @param[in] timerQueue Pointer to the timer queue to be updated.
 * @param[in] queueSize Size value of the timer queue.
 * @param[in] timerUpdate Passed amount of time to update.
 * @param[inout] queueHead Pointer to the queue's current next event.
 *
 * @return Operation status. 
 * @retval 0 Queue was successfully updated and there is a next event.
 * @retval 1 Queue was successfully updated, but there isn't a next event.
 */
uint8_t updateTimerQueue(TimerQueueEntry_t *timerQueue, 
                         uint8_t queueSize, 
                         uint32_t timerUpdate,
                         uint8_t *queueHead) {
    uint8_t i;
    uint8_t returnValue;
    uint32_t currentSmallestTime;
    uint8_t currentHead = *queueHead;

    /** Update remaining time, except for the current head */
    if (timerUpdate != 0) {
        for (i = 0; i < queueSize; i++) {
            if ((i != currentHead) && (timerQueue[i].scheduled)) {
                if (timerQueue[i].timeUntilNextEvent > timerUpdate) {
                    timerQueue[i].timeUntilNextEvent -= timerUpdate;
                }
                else {
                    timerQueue[i].timeUntilNextEvent = 0;
                }
            }
        }
    }

    /** Search for next event */
    returnValue = 1;
    for (i = 0; i < queueSize; i++) {
        if (timerQueue[i].scheduled) {
            if (returnValue) {
                /** Found first scheduled entry */
                returnValue = 0;
                currentSmallestTime = timerQueue[i].timeUntilNextEvent;
                *queueHead = i;
            }
            else {
                /** This is not the first entry found, so find out if this
                    entry's remaining time is smaller than the previously 
                    found */
                if (timerQueue[i].timeUntilNextEvent < currentSmallestTime) {
                    currentSmallestTime = timerQueue[i].timeUntilNextEvent;
                    *queueHead = i;
                }
            }
        }
    }

    return returnValue;
}

/************************************************************************
 * Init Interface 
 ************************************************************************/
    command error_t Init.init() {
        /** Initialize periodic task schedule table */
        uint8_t i;
        for (i = 0; i < MAX_SCHEDULED_PERIODIC_TASKS; i++) {
            scheduledPeriodicTasks[i].scheduled = 0;
            milliPeriodicTasksQueue[i].scheduled = 0;
        }

        return SUCCESS;
    }


/************************************************************************
 * Protocol Scheduler Interface 
 ************************************************************************/
    command uint8_t ProtocolScheduler.schedulePeriodicTask(
                                                    uint16_t taskId,
                                                    uint8_t *taskInstance,
                                                    uint8_t triggerPolicy,
                                                    uint32_t executionPeriod,
                                                    uint8_t periodScale,
                                                    uint8_t outputDataType,
                                                    uint8_t outputDataLength,
                                                    uint16_t outputFlowId,
                                                    uint8_t *errorCode) {
        uint8_t error;
        uint8_t scheduleIndex;
        TaskTriggerPolicy_t instanceTriggerPolicy;

        instanceTriggerPolicy = (TaskTriggerPolicy_t) triggerPolicy;

        *errorCode = 0;

        /** Try to schedule task instance */
        error = call TaskScheduler.schedulePeriodicTask(taskId,
                                                        taskInstance,
                                                        instanceTriggerPolicy);
        if (error) {
            *errorCode |= SCHEDULER_ERROR_NO_INSTANCE;
        }

        /** Try to schedule task output */
        if (!(*errorCode & SCHEDULER_ERROR_NO_INSTANCE)) {
            error = call EmitterScheduler.scheduleTaskOutput(
                                                        taskId,
                                                        *taskInstance,
                                                        0, // Output is not a trigger
                                                        outputDataType,
                                                        outputDataLength,
                                                        outputFlowId);
            if (error) {
                *errorCode |= SCHEDULER_ERROR_NO_OUTPUT;
            }
        }

        /** Try to find available index for scheduling task */
        if (findFreePeriodicTaskSchedule(&scheduleIndex)) {
            *errorCode |= SCHEDULER_ERROR_NO_PERIODIC_SCHEDULE;
        }
        else if (!(*errorCode)) {
            /** Mark instance as scheduled */
            scheduledPeriodicTasks[scheduleIndex].scheduled = 1;
            scheduledPeriodicTasks[scheduleIndex].readyToRun = 0;

            if (executionPeriod == 0) {
                scheduledPeriodicTasks[scheduleIndex].runOnlyOnce = 1;
            }

            scheduledPeriodicTasks[scheduleIndex].periodScale = periodScale;

            scheduledPeriodicTasks[scheduleIndex].taskId = taskId;
            scheduledPeriodicTasks[scheduleIndex].instanceNumber = 
                                                                *taskInstance;
            scheduledPeriodicTasks[scheduleIndex].triggerPolicy = 
                                                        instanceTriggerPolicy;

            scheduledPeriodicTasks[scheduleIndex].executionPeriod = 
                                                                executionPeriod;


            /** If trigger policy requires no trigger, add instance to
                execution queue */
            if (triggerPolicy == NO_TRIGGER) {
                addReadyPeriodicTaskToQueue(taskId, *taskInstance);
            }
        }

        /** Cancel instances if the scheduling is unsuccessful */
        if (*errorCode) {
            if (!(*errorCode & SCHEDULER_ERROR_NO_PERIODIC_SCHEDULE)) {
                scheduledPeriodicTasks[scheduleIndex].scheduled = 0;
            }

            if (!(*errorCode & SCHEDULER_ERROR_NO_INSTANCE)) {
                call TaskScheduler.cancelTask(taskId, *taskInstance);
                call EmitterScheduler.cancelTaskOutput(taskId, *taskInstance);
            }

            return 1;
        }

        /** Success */
        *errorCode |= SCHEDULER_SUCCESS;

        return 0;
    }

    command uint8_t ProtocolScheduler.scheduleInstantaneousTask(
                                                    uint16_t taskId,
                                                    uint8_t *taskInstance,
                                                    uint8_t triggerPolicy,
                                                    uint32_t discardPeriod,
                                                    uint8_t discardPeriodScale,
                                                    uint8_t outputDataType,
                                                    uint8_t outputDataLength,
                                                    uint16_t outputFlowId,
                                                    uint8_t inputNumber,
                                                    uint16_t *inputFlowIds,
                                                    uint8_t *inputSizes,
                                                    char *argumentString,
                                                    uint8_t *errorCode) {
        uint8_t error;
        uint8_t parameterId;
        TaskTriggerPolicy_t instanceTriggerPolicy;

        instanceTriggerPolicy = (TaskTriggerPolicy_t) triggerPolicy;

        *errorCode = 0;

        /** Try to schedule task instance */
        error = call TaskScheduler.scheduleInstantaneousTask(
                                                        taskId,
                                                        taskInstance,
                                                        instanceTriggerPolicy,
                                                        inputSizes,
                                                        argumentString);
        if (error) {
            *errorCode |= SCHEDULER_ERROR_NO_INSTANCE;
        }

        /** Try to schedule task output */
        if (!(*errorCode & SCHEDULER_ERROR_NO_INSTANCE)) {
            error = call EmitterScheduler.scheduleTaskOutput(
                                                        taskId,
                                                        *taskInstance,
                                                        0, // Output is not a trigger
                                                        outputDataType,
                                                        outputDataLength,
                                                        outputFlowId);
            if (error) {
                *errorCode |= SCHEDULER_ERROR_NO_OUTPUT;
            }
        }

        /** Try to schedule task inputs */
        if (!(*errorCode & SCHEDULER_ERROR_NO_INSTANCE) && inputNumber) {
            error = 0;
            for (parameterId = 0; parameterId < inputNumber; parameterId++) {
                error += call InputScheduler.scheduleTaskInput(
                                                     taskId,
                                                     *taskInstance,
                                                     parameterId,
                                                     inputFlowIds[parameterId]);
            }

            if (error) {
                *errorCode |= SCHEDULER_ERROR_NO_INPUT;
            }
        }

        /** Try to find available index for scheduling task */
        if (0) {
            // TODO: keep track of parameter expiration.
            *errorCode |= SCHEDULER_ERROR_NO_INSTANTANEOUS_SCHEDULE;
        }
        else if (!(*errorCode)) {
            /** Mark instance as scheduled */
            // TODO: keep track of parameter expiration.
        }

        /** Cancel instances if the scheduling is unsuccessful */
        if (*errorCode) {
            if (!(*errorCode & SCHEDULER_ERROR_NO_INSTANCE)) {
                call TaskScheduler.cancelTask(taskId, *taskInstance);
                call EmitterScheduler.cancelTaskOutput(taskId, *taskInstance);
                call InputScheduler.cancelTaskInput(taskId, *taskInstance);
            }

            return 1;
        }

        /** If an instantaneous task has no inputs, it should be run only
            once, then cancelled. */
        if (inputNumber == 0) {
            call TaskScheduler.runTask(taskId, *taskInstance);

            call ProtocolScheduler.cancelSchedule(taskId, *taskInstance);
        }

        /** Success */
        *errorCode |= SCHEDULER_SUCCESS;

        return 0;
    }

    command uint8_t ProtocolScheduler.scheduleTriggerTask(
                                                uint16_t taskId,
                                                uint8_t *taskInstance,
                                                uint16_t outputFlowId,
                                                uint8_t minuendRule,
                                                uint8_t subtrahendRule,
                                                uint8_t comparisonRule,
                                                uint8_t referenceRule,
                                                void *minuendInitialization,
                                                void *subtrahendInitialization,
                                                void *referenceInitialization,
                                                uint8_t *errorCode) {
        // TODO
        *errorCode = SCHEDULER_ERROR_NO_TRIGGER_SCHEDULE;
        return 1;
    }

    command void ProtocolScheduler.cancelSchedule(uint16_t taskId,
                                                  uint8_t taskInstance) {
        uint8_t taskIndex;

        /** Cancel Receiver, Emitter and Task API instances */
        call TaskScheduler.cancelTask(taskId, taskInstance);
        call EmitterScheduler.cancelTaskOutput(taskId, taskInstance);
        call InputScheduler.cancelTaskInput(taskId, taskInstance);

        /** Try to find and cancel task from periodic schedule table */
        if (!(findPeriodicTaskScheduleIndex(taskId, taskInstance, &taskIndex))) {
            scheduledPeriodicTasks[taskIndex].scheduled = 0;
            milliPeriodicTasksQueue[taskIndex].scheduled = 0;
        }
    }

/************************************************************************
 * Scheduler Status Interface 
 ************************************************************************/
    command uint8_t SchedulerStatus.getPeriodicSchedulesStatus(void) {
        uint8_t i;
        uint8_t scheduledPeriodicTaskCounter = 0;

        /** Count scheduled periodic tasks */
        for (i = 0; i < MAX_SCHEDULED_PERIODIC_TASKS; i++) {
            if (scheduledPeriodicTasks[i].scheduled) {
                scheduledPeriodicTaskCounter++;
            }
        }

        return scheduledPeriodicTaskCounter;
    }

/************************************************************************
 * Task Scheduler Events 
 ************************************************************************/
    event   void TaskScheduler.taskReadyToRun(uint16_t taskId, 
                                              uint8_t taskInstance, 
                                              TaskType_t taskType) {

        /** If the task is not of the periodic type, simply run it */
        if (taskType != PERIODIC_TASK_TYPE) {
            call TaskScheduler.runTask(taskId, taskInstance);
            return;
        }

        addReadyPeriodicTaskToQueue(taskId, taskInstance);
    }

/************************************************************************
 * Timer Events
 ************************************************************************/
    event   void MilliPeriodicTimer.fired() {
        uint8_t taskToRunIndex;
        uint16_t taskToRunId;
        uint8_t taskToRunInstance;
        uint32_t elapsedTime;
        uint32_t timeUntilNextEvent;
        uint8_t updateStatus;

        /** Run periodic task whose time of execution arrived */
        taskToRunIndex = nextMilliPeriodicTaskToRun;
        taskToRunId = scheduledPeriodicTasks[taskToRunIndex].taskId;
        taskToRunInstance = scheduledPeriodicTasks[taskToRunIndex].instanceNumber;

        /** Note: comment next line if compiling Scheduler disconnected */
        call TaskScheduler.runTask(taskToRunId, taskToRunInstance);

        /** In case this task should always be triggered to run */
        if (scheduledPeriodicTasks[taskToRunIndex].triggerPolicy 
                                                        == TRIGGER_ALWAYS) {
            scheduledPeriodicTasks[taskToRunIndex].readyToRun = 0;
            milliPeriodicTasksQueue[taskToRunIndex].scheduled = 0;
        }

        /** Next time this task will be run is after its time period 
            has elapsed once more */
        elapsedTime =
            milliPeriodicTasksQueue[taskToRunIndex].timeUntilNextEvent;
        milliPeriodicTasksQueue[taskToRunIndex].timeUntilNextEvent =
            scheduledPeriodicTasks[taskToRunIndex].executionPeriod;

        /** Update queue for millisecond scale periodic tasks */
        updateStatus = updateTimerQueue(milliPeriodicTasksQueue, 
                                        MAX_SCHEDULED_PERIODIC_TASKS,
                                        elapsedTime,
                                        &nextMilliPeriodicTaskToRun);

        /** In case we have a next task to run, schedule it */
        if (!updateStatus) {
            timeUntilNextEvent = 
                milliPeriodicTasksQueue[nextMilliPeriodicTaskToRun].timeUntilNextEvent;
            
            call MilliPeriodicTimer.startOneShot(timeUntilNextEvent);
        }
    }
}
