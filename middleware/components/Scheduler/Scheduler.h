/**
 * @file Scheduler.h
 * @brief Header file for the Scheduler component.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include "TaskAPI.h"

/******************************************************************************
 * Error code macros
 ******************************************************************************/

#define SCHEDULER_SUCCESS                            (1 << 7)
#define SCHEDULER_ERROR_NO_INSTANCE                  (1 << 6)
#define SCHEDULER_ERROR_NO_INPUT                     (1 << 5)
#define SCHEDULER_ERROR_NO_OUTPUT                    (1 << 4)
#define SCHEDULER_ERROR_NO_PERIODIC_SCHEDULE         (1 << 3)
#define SCHEDULER_ERROR_NO_INSTANTANEOUS_SCHEDULE    (1 << 2)
#define SCHEDULER_ERROR_NO_TRIGGER_SCHEDULE          (1 << 1)

/******************************************************************************
 * Scheduler types
 ******************************************************************************/

/** Period time scale */
enum PeriodScale {
    MILLISECOND_PERIOD_SCALE = 0,
    MICROSECOND_PERIOD_SCALE = 1
};

/** Data structure for keeping track of periodic tasks */
typedef struct {
    struct {
        uint8_t scheduled : 1;
        uint8_t readyToRun : 1;
        uint8_t runOnlyOnce : 1;
        uint8_t periodScale : 1;
    };
    uint16_t taskId;
    uint8_t instanceNumber;
    TaskTriggerPolicy_t triggerPolicy;
    uint32_t executionPeriod;
} PeriodicTaskSchedule_t;

/** Data structure for keeping track of instantaneous tasks */
typedef struct {
    struct {
        uint8_t scheduled : 1;
        uint8_t runOnlyOnce : 1;
        uint8_t periodScale : 1;
    };
    uint16_t taskId;
    uint8_t instanceNumber;
    TaskTriggerPolicy_t triggerPolicy;
    uint32_t discardPeriod;
} InstantaneousTaskSchedule_t;

/** Data structure for keeping tack of trigger tasks */
typedef struct {
    uint8_t scheduled;
    uint16_t taskId;
    uint8_t instanceNumber;
} TriggerTaskSchedule_t;

/** Data structure for timer queue entries */
typedef struct {
    uint8_t scheduled;
    uint32_t timeUntilNextEvent;
} TimerQueueEntry_t;

#endif // __SCHEDULER_H__
