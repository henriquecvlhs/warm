/**
 * @file ReceiverP.nc
 * @brief File implementing the Receiver component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Receiver.h"
#include "MiddlewareSettings.h"

/**
 * @brief Module that implements Receiver component's interfaces.
 */
module ReceiverP {
    provides{
        interface InputScheduler;
        interface ReceiverStatus;
        interface Init;
    }

    uses {
        interface TaskInput;
        interface ProtocolInput;
        interface SDNReceive;
    }
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

/** Keep track of scheduled inputs */
InputSchedule_t scheduledInputs[MAX_SCHEDULED_NETWORK_INPUTS];

/************************************************************************
 * Auxiliary functions
 ************************************************************************/

/**
 * @brief Find free index of the input schedule table.
 *
 * @param[out] scheduleIndex Pointer to the free position found.
 *
 * @return Operation status.
 * @retval 0 Successful operation.
 * @retval 1 Unsuccessful operation.
 */
uint8_t findFreeInputSchedule(uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_NETWORK_INPUTS; i++) {
        if (scheduledInputs[i].scheduled == 0) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/**
 * @brief Find table index of an input schedule.
 *
 * @param[in] flowId ID of the SDN flow whose index is to be found.
 * @param[out] scheduleIndex Table index corresponding to the flow ID.
 *
 * @return Operation status.
 * @retval 0 Index was successfully found.
 * @retval 1 No index was found.
 */
uint8_t findInputScheduleIndex(uint16_t flowId, uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_NETWORK_INPUTS; i++) {
        if (scheduledInputs[i].flowId == flowId) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/**
 * @brief Forward received package to appropriate component.
 *
 * @param[in] flowId ID of the SDN flow which brought the package.
 * @param[in] package Pointer to the received package.
 */
void forwardReceivedPackage(uint16_t flowId, uint8_t *package) {
    uint8_t packagePid = *package >> 4;
    uint8_t inputIndex;
    uint64_t data;

    switch(packagePid) {
        case DATA_TRANSMISSION_PID:
            if (!(findInputScheduleIndex(flowId, &inputIndex))) {
                /** Extract data from package */
                data = ((DataTransmission_t *) package)->td;

                /** Send extracted data to task */
                call TaskInput.data(scheduledInputs[inputIndex].taskId,
                                    scheduledInputs[inputIndex].taskInstance,
                                    scheduledInputs[inputIndex].parameterId,
                                    (void *) &data);
            }
            break;

        case TRIGGER_SIGNAL_TRANSMISSION_PID:
            // TODO: find out how to deal with trigger signals.
            break;

        default:
            /** If it is not either data or trigger signal,
                forward to Protocol Processor */
            call ProtocolInput.data((void *) package);
            break;
    }
}

/************************************************************************
 * Init Interface 
 ************************************************************************/
    command error_t Init.init() {
        /** Initialize input schedule table */
        uint8_t i;
        for (i = 0; i < MAX_SCHEDULED_NETWORK_INPUTS; i++) {
            scheduledInputs[i].scheduled = 0;
        }

        return SUCCESS;
    }


/************************************************************************
 * Input Scheduler Interface 
 ************************************************************************/
    command uint8_t InputScheduler.scheduleTaskInput(uint16_t taskId,
                                                     uint8_t taskInstance,
                                                     uint8_t parameterId,
                                                     uint16_t inputFlowId) {    
        uint8_t freeScheduleIndex;

        /** Search for free input schedule entry */
        if (findFreeInputSchedule(&freeScheduleIndex)) {
            /** No schedule entry is available: cannot schedule */
            return 1;
        }
    
        /** Register input schedule */
        scheduledInputs[freeScheduleIndex].scheduled = 1;
        scheduledInputs[freeScheduleIndex].flowId = inputFlowId;
        scheduledInputs[freeScheduleIndex].taskId = taskId;
        scheduledInputs[freeScheduleIndex].taskInstance = taskInstance;
        scheduledInputs[freeScheduleIndex].parameterId = parameterId;

        return 0;
    }
    
    command void InputScheduler.cancelTaskInput(uint16_t taskId,
                                                uint8_t taskInstance) {
        uint8_t i;

        /** Search for entries of respective task ID and instance */
        for (i = 0; i < MAX_SCHEDULED_NETWORK_INPUTS; i ++) {
            if ((scheduledInputs[i].taskId == taskId) &&
                (scheduledInputs[i].taskInstance == taskInstance)) {
                /** Cancel found entry schedule */
                scheduledInputs[i].scheduled = 0;
            }
        }
    }
/************************************************************************
 * Receiver Status Interface 
 ************************************************************************/
    command uint8_t ReceiverStatus.getInputSchedulesStatus(void) {
        uint8_t i;
        uint8_t scheduleCounter = 0;

        /** Count scheduled entries */
        for (i = 0; i < MAX_SCHEDULED_NETWORK_INPUTS; i++) {
            if (scheduledInputs[i].scheduled == 1) {
                scheduleCounter++;
            }
        }

        return scheduleCounter;
    }
/************************************************************************
 * SDN Receive Interface Events
 ************************************************************************/
    event void SDNReceive.receive(uint16_t originId,
                                  uint16_t flowId,
                                  void *payload,
                                  uint8_t length) {
        (void) originId;
        (void) length;

        forwardReceivedPackage(flowId, (uint8_t *) payload);
    }
}
