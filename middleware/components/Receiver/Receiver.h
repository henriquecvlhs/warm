/**
 * @file Receiver.h
 * @brief Header file for the Receiver component.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RECEIVER_H__
#define __RECEIVER_H__

#include "protocol.h"

/******************************************************************************
 * Receiver types
 ******************************************************************************/

/** Data structure for keeping track of scheduled inputs */
typedef struct {
    uint8_t scheduled;
    uint16_t flowId;
    uint16_t taskId;
    uint8_t taskInstance;
    uint8_t parameterId;
} InputSchedule_t;

#endif // __RECEIVER_H__
