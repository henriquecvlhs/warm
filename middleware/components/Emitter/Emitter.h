/**
 * @file Emitter.h
 * @brief Header file for the Emitter component.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EMITTER_H__
#define __EMITTER_H__

#include "protocol/DataTransmission.h"
#include "protocol/TriggerSignalTransmission.h"

/******************************************************************************
 * Emitter types
 ******************************************************************************/

/** Possible data types for task output data */
typedef enum OutputDataType {
    OUTPUT_DATA_TYPE_FLOAT  = 0,
    OUTPUT_DATA_TYPE_FIXED  = 1,
    OUTPUT_DATA_TYPE_INT    = 2,
    OUTPUT_DATA_TYPE_UINT   = 3,
    OUTPUT_DATA_TYPE_BARRAY = 4,
    OUTPUT_DATA_TYPE_BOOL   = 5,
} OutputDataType_t;

/** Possible data length for task output data */
typedef enum OutputDataLength {
    OUTPUT_DATA_LENGTH_1 = 0, // 1 byte  / 8 bits
    OUTPUT_DATA_LENGTH_2 = 1, // 2 bytes / 16 bits
    OUTPUT_DATA_LENGTH_4 = 2, // 4 bytes / 32 bits
    OUTPUT_DATA_LENGTH_8 = 3, // 8 bytes / 64 bits
} OutputDataLength_t;

/** Data structure for keeping track of scheduled outputs */
typedef struct {
    struct {
        uint8_t scheduled  : 1;
        uint8_t trigger    : 1;
        uint8_t dataType   : 3;
        uint8_t dataLength : 2;
    };

    uint16_t flowId;
    uint16_t taskId;
    uint8_t taskInstance;
} OutputSchedule_t;

#endif // __EMITTER_H__
