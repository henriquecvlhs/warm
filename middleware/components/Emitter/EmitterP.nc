/**
 * @file EmitterP.nc
 * @brief File implementing the Emitter component module.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Emitter.h"
#include "MiddlewareSettings.h"

/**
 * @brief Module that implements Emitter component's interfaces.
 */
module EmitterP {
    provides{
        interface EmitterScheduler;
        interface EmitterStatus;
        interface Init;
    }

    uses {
        interface TaskOutput;
        interface ProtocolOutput;
        interface SDNSend;
    }
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

/** Keep track of scheduled outputs */
OutputSchedule_t scheduledOutputs[MAX_SCHEDULED_NETWORK_OUTPUTS];

/** Buffer for for Data Transmission packages */
DataTransmission_t dataPackage;

/** Buffer for for Trigger Signal Transmission packages */
TriggerSignalTransmission_t triggerPackage;

/************************************************************************
 * Auxiliary functions
 ************************************************************************/

/**
 * @brief Find free index in the output schedule table.
 *
 * @param[out] scheduleIndex Pointer to the free position found.
 *
 * @return Operation status.
 * @retval 0 Successful operation.
 * @retval 1 Unsuccessful operation.
 */
uint8_t findFreeOutputSchedule(uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_NETWORK_OUTPUTS; i++) {
        if (scheduledOutputs[i].scheduled == 0) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/**
 * @brief Find table index of an output schedule.
 *
 * @param[in] taskId ID of the task whose index is to be found.
 * @param[in] taskInstance Instance of the task whose index is to be found.
 * @param[out] scheduleIndex Table index corresponding to the task instance.
 *
 * @return Operation status.
 * @retval 0 Index was successfully found.
 * @retval 1 No index was found.
 */
uint8_t findOutputScheduleIndex(uint16_t taskId, 
                                uint8_t taskInstance,
                                uint8_t *scheduleIndex) {
    uint8_t i;

    for (i = 0; i < MAX_SCHEDULED_NETWORK_OUTPUTS; i++) {
        if ((scheduledOutputs[i].taskId == taskId) &&
            (scheduledOutputs[i].taskInstance == taskInstance)) {
            *scheduleIndex = i;
            return 0;
        }
    }

    return 1;
}

/************************************************************************
 * Init Interface 
 ************************************************************************/
    command error_t Init.init() {
        /** Initialize input schedule table */
        uint8_t i;
        for (i = 0; i < MAX_SCHEDULED_NETWORK_OUTPUTS; i++) {
            scheduledOutputs[i].scheduled = 0;
        }

        return SUCCESS;
    }

/************************************************************************
 * Emitter Scheduler Interface 
 ************************************************************************/
    command uint8_t EmitterScheduler.scheduleTaskOutput(
                                                    uint16_t taskId,
                                                    uint8_t taskInstance,
                                                    uint8_t triggerOutput,
                                                    uint8_t outputDataType,
                                                    uint8_t outputDataLength,
                                                    uint16_t outputFlowId) {
        uint8_t freeScheduleIndex;

        /** Search for free output schedule entry */
        if (findFreeOutputSchedule(&freeScheduleIndex)) {
            /** No schedule entry is available: cannot schedule */
            return 1;
        }

        /** Register output schedule */
        scheduledOutputs[freeScheduleIndex].scheduled = 1;
        scheduledOutputs[freeScheduleIndex].flowId = outputFlowId;
        scheduledOutputs[freeScheduleIndex].taskId = taskId;
        scheduledOutputs[freeScheduleIndex].taskInstance = taskInstance;
        scheduledOutputs[freeScheduleIndex].trigger = triggerOutput;
        scheduledOutputs[freeScheduleIndex].dataType = outputDataType;
        scheduledOutputs[freeScheduleIndex].dataLength = outputDataLength;

        return 0;
    }

    command void EmitterScheduler.cancelTaskOutput(uint16_t taskId,
                                                   uint8_t taskInstance) {
        uint8_t outputIndex;
        if (findOutputScheduleIndex(taskId, taskInstance, &outputIndex)) {
            /** There is no such scheduled output, so no need to cancel */
            return;
        }

        /** Cancel found schedule entry */
        scheduledOutputs[outputIndex].scheduled = 0;
    }

/************************************************************************
 * Emitter Status Interface 
 ************************************************************************/
    command uint8_t EmitterStatus.getOutputSchedulesStatus(void) {
        uint8_t i;
        uint8_t scheduleCounter = 0;

        /** Count scheduled entries */
        for (i = 0; i < MAX_SCHEDULED_NETWORK_OUTPUTS; i++) {
            if (scheduledOutputs[i].scheduled == 1) {
                scheduleCounter++;
            }
        }

        return scheduleCounter;
    }

/************************************************************************
 * Task Output Interface Events
 ************************************************************************/
    event void TaskOutput.ready(uint16_t taskId, 
                                uint8_t taskInstance, 
                                void *value) {
        uint8_t outputIndex;
        uint16_t destinationFlowId;

        /** Find schedule entry fro this task instance */
        if (findOutputScheduleIndex(taskId, taskInstance, &outputIndex)) {
            /** No such output schedule, cannot send data to network */
            return;
        }

        /** Get destination SDN flow ID */
        destinationFlowId = scheduledOutputs[outputIndex].flowId;

        /** Check if this is a trigger signal */
        if (scheduledOutputs[outputIndex].trigger) {
            triggerPackage.pid = TRIGGER_SIGNAL_TRANSMISSION_PID;
            /** Send trigger signal */
            call SDNSend.send(destinationFlowId,
                              (void *) &triggerPackage,
                              sizeof(dataPackage));
            return;
        }

        /** Prepare data transmission package */
        dataPackage.pid = DATA_TRANSMISSION_PID;
        dataPackage.odt = scheduledOutputs[outputIndex].dataType;
        dataPackage.odl = scheduledOutputs[outputIndex].dataLength;
        dataPackage.td = *((uint64_t *) value);

        /** Send data package */
        call SDNSend.send(destinationFlowId, 
                          (void *) &dataPackage, 
                          sizeof(dataPackage));
    }

/************************************************************************
 * Protocol Output Interface Events
 ************************************************************************/
    event void ProtocolOutput.ready(uint16_t destinationFlowId, 
                                    uint8_t *protocolResponse,
                                    uint8_t responseLength) {
        /** Send protocol response */
        call SDNSend.send(destinationFlowId,
                          (void *) protocolResponse,
                          responseLength);
    }
}
