# WARM - Middleware #

This directory contains files related to the WARM framework's middleware component.

General directory structure is as follows:

  - Middleware components are kept under "components" subdirectory.
  - Middleware interfaces are kept under "interfaces" subdirectory.
  - Common header files are kept under "libs" subdirectory.
  - Task components are kept under the "tasks" subdirectory.
  - Development tools are kept under the "tools" subdirectory.
  - Test units for components are kept under "tests" subdirectory.

### Documentation ###

Documentation can be automatically generated through the nesdoc tool provided with TinyOS.

To generate (e.g. for the TelosB platform) html based documentation for the project, run "make telosb docs" from the middleware directory.

Generated documentation will be output to directory "doc/nesdoc/telosb". We sugest starting with file "doc/nesdoc/telosb/chtml/MiddlewareC.html".

### Programming Environment ###

To setup and run a docker based programming environment for working with the middleware and TinyOS, make use of script "tinyos_docker.sh" in the "tools" subdirectory.

To compile (e.g. for TelosB platform) MiddlewareC component, simply run "make telosb".

To compile (e.g. for TelosB platform) a specific component, go to its subdirectory under components and run "make telosb".

To compile (e.g. for TelosB platform) a specific component's test unit, go to its subdirectory under tests and run "make telosb".

### TinySDN ###

WARM's middleware depends on the TinySDN component as a framework for SDN-enabled Wireless Sensor Networks. To build the project, it is necessary to have a copy of the TinySDN source code added to your TinyOS source.
