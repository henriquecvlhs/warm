/**
 * @file protocol.h
 * @brief Header file providing communication protocol implementation.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include "protocol/NodeCharacteristicsDescription.h"
#include "protocol/NodeAssociateConfirmation.h"
#include "protocol/TaskDescriptionRequest.h"
#include "protocol/TaskDescriptionResponse.h"
#include "protocol/PeriodicTaskSchedulingRequest.h"
#include "protocol/InstantaneousTaskSchedulingRequest.h"
#include "protocol/TriggerTaskSchedulingRequest.h"
#include "protocol/TaskSchedulingConfirmation.h"
#include "protocol/ScheduledTaskCancellationRequest.h"
#include "protocol/ScheduledTaskCancellationResponse.h"
#include "protocol/TaskStatisticsRequest.h"
#include "protocol/TaskStatisticsResponse.h"
#include "protocol/NodeStatisticsRequest.h"
#include "protocol/NodeStatisticsResponse.h"
#include "protocol/DataTransmission.h"
#include "protocol/TriggerSignalTransmission.h"

#define MAX_PAYLOAD_SIZE 90

#endif // __PROTOCOL_H__
