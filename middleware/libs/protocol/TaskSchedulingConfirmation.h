/**
 * @file TaskSchedulingConfirmation.h
 * @brief Header file describing Task Scheduling Confirmation protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TASK_SCHEDULING_CONFIRMATION_H__
#define __TASK_SCHEDULING_CONFIRMATION_H__

/**
 * @brief Package nx_structure of a Task Scheduling Confirmation Package.
 * @note Package may have PID 3, 4 or 5.
 */
#define PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID      3
#define INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID 4
#define TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID       5

typedef nx_struct TaskSchedulingConfirmation {
    nx_struct {
        nx_uint32_t pid :  4;
        nx_uint32_t tid : 16;
        nx_uint32_t     :  4; // Reserved
        nx_uint32_t tin :  8;
    };

    nx_union {
        nx_struct {
            nx_uint8_t s : 1;
            nx_uint8_t t : 1;
            nx_uint8_t i : 1;
            nx_uint8_t n : 1;
            nx_uint8_t p : 1;
            nx_uint8_t d : 1;
            nx_uint8_t r : 1;
            nx_uint8_t   : 1; // Reserved
        };
        nx_uint8_t code;
    } error;
} TaskSchedulingConfirmation_t;

#endif // __TASK_SCHEDULING_CONFIRMATION_H__
