/**
 * @file TriggerTaskSchedulingRequest.h
 * @brief Header file describing Trigger Task Scheduling Request protocol 
 *        package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TRIGGER_TASK_SCHEDULING_REQUEST_H__
#define __TRIGGER_TASK_SCHEDULING_REQUEST_H__

/**
 * @brief Package nx_structure of a Trigger Task Scheduling Request Package.
 * @note Package of PID 5.
 */
#define TRIGGER_TASK_SCHEDULING_REQUEST_PID          5

typedef nx_struct TriggerTaskSchedulingRequest {
    nx_struct {
        nx_uint32_t pid :  4;
        nx_uint32_t tid : 16;
        nx_uint32_t tmc :  3;
        nx_uint32_t tsc :  3;
        nx_uint32_t tcs :  3;
        nx_uint32_t trc :  3;
    };

    nx_uint64_t tmi;
    nx_uint64_t tsi;
    nx_uint64_t tri;

    nx_uint16_t ofi;
} TriggerTaskSchedulingRequest_t;

#endif // __TRIGGER_TASK_SCHEDULING_REQUEST_H__
