/**
 * @file NodeStatisticsResponse.h
 * @brief Header file describing Node Statistics Response protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NODE_STATISTICS_RESPONSE_H__
#define __NODE_STATISTICS_RESPONSE_H__

/**
 * @brief Package nx_structure of a Node Statistics Response Package.
 * @note Package of PID 8.
 */
#define NODE_STATISTICS_RESPONSE_PID                 8

typedef nx_struct NodeStatisticsResponse {
    nx_struct {
        nx_uint32_t pid :  4;
        nx_uint32_t cpq :  8;
        nx_uint32_t ciq :  8;
        nx_uint32_t cnq :  8;
        nx_uint32_t     :  4; // Reserved
    };

    nx_uint8_t cbl;
    nx_uint8_t crp;

    nx_uint32_t latitude;
    nx_uint32_t longitude;
    nx_uint32_t height;

    nx_uint8_t csq[MAX_LOADED_TASKS];
} NodeStatisticsResponse_t;

#endif // __NODE_STATISTICS_RESPONSE_H__
