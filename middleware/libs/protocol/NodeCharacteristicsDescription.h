/**
 * @file NodeCharacteristicsDescription.h
 * @brief Header file describing Node Characteristics Description protocol 
 *        package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NODE_CHARACTERISTICS_DESCRIPTION_H__
#define __NODE_CHARACTERISTICS_DESCRIPTION_H__

/**
 * @brief Package strutcture of a Sensor Node Characteristics Description Package.
 * @note Package of PID 0.
 */
#define NODE_CHARACTERISTICS_DESCRIPTION_PID         0

#define MAX_LOADED_TASKS   32

/** Field "m" options */
#define NODE_MOBILITY_STATIC 0
#define NODE_MOBILITY_MOBILE 1

/** Field "e" options */
#define NODE_ENERGY_SOURCE_UNLIMITED 0
#define NODE_ENERGY_SOURCE_LIMITED   1

/** Field "did" options */
enum {
    DEVICE_ID_UNKNOWN = 1,
    DEVICE_ID_TELOSB = 2,
};

/** Field "os" options */
enum {
    OS_ID_UNKNOWN = 1,
    OS_ID_TINYOS = 2,
};


typedef nx_struct NodeCharacteristicsDescription {
    nx_struct {
        nx_uint16_t pid :  4;
        nx_uint16_t nid : 10;
        nx_uint16_t m   :  1;
        nx_uint16_t e   :  1;
    };

    nx_uint16_t did;
    nx_uint8_t  os;

    nx_uint32_t latitude;
    nx_uint32_t longitude;
    nx_uint32_t height;

    nx_struct {
        nx_uint32_t ptq : 8;
        nx_uint32_t idq : 8;
        nx_uint32_t ntq : 8;
        nx_uint32_t ntn : 5;
        nx_uint32_t     : 3; // Reserved
    };

    nx_uint16_t tid[MAX_LOADED_TASKS];
} NodeCharacteristicsDescription_t;

#endif // __NODE_CHARACTERISTICS_DESCRIPTION_H__
