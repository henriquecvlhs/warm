/**
 * @file TaskDescriptionResponse.h
 * @brief Header file describing Task Description Response protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TASK_DESCRIPTION_RESPONSE_H__
#define __TASK_DESCRIPTION_RESPONSE_H__

/**
 * @brief Package nx_structure of a Task Description Response Package.
 * @note Package of PID 2.
 */
#define TASK_DESCRIPTION_RESPONSE_PID                2

typedef nx_struct TaskDescriptionResponse {
    nx_struct {
        nx_uint64_t pid  :  4;
        nx_uint64_t tid  : 16;
        nx_uint64_t tti  :  2;
        nx_uint64_t      :  2; // Reserved
        nx_uint64_t msq  :  8;
        nx_uint64_t odf  : 10;
        nx_uint64_t ipn  :  4;
        nx_uint64_t      :  2; // Reserved
        nx_uint64_t d    :  1;
        nx_uint64_t a    :  1;
        nx_uint64_t g    :  1;
        nx_uint64_t s    :  1;
        nx_uint64_t      : 12; // Reserved
    };

    nx_struct {
        nx_uint16_t ipq :  6;
        nx_uint16_t ipf : 10;
    } iparam[16];
} TaskDescriptionResponse_t;

#endif // __TASK_DESCRIPTION_RESPONSE_H__
