/**
 * @file NodeAssociateConfirmation.h
 * @brief Header file describing Node Association Confirmation protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NODE_ASSOCIATE_CONFIRMATION_H__
#define __NODE_ASSOCIATE_CONFIRMATION_H__

/**
 * @brief Package nx_structure of a Sensor Node Associate Confirmation Package.
 * @note Package of PID 0.
 */
#define NODE_ASSOCIATE_CONFIRMATION_PID              1

typedef nx_struct NodeAssociateConfirmation {
    nx_uint32_t pid :  4;
    nx_uint32_t cfi : 16; 
    nx_uint32_t     : 12; // Reserved
} NodeAssociateConfirmation_t;

#endif // __NODE_ASSOCIATE_CONFIRMATION_H__
