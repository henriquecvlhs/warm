/**
 * @file TriggerSignalTransmission.h
 * @brief Header file describing Trigger Signal Transmission protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TRIGGER_SIGNAL_TRANSMISSION_H__
#define __TRIGGER_SIGNAL_TRANSMISSION_H__

/**
 * @brief Package nx_structure of a Trigger Signal Transmission Package.
 * @note Package of PID 10.
 */
#define TRIGGER_SIGNAL_TRANSMISSION_PID             10

typedef nx_struct TriggerSignalTransmission {
    nx_uint8_t pid : 4;
    nx_uint8_t     : 4; // Reserved
} TriggerSignalTransmission_t;

#endif // __TRIGGER_SIGNAL_TRANSMISSION_H__
