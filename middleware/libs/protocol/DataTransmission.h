/**
 * @file DataTransmission.h
 * @brief Header file describing Data Transmission protocol package.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DATA_TRANSMISSION_H__
#define __DATA_TRANSMISSION_H__

/**
 * @brief Package nx_structure of a Data Transmission Package.
 * @note Package of PID 9.
 */
#define DATA_TRANSMISSION_PID                        9

typedef nx_struct DataTransmission {
    nx_struct {
        nx_uint16_t pid : 4;
        nx_uint16_t odt : 3;
        nx_uint16_t odl : 2;
        nx_uint16_t     : 7; // Reserved
    };

    nx_uint64_t td;
} DataTransmission_t;

#endif // __DATA_TRANSMISSION_H__
