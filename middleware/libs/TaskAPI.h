/**
 * @file TaskAPI.h
 * @brief Header file providing Task API functionalities.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TASK_API_H__
#define __TASK_API_H__

#include "protocol/TaskDescriptionResponse.h"

/****************************************************************************** 
 * Task types 
 ******************************************************************************/

/**
 * Task type
 */
typedef enum TaskType {
    INSTANTANEOUS_TASK_TYPE = 1,
    PERIODIC_TASK_TYPE = 2,
    TRIGGER_TASK_TYPE = 3,
} TaskType_t;

/**
 * Task trigger policies 
 */
typedef enum TaskTriggerPolicy {
    NO_TRIGGER = 0,
    TRIGGER_ONCE = 2,
    TRIGGER_ALWAYS = 3
} TaskTriggerPolicy_t;

/**
 * Trigger task rules
 */
typedef enum TaskTriggerStorageRule {
    TRIGGER_CONSTANT = 0,
    TRIGGER_INPUT_PARAMETER,
    TRIGGER_STORE_MOST_RECENT_LEFTMOST,
    TRIGGER_STORE_GREATER_LEFTMOST,
    TRIGGER_STORE_LESS_LEFTMOST,
    TRIGGER_STORE_MOST_RECENT_RIGHTMOST,
    TRIGGER_STORE_GREATER_RIGHTMOST,
    TRIGGER_STORE_LESS_RIGHTMOST
} TaskTriggerStorageRule_t;

typedef enum TaskTriggerComparisonRule {
    TRIGGER_ON_EQUAL = 0,
    TRIGGER_ON_DIFFERENT,
    TRIGGER_ON_LESS,
    TRIGGER_ON_GREATER,
    TRIGGER_ON_LESS_OR_EQUAL,
    TRIGGER_ON_GREATER_OR_EQUAL,
    TRIGGER_ON_LOGIC_AND,
    TRIGGER_ON_LOGIC_OR
} TaskTriggerComparisonRule_t;

/**
 * Task parameter status
 */
typedef enum TaskInputParameterStatus {
    INPUT_STATUS_NOT_READY = 0,
    INPUT_STATUS_READY,
} TaskInputParameterStatus_t;

/**
 * Task parameter storage policies
 */
typedef enum TaskInputParameterStoragePolicy {
    STORE_LEAST_RECENT_INPUT = 0, // Discard recently arrived data
    STORE_MOST_RECENT_INPUT = 1,
} TaskInputParameterStoragePolicy_t;

/****************************************************************************** 
 * Task constants 
 ******************************************************************************/

/** Maximum size for instantaneous tasks' argument strings */
#define MAX_ARG_STRING_LENGTH 31

/****************************************************************************** 
 * Possible task roles
 ******************************************************************************/

/** Task outputs data to the network */
enum {
    OUTPUT_TO_NETWORK_FALSE = 0,
    OUTPUT_TO_NETWORK_TRUE = 1,
};

/** Task controls an actuator */
enum {
    CONTROL_ACTUATOR_FALSE = 0,
    CONTROL_ACTUATOR_TRUE = 1,
};

/** Task aggregates data */
enum {
    AGGREGATE_DATA_FALSE = 0,
    AGGREGATE_DATA_TRUE = 1,
};

/** Task acts as network sink */
enum {
    DATA_SINK_FALSE = 0,
    DATA_SINK_TRUE = 1,
};

/****************************************************************************** 
 * Task supported input / output data formats
 ******************************************************************************/

/** Supported data types */
#define SUPPORT_FLOAT      (1 << 9)
#define SUPPORT_FIXED      (1 << 8)
#define SUPPORT_INT        (1 << 7)
#define SUPPORT_UINT       (1 << 6)
#define SUPPORT_BIT_ARRAY  (1 << 5)
#define SUPPORT_BOOL       (1 << 4)

/** Supported data lengths */
#define SUPPORT_1_BYTE_LENGTH (1 << 3)
#define SUPPORT_2_BYTE_LENGTH (1 << 2)
#define SUPPORT_4_BYTE_LENGTH (1 << 1)
#define SUPPORT_8_BYTE_LENGTH (1 << 0)

/** No output / input supported */
#define SUPPORT_NO_DATA (0b0000000000)

/****************************************************************************** 
 * Task macros 
 ******************************************************************************/

/** Wire task interface */
#define WIRE_TASK_INTERFACE(taskId, taskComponent) \
            TaskAPIP.Task[taskId] -> taskComponent

/** Signal task output data to Task API */
#define OUTPUT_TO_NETWORK(taskInstance, outputData) \
            signal Task.outputReady(taskInstance, ((void *) &outputData))

/** Schedule status manipulation macros */
#define GET_SCHEDULE_STATUS(taskInstance) \
            (scheduleStatus[taskInstance >> 3] & (1 << (taskInstance & 0x07)))

#define SET_SCHEDULE_STATUS(taskInstance) \
            (scheduleStatus[taskInstance >> 3] |= (1 << (taskInstance & 0x07)))

#define CLEAR_SCHEDULE_STATUS(taskInstance) \
            (scheduleStatus[taskInstance >> 3] &= ~(1 << (taskInstance & 0x07)))

/** Trigger status manipulation macros */
#define GET_TRIGGER_STATUS(taskInstance) \
            (triggerStatus[taskInstance >> 3] & (1 << (taskInstance & 0x07)))

#define SET_TRIGGER_STATUS(taskInstance) \
            (triggerStatus[taskInstance >> 3] |= (1 << (taskInstance & 0x07)))

#define CLEAR_TRIGGER_STATUS(taskInstance) \
            (triggerStatus[taskInstance >> 3] &= ~(1 << (taskInstance & 0x07)))

/** Initialize description macro */
#define INITIALIZE_TASK_DESCRIPTION(outputFormat,                         \
                                    networkOutput, actuatorControl,       \
                                    dataAggregation, dataSink)            \
                        description.pid = TASK_DESCRIPTION_RESPONSE_PID;  \
                        description.tid = TASK_ID;                        \
                        description.tti = TASK_TYPE;                      \
                        description.msq = NUMBER_OF_INSTANCES;            \
                        description.odf = outputFormat;                   \
                        description.ipn = NUMBER_OF_INPUTS;               \
                        description.d   = networkOutput;                  \
                        description.a   = actuatorControl;                \
                        description.g   = dataAggregation;                \
                        description.s   = dataSink                        \

#define INITILIZE_TASK_INPUT_DESCRIPTION(inputId, inputSize, inputFormat) \
                        description.iparam[inputId].ipq = inputSize;      \
                        description.iparam[inputId].ipf = inputFormat

/****************************************************************************** 
 * Task implementation helpers 
 ******************************************************************************/

/** General helper */
#define PERIODIC_TASK_API_HELPER                                    \
    PERIODIC_TASK_AUXILIARY_VARIABLES_HELPER                        \
    PROGRAMMER_FUNCTION_PROTOTYPES_HELPER                           \
    PERIODIC_TASK_AUXILIARY_FUNCTIONS_HELPER                        \
    PERIODIC_TASK_INIT_HELPER                                       \
    TASK_RUN_HELPER                                                 \
    PERIODIC_TASK_RECEIVE_DATA_HELPER                               \
    PERIODIC_TASK_DISCARD_DATA_HELPER                               \
    TASK_SCHEDULE_HELPER                                            \
    PERIODIC_TASK_CONFIGURE_MAX_INPUT_HELPER                        \
    PERIODIC_TASK_CONFIGURE_ARGUMENT_STRING_HELPER                  \
    PERIODIC_TASK_CANCEL_HELPER                                     \
    TASK_TRIGGER_HELPER                                             \
    TASK_GET_DESCRIPTION_HELPER                                     \
    TASK_GET_EXECUTION_STATUS_HELPER                                \
    TASK_IS_SCHEDULED_HELPER                                        

/** General helper for Instantaneous Tasks */
#define INSTANTANEOUS_TASK_API_HELPER                               \
    INSTANTANEOUS_TASK_AUXILIARY_VARIABLES_HELPER                   \
    PROGRAMMER_FUNCTION_PROTOTYPES_HELPER                           \
    INSTANTANEOUS_TASK_AUXILIARY_FUNCTIONS_HELPER                   \
    INSTANTANEOUS_TASK_INIT_HELPER                                  \
    TASK_RUN_HELPER                                                 \
    INSTANTANEOUS_TASK_RECEIVE_DATA_HELPER                          \
    INSTANTANEOUS_TASK_DISCARD_DATA_HELPER                          \
    TASK_SCHEDULE_HELPER                                            \
    INSTANTANEOUS_TASK_CONFIGURE_MAX_INPUT_HELPER                   \
    INSTANTANEOUS_TASK_CONFIGURE_ARGUMENT_STRING_HELPER             \
    INSTANTANEOUS_TASK_CANCEL_HELPER                                \
    TASK_TRIGGER_HELPER                                             \
    TASK_GET_DESCRIPTION_HELPER                                     \
    TASK_GET_EXECUTION_STATUS_HELPER                                \
    TASK_IS_SCHEDULED_HELPER                                        \
    TASK_PARAMETER_INPUT_READY_EVENT_HELPER                         \
    TASK_PARAMETER_INPUT_DEFAULTS_HELPER                            

/** Task auxiliary variables helper */
#define PERIODIC_TASK_AUXILIARY_VARIABLES_HELPER                    \
    /** Keep track of scheduled instances */                        \
    uint8_t scheduleStatus[1 + (NUMBER_OF_INSTANCES - 1)/8];        \
                                                                    \
    /** Keep track of trigger policies for each instance */         \
    TaskTriggerPolicy_t triggerPolicies[NUMBER_OF_INSTANCES];       \
                                                                    \
    /** Keep track of trigger status for each instance */           \
    uint8_t triggerStatus[1 + (NUMBER_OF_INSTANCES - 1)/8];         \
                                                                    \
    /** Keep track of execution status for each instance */         \
    uint16_t executionStatus[NUMBER_OF_INSTANCES];                  \
                                                                    \
    TaskDescriptionResponse_t description; 

#define INSTANTANEOUS_TASK_AUXILIARY_VARIABLES_HELPER               \
    PERIODIC_TASK_AUXILIARY_VARIABLES_HELPER                        \
    /** Keep track of argument string for each instance */          \
    char argString[NUMBER_OF_INSTANCES][MAX_ARG_STRING_LENGTH];     \
                                                                    \
    /** Keep track of argument string length for each instance */   \
    char argStringLength[NUMBER_OF_INSTANCES];                      \

/** Task programmer function prototypes helper */
#define PROGRAMMER_FUNCTION_PROTOTYPES_HELPER                       \
    void taskInit(void);                                            \
    uint8_t taskRoutine(uint8_t taskInstance);

/** Task auxiliary functions helper */
#define PERIODIC_TASK_AUXILIARY_FUNCTIONS_HELPER                    \
    /** Check if task instance is ready to run */                   \
    uint8_t checkIfReadyToRun(uint8_t taskInstance) {               \
        /** Check if input parameters are ready */                  \
        uint8_t ready = 1;                                          \
                                                                    \
        /** Check trigger status */                                 \
        if (!GET_TRIGGER_STATUS(taskInstance)) {                    \
            ready = 0;                                              \
        }                                                           \
                                                                    \
        return ready;                                               \
    }

#define INSTANTANEOUS_TASK_AUXILIARY_FUNCTIONS_HELPER               \
    /** Check if task instance is ready to run */                   \
    uint8_t checkIfReadyToRun(uint8_t taskInstance) {               \
        /** Check if input parameters are ready */                  \
        uint8_t ready = 1;                                          \
        uint8_t i;                                                  \
        for (i = 0; i < NUMBER_OF_INPUTS; i++) {                    \
            if (call input.status[i](taskInstance) != INPUT_STATUS_READY) { \
                ready = 0;                                          \
            }                                                       \
        }                                                           \
                                                                    \
        /** Check trigger status */                                 \
        if (!GET_TRIGGER_STATUS(taskInstance)) {                    \
            ready = 0;                                              \
        }                                                           \
                                                                    \
        return ready;                                               \
    }

/** Task run helper */
#define TASK_RUN_HELPER                                             \
    command void Task.run(uint8_t taskInstance) {                   \
        /** We trust that the scheduler will only call this if the             task is truly ready to run */ \
                                                                    \
        uint8_t success;                                            \
                                                                    \
        /** Check if running scheduled instance */                  \
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;             \
                                                                    \
        /** Perform task */                                         \
        success = taskRoutine(taskInstance);                        \
                                                                    \
        /** Check execution status */                               \
        if (success) {                                              \
            /** Increase execution counter */                       \
            executionStatus[taskInstance]++;                        \
        }                                                           \
                                                                    \
        /** Clear trigger if trigger always policy is set */        \
        if (triggerPolicies[taskInstance] == TRIGGER_ALWAYS) {      \
            CLEAR_TRIGGER_STATUS(taskInstance);                     \
        }                                                           \
    }

/** Task init helper */
#define PERIODIC_TASK_INIT_HELPER                                   \
    command void Task.init(void) {                                  \
        uint8_t i;                                                  \
                                                                    \
        /** Initialize instances */                                 \
        for (i = 0; i < NUMBER_OF_INPUTS; i++) {                    \
            CLEAR_SCHEDULE_STATUS(i);                               \
            triggerPolicies[i] = NO_TRIGGER;                        \
            SET_TRIGGER_STATUS(i);                                  \
            executionStatus[i] = 0;                                 \
        }                                                           \
                                                                    \
        /** Call Task API user initialization */                    \
        taskInit();                                                 \
    }

#define INSTANTANEOUS_TASK_INIT_HELPER                              \
    command void Task.init(void) {                                  \
        uint8_t i;                                                  \
                                                                    \
        /** Initialize instances */                                 \
        for (i = 0; i < NUMBER_OF_INPUTS; i++) {                    \
            call input.init[i]();                                   \
            CLEAR_SCHEDULE_STATUS(i);                               \
            triggerPolicies[i] = NO_TRIGGER;                        \
            SET_TRIGGER_STATUS(i);                                  \
            executionStatus[i] = 0;                                 \
            argString[i][0] = 0x00;                                 \
            argStringLength[i] = 0;                                 \
        }                                                           \
                                                                    \
        /** Call Task API user initialization */                    \
        taskInit();                                                 \
    }

/** Task receiveData helper */
#define PERIODIC_TASK_RECEIVE_DATA_HELPER                           \
    command void Task.receiveData(uint8_t taskInstance,             \
                                  uint8_t parameterId,              \
                                  void *value) {                    \
    }

#define INSTANTANEOUS_TASK_RECEIVE_DATA_HELPER                      \
    command void Task.receiveData(uint8_t taskInstance,             \
                                  uint8_t parameterId,              \
                                  void *value) {                    \
        /** Check if running scheduled instance */                  \
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;             \
                                                                    \
        call input.receive[parameterId](taskInstance, value);       \
    }

/** Task discardData helper */
#define PERIODIC_TASK_DISCARD_DATA_HELPER                           \
    command void Task.discardData(uint8_t taskInstance) {           \
    }

#define INSTANTANEOUS_TASK_DISCARD_DATA_HELPER                      \
    command void Task.discardData(uint8_t taskInstance) {           \
        uint8_t i;                                                  \
        for (i - 0; i < NUMBER_OF_INPUTS; i++) {                    \
            call input.discard[i](taskInstance);                    \
        }                                                           \
    }

/** Task schedule helper */
#define TASK_SCHEDULE_HELPER                                        \
    command uint8_t Task.schedule(uint8_t *taskInstance,            \
                                  TaskTriggerPolicy_t triggerPolicy) { \
        /** Search for available task instance */                   \
        uint8_t i = 0;                                              \
        uint8_t foundFreeInstance = 0;                              \
        for (i = 0; i < NUMBER_OF_INSTANCES; i++) {                 \
            if (GET_SCHEDULE_STATUS(i) == 0) {                      \
                SET_SCHEDULE_STATUS(i);                             \
                *taskInstance = i;                                  \
                foundFreeInstance = 1;                              \
                break;                                              \
            }                                                       \
        }                                                           \
                                                                    \
        /** Failure in case no instance is available */             \
        if (foundFreeInstance == 0) return 1;                       \
                                                                    \
        /** Configure trigger policy */                             \
        triggerPolicies[*taskInstance] = triggerPolicy;             \
                                                                    \
        /** Configure trigger status */                             \
        if (triggerPolicy != NO_TRIGGER) {                          \
            CLEAR_TRIGGER_STATUS(*taskInstance);                    \
        }                                                           \
        else {                                                      \
            SET_TRIGGER_STATUS(*taskInstance);                      \
        }                                                           \
                                                                    \
        /** Check if task is already ready to run */                \
        if (checkIfReadyToRun(*taskInstance)) {                     \
            signal Task.readyToRun(*taskInstance, TASK_TYPE);       \
        }                                                           \
                                                                    \
        return 0;                                                   \
    }

/** Task configureMaxInput helper */
#define PERIODIC_TASK_CONFIGURE_MAX_INPUT_HELPER                    \
    command void Task.configureMaxInput(uint8_t taskInstance,       \
                                        uint8_t parameterId,        \
                                        uint8_t inputMaxSize) {     \
    }

#define INSTANTANEOUS_TASK_CONFIGURE_MAX_INPUT_HELPER               \
    command void Task.configureMaxInput(uint8_t taskInstance,       \
                                        uint8_t parameterId,        \
                                        uint8_t inputMaxSize) {     \
        /** Check if running scheduled instance */                  \
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;             \
                                                                    \
        call input.setInstanceSize[parameterId](taskInstance, inputMaxSize); \
    }

/** Task configureArgumentString helper */
#define PERIODIC_TASK_CONFIGURE_ARGUMENT_STRING_HELPER              \
    command void Task.configureArgumentString(uint8_t taskInstance, \
                                              char *argumentString) { \
    }

#define INSTANTANEOUS_TASK_CONFIGURE_ARGUMENT_STRING_HELPER         \
    command void Task.configureArgumentString(uint8_t taskInstance, \
                                              char *argumentString) { \
        uint8_t i;                                                  \
                                                                    \
        /** Check if running scheduled instance */                  \
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;             \
                                                                    \
        /** Check argument string validity */                       \
        if (argumentString == NULL) return;                         \
                                                                    \
        argStringLength[taskInstance] = 0;                          \
        for (i = 0; i < MAX_ARG_STRING_LENGTH; i++) {               \
            argString[taskInstance][i] = argumentString[i];         \
                                                                    \
            if (argumentString[i] == 0x00) break;                   \
                                                                    \
            argStringLength[taskInstance] += 1;                     \
        }                                                           \
        argString[taskInstance][i] = 0x00; /* Make sure string ends */ \
    }

/** Task cancel helper */
#define PERIODIC_TASK_CANCEL_HELPER                                 \
    command void Task.cancel(uint8_t taskInstance) {                \
        CLEAR_SCHEDULE_STATUS(taskInstance);                        \
        executionStatus[taskInstance] = 0;                          \
    }

#define INSTANTANEOUS_TASK_CANCEL_HELPER                            \
    command void Task.cancel(uint8_t taskInstance) {                \
        CLEAR_SCHEDULE_STATUS(taskInstance);                        \
        executionStatus[taskInstance] = 0;                          \
        argString[taskInstance][0] = 0x00;                          \
        argStringLength[taskInstance] = 0;                          \
    }

/** Task trigger helper */
#define TASK_TRIGGER_HELPER                                         \
    command void Task.trigger(uint8_t taskInstance) {               \
        SET_TRIGGER_STATUS(taskInstance);                           \
                                                                    \
        if (checkIfReadyToRun(taskInstance)) {                      \
            signal Task.readyToRun(taskInstance, TASK_TYPE);        \
        }                                                           \
    }

/** Task getDescription helper */
#define TASK_GET_DESCRIPTION_HELPER                                 \
    command void *Task.getDescription(void) {                       \
        return ((void *) &description);                             \
    }

/** Task getExecutionStatus helper */
#define TASK_GET_EXECUTION_STATUS_HELPER                            \
    command uint16_t Task.getExecutionStatus(uint8_t taskInstance) {\
        return executionStatus[taskInstance];                       \
    }

/** Task isScheduled helper */
#define TASK_IS_SCHEDULED_HELPER                                    \
    command uint8_t Task.isScheduled(uint8_t taskInstance) {        \
        return GET_SCHEDULE_STATUS(taskInstance);                   \
    }

/** Task Parameter Input ready event helper */
#define TASK_PARAMETER_INPUT_READY_EVENT_HELPER                     \
    event   void input.ready[uint8_t index](uint8_t taskInstance) { \
        if (checkIfReadyToRun(taskInstance)) {                      \
            signal Task.readyToRun(taskInstance, TASK_TYPE);        \
        }                                                           \
    }

/** Task Parameter Input defaults helper */
#define TASK_PARAMETER_INPUT_DEFAULTS_HELPER                        \
    default command void                                            \
    input.init[uint8_t index](void) {                               \
    }                                                               \
                                                                    \
    default command void                                            \
    input.setInstanceSize[uint8_t index](uint8_t taskInstance,      \
                                         uint8_t size) {            \
    }                                                               \
                                                                    \
    default command void                                            \
    input.receive[uint8_t index](uint8_t taskInstance,              \
                                 void *inputData) {                 \
    }                                                               \
                                                                    \
    default command void                                            \
    input.discard[uint8_t index](uint8_t taskInstance) {            \
    }                                                               \
                                                                    \
    default command TaskInputParameterStatus_t                      \
    input.status[uint8_t index](uint8_t taskInstance) {             \
        return INPUT_STATUS_NOT_READY;                              \
    }                                                               \
                                                                    \
    default command void                                            \
    input.configureStoragePolicy[uint8_t index](                    \
                                    TaskInputParameterStoragePolicy_t policy) { \
    }

#endif // __TASK_API_H__
