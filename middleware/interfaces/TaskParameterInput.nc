/**
 * @file TaskParameterInput.nc
 * @brief File describing the Task Parameter Input interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface that abstracts access to task input parameters in a generic
 *        way, unrelated to the access of data stored by such parameters.
 */
interface TaskParameterInput {
    /**
     * @brief Initialize task parameter input interface during system 
     *        initialization.
     */
    command void init(void);

    /**
     * @brief Set maximum amount of data the parameter input of a given
     *        task instance can hold.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] size The maximum amount of data the parameter can hold.
     */
    command void setInstanceSize(uint8_t taskInstance, uint8_t size);

    /**
     * @brief Receive input data for parameter of a given task instance.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] inputData Pointer to received input data.
     */
    command void receive(uint8_t taskInstance, void *inputData);

    /**
     * @brief Signalize parameter has all data needed for given task 
     *        instance execution.
     *
     * @param[in] taskInstance Task instance number.
     */
    event   void ready(uint8_t taskInstance);

    /**
     * @brief Discard parameter data stored for a given task instance.
     *
     * @param[in] Task instance number.
     */
    command void discard(uint8_t taskInstance);

    /**
     * @brief Get input parameter status for a given task instance.
     *
     * @param[in] taskInstance Task instance number.
     *
     * @return Input parameter status
     * @retval INPUT_STATUS_NOT_READY Parameter does not hold enough data for
     *                                task instance execution.
     * @retval INPUT_STATUS_READY     Parameter does hold enough data for
     *                                task instance execution.
     */
    command TaskInputParameterStatus_t status(uint8_t taskInstance);

    /**
     * @brief Configure storage policy for a task input parameter.
     *
     * @param[in] policy Rule indicating if task should store or discard
     *                   received data when no storage space is available.
     */
    command void configureStoragePolicy(
                                    TaskInputParameterStoragePolicy_t policy);
}
