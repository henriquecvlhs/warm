/**
 * @file InputScheduler.nc
 * @brief File describing the Input Scheduler interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface for scheduling the receiving of task input data from the
 *        Receiver component.
 */
interface InputScheduler {
    /**
     * @brief Schedule the receiving of data for a task instance.
     *
     * @param[in] taskId ID of the task to send input data to.
     * @param[in] taskInstance Task instance to send input data to.
     * @param[in] parameterId ID of the parameter to which the received 
     *                        data corresponds.
     * @param[in] inputFlowId ID of the network flow which will bring
     *                        input data.
     *
     * @return Result of the scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleTaskInput(uint16_t taskId,
                                      uint8_t taskInstance,
                                      uint8_t parameterId,
                                      uint16_t inputFlowId);

    /**
     * @brief Cancel the receiving of data scheduled for a task instance.
     *
     * @param[in] taskId ID of the task to be cancelled.
     * @param[in] taskInstance Task instance to be cancelled.
     */
    command void cancelTaskInput(uint16_t taskId,
                                 uint8_t taskInstance);
}
