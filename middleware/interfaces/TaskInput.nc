/**
 * @file TaskInput.nc
 * @brief File describing the Task Input interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Interface for providing input data to tasks through the Task API
 *        component.
 */
interface TaskInput {
    /**
     * @brief Feed input data to task instance.
     *
     * @param[in] taskId       ID of the task we will feed data to.
     * @param[in] taskInstance Number of the task instance we will feed data to.
     * @param[in] inputId      ID of the input parameter we will feed data to.
     * @param[in] value        Pointer to input value.
     */
    command void data(uint16_t taskId, 
                      uint8_t taskInstance, 
                      uint8_t inputId,
                      void *value);
    /**
     * @brief Trigger a task instance.
     *
     * @param[in] taskId       ID of the task whose instance will be triggered.
     * @param[in] taskInstance Number of the task instance we will trigger.
     */
    command void trigger(uint16_t taskId, uint8_t taskInstance);
}
