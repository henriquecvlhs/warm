/**
 * @file EmitterStatus.nc
 * @brief File describing the Emitter Status interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface for inquiring network output scheduling status from the
 *        Emitter component.
 */
interface EmitterStatus {
    /**
     * @brief Get the current number of schedules for the emission of data
     *        to the network as the output of tasks.
     *
     * @return The current number of schedules for the output of task data.
     */
    command uint8_t getOutputSchedulesStatus(void);
}
