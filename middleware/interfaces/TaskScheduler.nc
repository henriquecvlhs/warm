/**
 * @file TaskScheduler.nc
 * @brief File describing the Task Scheduler interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Interface providing task scheduling commands from the Task API
 *        component to the Scheduler component.
 */
interface TaskScheduler {
    /**
     * @brief Schedule a periodic task.
     *
     * @note taskInstance output should only be considered in case the 
     *       scheduling was successful.
     *
     * @param[in] taskId        ID of the task to be scheduled.
     * @param[out] taskInstance Pointer to the task instance corresponding to
     *                          the requested scheduling.
     * @param[in] triggerPolicy Task triggering policy for this scheduling.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t schedulePeriodicTask(uint16_t taskId, 
                                         uint8_t *taskInstance,
                                         TaskTriggerPolicy_t triggerPolicy);

    /**
     * @brief Schedule an instantaneous task.
     *
     * @note taskInstance output should only be considered in case the 
     *       scheduling was successful.
     *
     * @param[in] taskId         ID of the task to be scheduled.
     * @param[out] taskInstance  Pointer to the task instance corresponding to
     *                           the requested scheduling.
     * @param[in] triggerPolicy  Task triggering policy for this scheduling.
     * @param[in] inputMaxSize   Array containing the sizes to be configured for
     *                           each of the scheduled instance's inputs.
     * @param[in] argumentString String carrying extra configuration parameters
     *                           for the scheduled instance.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleInstantaneousTask(uint16_t taskId, 
                                              uint8_t *taskInstance,
                                              TaskTriggerPolicy_t triggerPolicy,
                                              uint8_t *inputSize,
                                              char *argumentString);

    /**
     * @brief Schedule a trigger task.
     *
     * @param[in] taskId         ID of the task to be scheduled.
     * @param[out] taskInstance  Pointer to the task instance corresponding to
     *                           the requested scheduling.
     * @param[in] minuendRule Rule for trigger comparison.
     * @param[in] subtrahendRule Rule for trigger comparison.
     * @param[in] referenceRule Rule for trigger comparison.
     * @param[in] minuendInitialization Comparison parameter initialization.
     * @param[in] subtrahendInitialization Comparison parameter initialization.
     * @param[in] referenceInitialization Comparison parameter initialization.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleTriggerTask(uint16_t taskId, 
                                        uint8_t *taskInstance,
                                        TaskTriggerComparisonRule_t triggerRule,
                                        TaskTriggerStorageRule_t minuendRule,
                                        TaskTriggerStorageRule_t subtrahendRule,
                                        TaskTriggerStorageRule_t referenceRule,
                                        void *minuendInitialization,
                                        void *subtrahendInitialization,
                                        void *referenceInitialization);

    /**
     * @brief Cancel the scheduling of a task.
     *
     * @param[in] taskId        ID of the task whose instance will be cancelled.
     * @param[in] taskInstance  Number of the task instance we want to cancel.
     */
    command void cancelTask(uint16_t taskId, uint8_t taskInstance);

    /**
     * @brief Signal that a task instance is ready to be executed.
     *
     * @param[in] taskId        ID of the task which is ready.
     * @param[in] taskInstance  Number of the task instance which is ready.
     * @param[in] taskType      Type of the task which is ready.
     */
    event   void taskReadyToRun(uint16_t taskId, 
                                uint8_t taskInstance, 
                                TaskType_t taskType);

    /**
     * @brief Execute a task instance.
     *
     * @param[in] taskId        ID of the task to be executed.
     * @param[in] taskInstance  Number of the task instance which will be run.
     */
    command void runTask(uint16_t taskId, uint8_t taskInstance);

    /**
     * @brief Discard data of a task instance.
     *
     * @param[in] taskId       ID of the task whose data will be discarded.
     * @param[in] taskInstance Number of the task instance to discard data.
     */
    command void discardTaskData(uint16_t taskId, uint8_t taskInstance);
}
