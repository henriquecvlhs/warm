/**
 * @file TaskArrayParameterAccess.nc
 * @brief File describing the Task Array Parameter Access interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface that abstracts the access to data stored in an array of task
 *        input parameters.
 */
interface TaskArrayParameterAccess<parameter_t> {
    /**
     * @brief Set position value of the array parameter for a task instance.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] position Array position to be set.
     * @param[in] value Value to be assigned to specified array position.
     */
    command void set(uint8_t taskInstance, uint8_t position, parameter_t value);

    /**
     * @brief Get value from array parameter position of a task instance.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] position Array position from which data is to be retrieved.
     *
     * @return Value from array parameter position.
     */
    command parameter_t get(uint8_t taskInstance, uint8_t position);

    /**
     * @brief Get array parameter size for a task instance.
     *
     * @param[in] taskInstance Task instance number.
     *
     * @return Array parameter size.
     */
    command uint8_t getInstanceSize(uint8_t taskInstance);
}
