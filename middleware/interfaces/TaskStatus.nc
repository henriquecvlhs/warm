/**
 * @file TaskStatus.nc
 * @brief File describing the Task Status interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Interface for inquiring a task instance status through the Task API
 *        component.
 */
interface TaskStatus {
    /**
     * @brief Get the number of times a certain task instance has been executed.
     *
     * @param[in] taskId       ID of the task whose status we want to know.
     * @param[in] taskInstance Number of the task instance whose status we want
     *                         to know.
     *
     * @return Number of times the task instance has been executed.
     */
    command uint16_t getExecutionStatus(uint16_t taskId, uint8_t taskInstance);

    /**
     * @brief Get scheduling status of a certain task instance.
     *
     * @param[in] taskId       ID of the task whose status we want to know.
     * @param[in] taskInstance Number of the task instance whose status we want
     *                         to know.
     *
     * @return Task instance scheduling status.
     * @retval 0 Task instance is not scheduled.
     * @retval 1 Task instance is scheduled.
     */
    command uint8_t getScheduleStatus(uint16_t taskId, uint8_t taskInstance);
}
