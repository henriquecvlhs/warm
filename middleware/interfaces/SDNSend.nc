/**
 * @file SDNSend.nc
 * @brief File describing the SDN Send interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface that abstracts the sending of data from the network
 *        through an SDN abstraction layer component.
 */
interface SDNSend {
    /**
     * @brief Send message to Software Defined Network.
     *
     * @param[in] flowId ID of the SDN flow which will carry the message.
     * @param[in] payload Payload to be sent.
     * @param[in] length Payload length.
     */
    command void send(uint16_t flowId, void *payload, uint8_t length);
}
