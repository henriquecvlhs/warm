/**
 * @file ProtocolScheduler.nc
 * @brief File describing the Protocol Scheduler interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface providing task scheduling commands from the Scheduler
 *        component to the Protocol Processor component.
 */
interface ProtocolScheduler {
    /**
     * @brief Schedule a periodic task.
     *
     * @param[in] taskId ID of the task to be scheduled.
     * @param[out] taskInstance Task instance allocated for scheduling.
     * @param[in] triggerPolicy Task triggering policy for this scheduling.
     * @param[in] executionPeriod Periodicity with which to run the task.
     * @param[in] periodScale Execution period is expressed in milliseconds
     *                        (0) or in microseconds (1).
     * @param[in] outputDataType Task output data type.
     * @param[in] outputDataLength Task output data length.
     * @param[in] outputFlowId Network flow ID to label task output.
     * @param[out] errorCode Scheduling operation error code.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t schedulePeriodicTask(uint16_t taskId,
                                         uint8_t *taskInstance,
                                         uint8_t triggerPolicy,
                                         uint32_t executionPeriod,
                                         uint8_t periodScale,
                                         uint8_t outputDataType,
                                         uint8_t outputDataLength,
                                         uint16_t outputFlowId,
                                         uint8_t *errorCode);

    /**
     * @brief Schedule an instantaneous task.
     *
     * @param[in] taskId ID of the task to be scheduled.
     * @param[in] taskInstance Task instance allocated for scheduling.
     * @param[in] triggerPolicy Task triggering policy for this scheduling.
     * @param[in] discardPeriod Periodicity with which unused task data is
     *                          to be discarded.
     * @param[in] periodScale Discard period is expressed in milliseconds
     *                        (0) or in microseconds (1).
     * @param[in] outputDataType Task output data type.
     * @param[in] outputDataLength Task output data length.
     * @param[in] outputFlowId Network flow ID to label task output.
     * @param[in] inputNumber Number of task input parameters.
     * @param[in] inputFlowIds Pointer to array of network flow IDs which
     *                         will bring the task's input parameters.
     * @param[in] inputSizes Pointer to array of the sizes which are to be
     *                       set for each of the input parameters.
     * @param[in] argumentString Pointer to string with additional settings
     *                           for the scheduled task instance.
     * @param[out] errorCode Scheduling operation error code.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleInstantaneousTask(uint16_t taskId,
                                              uint8_t *taskInstance,
                                              uint8_t triggerPolicy,
                                              uint32_t discardPeriod,
                                              uint8_t discardPeriodScale,
                                              uint8_t outputDataType,
                                              uint8_t outputDataLength,
                                              uint16_t outputFlowId,
                                              uint8_t inputNumber,
                                              uint16_t *inputFlowIds,
                                              uint8_t *inputSizes,
                                              char *argumentString,
                                              uint8_t *errorCode);

    /**
     * @brief Schedule a trigger task.
     *
     * @param[in] taskId ID of the task to be scheduled.
     * @param[out] taskInstance Task instance allocated for scheduling.
     * @param[in] outputFlowId Network flow ID to label task output.
     * @param[in] minuendRule Rule for trigger comparison.
     * @param[in] subtrahendRule Rule for trigger comparison.
     * @param[in] referenceRule Rule for trigger comparison.
     * @param[in] minuendInitialization Comparison parameter initialization.
     * @param[in] subtrahendInitialization Comparison parameter initialization.
     * @param[in] referenceInitialization Comparison parameter initialization.
     * @param[out] errorCode Scheduling operation error code.
     *
     * @return Result of scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleTriggerTask(uint16_t taskId,
                                        uint8_t *taskInstance,
                                        uint16_t outputFlowId,
                                        uint8_t minuendRule,
                                        uint8_t subtrahendRule,
                                        uint8_t comparisonRule,
                                        uint8_t referenceRule,
                                        void *minuendInitialization,
                                        void *subtrahendInitialization,
                                        void *referenceInitialization,
                                        uint8_t *errorCode);

    /**
     * @brief Cancel a scheduled task instance.
     *
     * @param[in] taskId ID of the task instance to be cancelled.
     * @param[out] taskInstance Task instance to be cancelled.
     */
    command void cancelSchedule(uint16_t taskId,
                                uint8_t taskInstance);
}
