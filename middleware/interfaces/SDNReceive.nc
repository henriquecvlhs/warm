/**
 * @file SDNReceive.nc
 * @brief File describing the SDN Receive interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface that abstracts the receiving of data from the network
 *        through an SDN abstraction layer component.
 */
interface SDNReceive {
    /**
     * @brief Receive message from Software Defined Network.
     *
     * @param[in] originId ID of the node which sent the message.
     * @param[in] flowId ID of the SDN flow which brought the message.
     * @param[in] payload The received payload.
     * @param[in] length Received payload length.
     */
    event void receive(uint16_t originId, 
                       uint16_t flowId, 
                       void *payload, 
                       uint8_t length);
}
