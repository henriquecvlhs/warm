/**
 * @file ProtocolOutput.nc
 * @brief File describing the Protocol Output interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface for signalizing the output of protocol packages from the
 *        Protocol Processor component.
 */
interface ProtocolOutput {
    /**
     * @brief Signalize a protocol response is ready.
     *
     * @param[in] destinationFlowId ID of the flow which will take the
     *                              protocol response to its destination.
     * @param[in] protocolResponse  Pointer to the payload containing the
     *                              protocol response.
     * @param[in] responseLength    Length of protocol response package.
     */
    event void ready(uint16_t destinationFlowId, 
                     uint8_t *protocolResponse, 
                     uint8_t responseLength);
}
