/**
 * @file Task.nc
 * @brief File describing the Task interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Interface that abstracts the behaviour of a task to the Task API 
 *        component.
 */
interface Task {
    /**
     * @biref Execute main task routine for a task instance.
     *
     * @param[in] taskInstance Task instance number.
     */
    command void run(uint8_t taskInstance);

    /**
     * @brief Initialize task during system initialization.
     */
    command void init(void);

    /**
     * @brief Receive task input data for a given task instance.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] parameterId ID of the input parameter which the received
     *                        data represents.
     * @param[in] value Pointer to the received data itself.
     */
    command void receiveData(uint8_t taskInstance, 
                             uint8_t parameterId, 
                             void *value);

    /**
     * @brief Discard all parameter data stored for a given task instance.
     *
     * @note Useful when the data validity period for a task instance has
     *       expired, so all its received input data must be discarded.
     *
     * @param[in] taskInstance Task instance number.
     */
    command void discardData(uint8_t taskInstance);

    /**
     * @brief Schedule task instance.
     *
     * @param[out] taskInstance Pointer to the instance number which will
     *                          be allocated to the scheduled task instance.
     * @param[in] triggerPolicy Descriptor for the trigger policy which is
     *                          to be applied to the scheduled task instance.
     *
     * @return Operation status.
     * @retval 0 Successful operation.
     * @retval 1 Unsuccessful operation.
     */
    command uint8_t schedule(uint8_t *taskInstance, 
                             TaskTriggerPolicy_t triggerPolicy);

    /**
     * @brief Configure the maximum amount of input data a certain task input
     *        parameter may receive before the task is considered ready to run.
     *
     * @note This should only be used to configure instantaneous task instances.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] parameterId ID of input parameter to be configured.
     * @param[in] inputMaxSize Maximum amount of input data to be set.
     */
    command void configureMaxInput(uint8_t taskInstance, 
                                   uint8_t parameterId, 
                                   uint8_t inputMaxSize); 

    /**
     * @brief Configure an instantaneous task instance by passing an argument
     *        string carrying possible additional settings.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] argumentString Pointer to the argument string.
     *
     * @note This should only be used to configure instantaneous task instances.
     */
    command void configureArgumentString(uint8_t taskInstance, char *argumentString);

    /**
     * @brief Cancel a scheduled task instance.
     *
     * @param[in] taskInstance Task instance number.
     */
    command void cancel(uint8_t taskInstance);

    /**
     * @brief Trigger a given task instance.
     *
     * @param[in] taskInstance Task instance number.
     */
    command void trigger(uint8_t taskInstance);

    /**
     * @brief Signalize that the output data of a given task instance is ready.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] value Data output value.
     */
    event   void outputReady(uint8_t taskInstance, void* value);

    /**
     * @brief Signalize that a task instance is ready to run.
     *
     * Event that signalizes that a given task instance is ready to be
     * executed, either because it has all the input data it needs or because
     * it has received the appropriate trigger signal.
     *
     * @param[in] taskInstance Task instance number.
     * @param[in] taskType Descriptor of the task type (e.g. a periodic task).
     */
    event   void readyToRun(uint8_t taskInstance, TaskType_t taskType);

    /**
     * @brief Get description of a given task.
     *
     * @return Pointer to a Task Description Response package filled with 
     *         task description data.
     */
    command void *getDescription(void);

    /**
     * @brief Get the execution status of a given task instance.
     *
     * @param[in] taskInstance Task instance number.
     *
     * @return The number of times this task instance has been executed.
     */
    command uint16_t getExecutionStatus(uint8_t taskInstance);

    /**
     * @brief Check if instance is scheduled.
     *
     * @param[in] taskInstance Task instance number.
     *
     * @return Instance scheduling status.
     * @retval 0 Instance is currently scheduled.
     * @retval 1 Instance is not currently schedule.
     */
    command uint8_t isScheduled(uint8_t taskInstance);
}
