/**
 * @file EmitterScheduler.nc
 * @brief File describing the Emitter Scheduler interface.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Interface for scheduling the output, through the network, of task 
 *        instance data with the Emitter component.
 *        
 */
interface EmitterScheduler {
    /**
     * @brief Schedule the output of data for a task instance.
     *
     * @param[in] taskId ID of the task which will output data.
     * @param[in] taskInstance Task instance which will output data.
     * @param[in] triggerOutput Boolean stating if output is a trigger signal.
     * @param[in] outputDataType Output data type.
     * @param[in] outputDataLength Output data length.
     * @param[in] outputFlowId ID of the network flow which will carry
     *                         output data through the network.
     *
     * @return Result of the scheduling operation.
     * @retval 0 Scheduling operation was successful.
     * @retval 1 Scheduling operation was unsuccessful.
     */
    command uint8_t scheduleTaskOutput(uint16_t taskId,
                                       uint8_t taskInstance,
                                       uint8_t triggerOutput,
                                       uint8_t outputDataType,
                                       uint8_t outputDataLength,
                                       uint16_t outputFlowId);

    /**
     * @brief Cancel the output of data scheduled for a task instance.
     *
     * @param[in] taskId ID of the task to be cancelled.
     * @param[in] taskInstance Task instance to be cancelled.
     */
    command void cancelTaskOutput(uint16_t taskId,
                                  uint8_t taskInstance);
}
