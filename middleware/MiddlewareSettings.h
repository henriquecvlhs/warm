/**
 * @file MiddlewareSettings.h
 * @brief This file holds main configurations for WARM's Middleware.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Default SDN Flow ID to take packages to the framework controller */
#define DEFAULT_CONTROLLER_SDN_FLOW_ID 0x0003

/** General node information */
#define NODE_ID                  TOS_NODE_ID
#define NODE_MOBILITY            NODE_MOBILITY_MOBILE
#define NODE_ENERGY_SOURCE       NODE_ENERGY_SOURCE_LIMITED
#define NODE_DEVICE_ID           DEVICE_ID_TELOSB
#define NODE_OS                  OS_ID_TINYOS

#define NODE_LOCATION_LATITUDE   0xABABABAB
#define NODE_LOCATION_LONGITUDE  0xACACACAC
#define NODE_LOCATION_HEIGHT     0xADADADAD

/** Scheduler configuration (maximum value: 255) */
#define MAX_SCHEDULED_PERIODIC_TASKS 20

/** Receiver configuration (maximum value: 255) */
#define MAX_SCHEDULED_NETWORK_INPUTS 20

/** Emitter configuration (maximum value: 255) */
#define MAX_SCHEDULED_NETWORK_OUTPUTS 20
