/**
 * @file TestHelperTaskExampleC.nc
 * @brief File implementing configuration for Helper Task Example 
 *        component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define TEST_TASK_INSTANCE_NUMBER 3

#define INPUT_NUMBER 2

#define SINGLE_INPUT_ID 0
#define SINGLE_TYPE uint8_t

#define ARRAY_INPUT_ID  1
#define ARRAY_MAX_SIZE 10
#define ARRAY_TYPE int32_t

#define OUTPUT_TYPE int32_t

/**
 * @brief Configuration for Helper Task Example component's test unit.
 */
configuration TestHelperTaskExampleC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestHelperTaskExampleP;
    components HelperTaskExampleC as testTask;

    TestHelperTaskExampleP.Boot -> MainC.Boot;

    TestHelperTaskExampleP.Task -> testTask.Task;
}

