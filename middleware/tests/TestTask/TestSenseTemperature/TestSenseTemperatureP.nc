/**
 * @file TestSenseTemperatureP.nc
 * @brief File implementing module of Sense Temperature task component's 
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Sense Temperature task component's 
 *        test unit.
 */
module TestSenseTemperatureP @safe() {
    uses {
        interface Boot;
        interface Task;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    uint8_t i, j;
    uint8_t result;
    uint8_t instance;
    uint8_t statistics;
    uint8_t instanceReady[TEST_TASK_INSTANCE_NUMBER];
    void *voidDescription;
    TaskDescriptionResponse_t description;

    event void Boot.booted() {
        /** Test init */
        call Task.init();
        printf("Testing Task \"Sense Temperature\"\n");

        /** Initialize instanceReady boolean array */
        for (i = 0; i < TEST_TASK_INSTANCE_NUMBER; i++) {
            instanceReady[i] = 0;
        }
        
        /** Schedule and configure instances so we can test it */
        printf("\n1) Scheduling task instances.\n");
        for (i = 0; i < TEST_TASK_INSTANCE_NUMBER; i++) {
            result = call Task.schedule(&instance, NO_TRIGGER);
       
            if ((instance == i) && (!result)) {
                printf("    Successfully scheduled task instance %d.\n", i);   
            }
            else {
                printf("    Failed to schedule task instance %d.\n", i); 
            }
        }

        /** Test execution */
        printf("\n4) Testing task execution (This test is non blocking).\n");
        if (instanceReady[0]) {
            printf("    Running task instance %d.\n", 0);
            call Task.run(0);
        }
    }

    void finishTest(void) {

        /** Test statistics */
        printf("\n5) Printing task execution status.\n");
        for (i = 0; i < TEST_TASK_INSTANCE_NUMBER; i++) {
            result = call Task.getExecutionStatus(i);
            printf("    Task instance %d has been executed %d times.\n", i, result);

            if (result == 1) {
                printf("        Task instance test has been successful so far!\n");
            }
            else {
                printf("        Task instance test has not been successful.\n");
            }
        }

        /** Test scheduling cancellation */
        printf("\n6) Testing scheduling cancellation\n");
        for (i = 0; i < TEST_TASK_INSTANCE_NUMBER; i++) {
            call Task.cancel(i);
            if (call Task.isScheduled(i)) {
                printf("    Failed to cancel scheduling of instance %d.\n", i);
            }
            else {
                printf("    Successfully canceled scheduling of instance %d.\n", i);
            }
        }

        /** Test description */
        voidDescription = call Task.getDescription();
        description = *((TaskDescriptionResponse_t *) voidDescription);
        printf("\n7) Printing Task description:\n");
        printf("    PID = %d\n", description.pid);
        printf("    TID = %d\n", description.tid);
        printf("    TTI = %d\n", description.tti);
        printf("    MSQ = %d\n", description.msq);
        printf("    ODF = 0x%x\n", description.odf);
        printf("    IPN = %d\n", description.ipn);
        printf("    D   = %d\n", description.d);
        printf("    A   = %d\n", description.a);
        printf("    G   = %d\n", description.g);
        printf("    S   = %d\n", description.s);
        for (i = 0; i < INPUT_NUMBER; i++) {
            printf("    IPARAM[%d] : IPQ = %d\n", i, description.iparam[i].ipq);
            printf("    IPARAM[%d] : IPF = 0x%x\n", i, description.iparam[i].ipf);
        }
    }

    event void Task.readyToRun(uint8_t taskInstance, TaskType_t taskType) {
        /** Do nothing */
        printf("    ===> Event: Instance %d of Task \"Sense Temperature\" is ready to run!\n", taskInstance);
        instanceReady[taskInstance] = 1;
        return;
    }

    event void Task.outputReady(uint8_t taskInstance, void *value) {
        /** Do notheing */
        printf("    ===> Event: Instance %d of Task \"Sense Temperature\" has produced data: %d\n", taskInstance, *((OUTPUT_TYPE *) value));

        taskInstance++;
        if (taskInstance < TEST_TASK_INSTANCE_NUMBER) {
            if (instanceReady[taskInstance]) {
                printf("    Running task instance %d.\n", taskInstance);
                call Task.run(taskInstance);
            }
        }
        else {
            finishTest();
        }

        return;
    }
}

