/**
 * @file TestProtocolProcessorC.nc
 * @brief File implementing configuration for the Protocol Processor 
 *        component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define TEST_CONTROLLER_FLOW_ID 0x0CFD

#define TEST_SCHEDULER_STATUS 10
#define TEST_RECEIVER_STATUS   5
#define TEST_EMITTER_STATUS    8

/**
 * @brief Configuration for Scheduler component's test unit.
 *
 * @note This test assumes the maximum number of scheduled periodic tasks is
 *       set to 20.
 */
configuration TestProtocolProcessorC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestProtocolProcessorP;
    components ProtocolProcessorC;

    TestProtocolProcessorP.Boot -> MainC.Boot;

    ProtocolProcessorC.ProtocolScheduler -> TestProtocolProcessorP.dummyScheduler;
    ProtocolProcessorC.SchedulerStatus -> TestProtocolProcessorP.dummySchedulerStatus;

    ProtocolProcessorC.TaskDescription -> TestProtocolProcessorP.dummyTaskDescription;
    ProtocolProcessorC.TaskStatus -> TestProtocolProcessorP.dummyTaskStatus;

    ProtocolProcessorC.ReceiverStatus -> TestProtocolProcessorP.dummyReceiver;
    ProtocolProcessorC.EmitterStatus -> TestProtocolProcessorP.dummyEmitter;

    TestProtocolProcessorP.ProtocolInput -> ProtocolProcessorC.ProtocolInput;
    TestProtocolProcessorP.ProtocolOutput -> ProtocolProcessorC.ProtocolOutput;
}

