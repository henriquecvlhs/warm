/**
 * @file TestProtocolProcessorP.nc
 * @brief File implementing module of Protocol Processor component's 
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "protocol.h"

/**
 * @brief Module that implements Protocol Processor component's test unit.
 */
module TestProtocolProcessorP @safe() {
    provides {
        interface ProtocolScheduler as dummyScheduler;
        interface SchedulerStatus as dummySchedulerStatus;

        interface TaskDescription as dummyTaskDescription;
        interface TaskStatus as dummyTaskStatus;

        interface ReceiverStatus as dummyReceiver;
        interface EmitterStatus as dummyEmitter;
    }

    uses {
        interface ProtocolInput;
        interface ProtocolOutput;

        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    uint8_t result;
    uint8_t i, j;
    uint8_t started = 0;

    NodeAssociateConfirmation_t nodeAssociate;

    TaskDescriptionRequest_t descriptionRequest;
    TaskDescriptionResponse_t descriptionResponse; 

    PeriodicTaskSchedulingRequest_t periodicScheduling;
    InstantaneousTaskSchedulingRequest_t instantaneousScheduling;
    TriggerTaskSchedulingRequest_t triggerScheduling;

    TaskStatisticsRequest_t taskStatisticsRequest;
    NodeStatisticsRequest_t nodeStatisticsRequest;

    ScheduledTaskCancellationRequest_t cancellationRequest;

    uint16_t loadedTaskIds[] = { 2482, 2975, 7837, 2766, 4593, 5638 };

    void printNodeCharacteristicsDescription(void *package) {
        printf("\nPrinting Node Characteristics Description Package.");
        printf("\nPID: %d", ((NodeCharacteristicsDescription_t *) package)->pid);
        printf("\nNID: %d", ((NodeCharacteristicsDescription_t *) package)->nid);
        printf("\nM  : %d", ((NodeCharacteristicsDescription_t *) package)->m);
        printf("\nE  : %d", ((NodeCharacteristicsDescription_t *) package)->e);
        printf("\nDID: %d", ((NodeCharacteristicsDescription_t *) package)->did);
        printf("\nOS : %d", ((NodeCharacteristicsDescription_t *) package)->os);
        printf("\nLat: %lu", ((NodeCharacteristicsDescription_t *) package)->latitude);
        printf("\nLon: %lu", ((NodeCharacteristicsDescription_t *) package)->longitude);
        printf("\nHei: %lu", ((NodeCharacteristicsDescription_t *) package)->height);
        printf("\nPTQ: %d", ((NodeCharacteristicsDescription_t *) package)->ptq);
        printf("\nIDQ: %d", ((NodeCharacteristicsDescription_t *) package)->idq);
        printf("\nNTQ: %d", ((NodeCharacteristicsDescription_t *) package)->ntq);
        printf("\nNTN: %d", ((NodeCharacteristicsDescription_t *) package)->ntn);

        for (i = 0; i < ((NodeCharacteristicsDescription_t *) package)->ntn; i++) {
            printf("\nNode loaded task %d - Task ID: %d", i, ((NodeCharacteristicsDescription_t *) package)->tid[i]);
        }

        printf("\n");
    }

    void printNodeAssociateConfirmation(void *package) {
        printf("\nPrinting Node Associate Confirmation Package.");
        printf("\nPID: %d", ((NodeAssociateConfirmation_t *) package)->pid);
        printf("\nCFI: %u", ((NodeAssociateConfirmation_t *) package)->cfi);

        printf("\n");
    }

    void printTaskDescriptionRequest(void *package) {
        printf("\nPrinting Task Description Request Package.");
        printf("\nPID: %d", ((TaskDescriptionRequest_t *) package)->pid);
        printf("\nTID: %u", ((TaskDescriptionRequest_t *) package)->tid);

        printf("\n");
    }

    void printTaskDescriptionResponse(void *package) {
        printf("\nPrinting Task Description Response Package.");
        printf("\nPID: %d", ((TaskDescriptionResponse_t *) package)->pid);
        printf("\nTID: %u", ((TaskDescriptionResponse_t *) package)->tid);
        printf("\nTTI: %d", ((TaskDescriptionResponse_t *) package)->tti);
        printf("\nMSQ: %d", ((TaskDescriptionResponse_t *) package)->msq);
        printf("\nODF: %d", ((TaskDescriptionResponse_t *) package)->odf);
        printf("\nIPN: %d", ((TaskDescriptionResponse_t *) package)->ipn);
        printf("\nD  : %d", ((TaskDescriptionResponse_t *) package)->d);
        printf("\nA  : %d", ((TaskDescriptionResponse_t *) package)->a);
        printf("\nG  : %d", ((TaskDescriptionResponse_t *) package)->g);
        printf("\nS  : %d", ((TaskDescriptionResponse_t *) package)->s);

        for (i = 0; i < ((TaskDescriptionResponse_t *) package)->ipn; i++) {
            printf("\nInput Parameter %d - IPQ: %d", i, ((TaskDescriptionResponse_t *) package)->iparam[i].ipq);
            printf("\nInput Parameter %d - IPF: %d", i, ((TaskDescriptionResponse_t *) package)->iparam[i].ipf);
        }

        printf("\n");
    }

    void printPeriodicTaskSchedulingRequest(void *package) {
        printf("\nPrinting Periodic Task Scheduling Request Package.");
        printf("\nPID: %d", ((PeriodicTaskSchedulingRequest_t *) package)->pid);
        printf("\nTID: %u", ((PeriodicTaskSchedulingRequest_t *) package)->tid);
        printf("\nTA : %d", ((PeriodicTaskSchedulingRequest_t *) package)->ta);
        printf("\nODT: %d", ((PeriodicTaskSchedulingRequest_t *) package)->odt);
        printf("\nODL: %d", ((PeriodicTaskSchedulingRequest_t *) package)->odl);
        printf("\nP  : %d", ((PeriodicTaskSchedulingRequest_t *) package)->p);
        printf("\nTP : %lu", ((PeriodicTaskSchedulingRequest_t *) package)->tp);
        printf("\nOFI: 0x%4.4X", ((PeriodicTaskSchedulingRequest_t *) package)->ofi);

        printf("\n");
    }

    void printInstantaneousTaskSchedulingRequest(void *package) {
        printf("\nPrinting Instantaneous Task Scheduling Request Package.");
        printf("\nPID: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->pid);
        printf("\nTID: %u", ((InstantaneousTaskSchedulingRequest_t *) package)->tid);
        printf("\nIPN: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->ipn);
        printf("\nTA : %d", ((InstantaneousTaskSchedulingRequest_t *) package)->ta);
        printf("\nODT: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->odt);
        printf("\nODL: %d", ((InstantaneousTaskSchedulingRequest_t *) package)->odl);
        printf("\nP  : %d", ((InstantaneousTaskSchedulingRequest_t *) package)->p);
        printf("\nPTW: %lu", ((InstantaneousTaskSchedulingRequest_t *) package)->ptw);
        printf("\nOFI: 0x%4.4X", ((InstantaneousTaskSchedulingRequest_t *) package)->ofi);

        for (i = 0; i < ((InstantaneousTaskSchedulingRequest_t *) package)->ipn; i++) {
            printf("\nInput Parameter %d - IFI: 0x%4.4X", i, ((InstantaneousTaskSchedulingRequest_t *) package)->input[i].ifi);
            printf("\nInput Parameter %d - PN : %d", i, ((InstantaneousTaskSchedulingRequest_t *) package)->input[i].pn);
        }

        printf("\nArgs: %s", (char *) ((InstantaneousTaskSchedulingRequest_t *) package)->args);

        printf("\n");
    }

    void printTriggerTaskSchedulingRequest(void *package) {
        printf("\nPrinting Trigger Task Scheduling Request Package.");
        printf("\nPID: %d", ((TriggerTaskSchedulingRequest_t *) package)->pid);
        printf("\nTID: %u", ((TriggerTaskSchedulingRequest_t *) package)->tid);
        printf("\nTMC: %d", ((TriggerTaskSchedulingRequest_t *) package)->tmc);
        printf("\nTSC: %d", ((TriggerTaskSchedulingRequest_t *) package)->tsc);
        printf("\nTCS: %d", ((TriggerTaskSchedulingRequest_t *) package)->tcs);
        printf("\nTRC: %d", ((TriggerTaskSchedulingRequest_t *) package)->trc);
        printf("\nTMI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tmi);
        printf("\nTSI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tsi);
        printf("\nTRI: %llu", ((TriggerTaskSchedulingRequest_t *) package)->tri);
        printf("\nOFI: 0x%4.4X", ((TriggerTaskSchedulingRequest_t *) package)->ofi);

        printf("\n");
    }

    void printTaskSchedulingConfirmation(void *package) {
        printf("\nPrinting Task Scheduling Confirmation Package.");
        printf("\nPID: %d", ((TaskSchedulingConfirmation_t *) package)->pid);
        printf("\nTID: %u", ((TaskSchedulingConfirmation_t *) package)->tid);
        printf("\nTIN: %d", ((TaskSchedulingConfirmation_t *) package)->tin);
        printf("\nError: 0x%2.2X", ((TaskSchedulingConfirmation_t *) package)->error.code);

        printf("\n");
    }

    void printTaskStatisticsRequest(void *package) {
        printf("\nPrinting Task Statistics Request Package.");
        printf("\nPID: %d", ((TaskStatisticsRequest_t *) package)->pid);
        printf("\nTID: %u", ((TaskStatisticsRequest_t *) package)->tid);
        printf("\nTIN: %d", ((TaskStatisticsRequest_t *) package)->tin);

        printf("\n");
    }

    void printTaskStatisticsResponse(void *package) {
        printf("\nPrinting Task Statistics Request Package.");
        printf("\nPID: %d", ((TaskStatisticsResponse_t *) package)->pid);
        printf("\nTID: %u", ((TaskStatisticsResponse_t *) package)->tid);
        printf("\nTIN: %d", ((TaskStatisticsResponse_t *) package)->tin);
        printf("\nTEN: %u", ((TaskStatisticsResponse_t *) package)->ten);

        printf("\n");
    }

    void printNodeStatisticsRequest(void *package) {
        printf("\nPrinting Node Statistics Request Package.");
        printf("\nPID: %d", ((NodeStatisticsRequest_t *) package)->pid);

        printf("\n");
    }

    void printNodeStatisticsResponse(void *package) {
        printf("\nPrinting Node Statistics Response Package.");
        printf("\nPID: %d", ((NodeStatisticsResponse_t *) package)->pid);
        printf("\nCPQ: %d", ((NodeStatisticsResponse_t *) package)->cpq);
        printf("\nCIQ: %d", ((NodeStatisticsResponse_t *) package)->ciq);
        printf("\nCNQ: %d", ((NodeStatisticsResponse_t *) package)->cnq);
        printf("\nCBL: %d", ((NodeStatisticsResponse_t *) package)->cbl);
        printf("\nCRP: %d", ((NodeStatisticsResponse_t *) package)->crp);
        printf("\nLat: %lu", ((NodeStatisticsResponse_t *) package)->latitude);
        printf("\nLon: %lu", ((NodeStatisticsResponse_t *) package)->longitude);
        printf("\nHei: %lu", ((NodeStatisticsResponse_t *) package)->height);

        for (i = 0; i < sizeof(loadedTaskIds)/2; i++) {
            printf("\nLoaded Task %d - CSQ: %d", i, ((NodeStatisticsResponse_t *) package)->csq[i]);
        }

        printf("\n");
    }

    void printScheduledTaskCancellationRequest(void *package) {
        printf("\nPrinting Scheduled Task Cancellation Request Package.");
        printf("\nPID: %d", ((ScheduledTaskCancellationRequest_t *) package)->pid);
        printf("\nTID: %u", ((ScheduledTaskCancellationRequest_t *) package)->tid);
        printf("\nTIN: %d", ((ScheduledTaskCancellationRequest_t *) package)->tin);

        printf("\n");
    }

    void printScheduledTaskCancellationResponse(void *package) {
        printf("\nPrinting Scheduled Task Cancellation Request Package.");
        printf("\nPID: %d", ((ScheduledTaskCancellationResponse_t *) package)->pid);
        printf("\nTID: %u", ((ScheduledTaskCancellationResponse_t *) package)->tid);
        printf("\nTIN: %d", ((ScheduledTaskCancellationResponse_t *) package)->tin);
        printf("\nS  : %d", ((ScheduledTaskCancellationResponse_t *) package)->s);

        printf("\n");
    }

    event void Boot.booted() {
        /** Test init. Should be called elsewhere */
        if (!started) {
            printf("Testing Protocol Processor");
            printf("\n\n1) Test node initialization.\n");
            printf("\nExpect Node Characteristcs Description Package to be sent.\n");
            printf("\n    Failed node initialization");
            started = 1;
        }

        /** Test Node Associate Confirmation processing */
        printf("\n\n2) Test Node Associate Confirmation processing.\n");

        printf("\nPreparing package to be fed.\n");

        nodeAssociate.pid = NODE_ASSOCIATE_CONFIRMATION_PID;
        nodeAssociate.cfi = TEST_CONTROLLER_FLOW_ID;

        printNodeAssociateConfirmation((void *) &nodeAssociate);

        printf("\nHandling prepared package to Protocol Processor.");
        printf("\nExpect no response.");

        call ProtocolInput.data((void *) &nodeAssociate);

        /** Test Task Description Request processing */
        printf("\n\n3) Test Task Description Request processing.\n");

        printf("\nPreparing package to be fed.\n");

        descriptionRequest.pid = TASK_DESCRIPTION_REQUEST_PID;
        descriptionRequest.tid = loadedTaskIds[0];

        printTaskDescriptionRequest((void *) &descriptionRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &descriptionRequest);

        printf("\nPreparing package to be fed.\n");

        descriptionRequest.tid = loadedTaskIds[1];

        printTaskDescriptionRequest((void *) &descriptionRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &descriptionRequest);

        /** Test Task Scheduling Request processing */
        printf("\n\n4) Test Task Scheduling Request processing.\n");

        printf("\nPreparing package to be fed.\n");

        periodicScheduling.pid = PERIODIC_TASK_SCHEDULING_REQUEST_PID;
        periodicScheduling.tid = loadedTaskIds[0];
        periodicScheduling.ta  = 1;
        periodicScheduling.odt = 5;
        periodicScheduling.odl = 2;
        periodicScheduling.p   = 0;
        periodicScheduling.tp  = 1000;
        periodicScheduling.ofi = 0x0FD0;

        printPeriodicTaskSchedulingRequest((void *) &periodicScheduling);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &periodicScheduling);

        printf("\nPreparing package to be fed.\n");

        instantaneousScheduling.pid = INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID;
        instantaneousScheduling.tid = loadedTaskIds[1];
        instantaneousScheduling.ipn = loadedTaskIds[1] & 0x000F;
        instantaneousScheduling.ta  = 1;
        instantaneousScheduling.odt = loadedTaskIds[1] & 0x0007;
        instantaneousScheduling.odl = loadedTaskIds[1] & 0x0003;
        instantaneousScheduling.p   = 0;
        instantaneousScheduling.ptw = 3000;
        instantaneousScheduling.ofi = 0x0FD1;

        for (i = 0; i < instantaneousScheduling.ipn; i++) {
            instantaneousScheduling.input[i].ifi = 0x0DF0 + i;
            instantaneousScheduling.input[i].pn  = i + 1;
        }

        instantaneousScheduling.args[0] = (uint8_t) 'c';
        instantaneousScheduling.args[1] = (uint8_t) 'm';
        instantaneousScheduling.args[2] = (uint8_t) 'd';
        instantaneousScheduling.args[3] = (uint8_t) '1';
        instantaneousScheduling.args[4] = 0x00;

        printInstantaneousTaskSchedulingRequest((void *) &instantaneousScheduling);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &instantaneousScheduling);

        printf("\nPreparing package to be fed.\n");

        triggerScheduling.pid = TRIGGER_TASK_SCHEDULING_REQUEST_PID;
        triggerScheduling.tid = loadedTaskIds[2];
        triggerScheduling.tmc = 1;
        triggerScheduling.tsc = 2;
        triggerScheduling.tcs = 3;
        triggerScheduling.trc = 4;
        triggerScheduling.tmi = 0xDEADBEEFDEADBEEF;
        triggerScheduling.tsi = 0xDEADBEEFDEADBEEF;
        triggerScheduling.tri = 0xDEADBEEFDEADBEEF;
        triggerScheduling.ofi = 0x0FD2;

        printTriggerTaskSchedulingRequest((void *) &triggerScheduling);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &triggerScheduling);

        /** Test Task Statistics Request processing */
        printf("\n\n5) Test Task Statistics Request processing.\n");

        printf("\nPreparing package to be fed.\n");

        taskStatisticsRequest.pid = TASK_STATISTICS_REQUEST_PID;
        taskStatisticsRequest.tid = loadedTaskIds[0];
        taskStatisticsRequest.tin = 3;

        printTaskStatisticsRequest((void *) &taskStatisticsRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &taskStatisticsRequest);

        printf("\nPreparing package to be fed.\n");

        taskStatisticsRequest.tid = loadedTaskIds[1];
        taskStatisticsRequest.tin = 2;

        printTaskStatisticsRequest((void *) &taskStatisticsRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &taskStatisticsRequest);

        printf("\nPreparing package to be fed.\n");

        taskStatisticsRequest.tid = loadedTaskIds[2];
        taskStatisticsRequest.tin = 1;

        printTaskStatisticsRequest((void *) &taskStatisticsRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &taskStatisticsRequest);

        /** Test Node Statistics Request processing */
        printf("\n\n6) Test Node Statistics Request processing.\n");

        printf("\nPreparing package to be fed.\n");

        nodeStatisticsRequest.pid = NODE_STATISTICS_REQUEST_PID;

        printNodeStatisticsRequest((void *) &nodeStatisticsRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &nodeStatisticsRequest);

        printf("\n\n");

        /** Test Node Statistics Request processing */
        printf("\n\n7) Test Scheduled Task Cancellation Request processing.\n");

        printf("\nPreparing package to be fed.\n");

        cancellationRequest.pid = SCHEDULED_TASK_CANCELLATION_REQUEST_PID;
        cancellationRequest.tid = loadedTaskIds[0];
        cancellationRequest.tin = loadedTaskIds[0] & 0x000F;

        printScheduledTaskCancellationRequest((void *) &cancellationRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &cancellationRequest);

        printf("\nPreparing package to be fed.\n");

        cancellationRequest.tid = loadedTaskIds[1];
        cancellationRequest.tin = loadedTaskIds[1] & 0x000F;

        printScheduledTaskCancellationRequest((void *) &cancellationRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &cancellationRequest);

        printf("\nPreparing package to be fed.\n");

        cancellationRequest.tid = loadedTaskIds[2];
        cancellationRequest.tin = loadedTaskIds[2] & 0x000F;

        printScheduledTaskCancellationRequest((void *) &cancellationRequest);

        printf("\nHandling prepared package to Protocol Processor.\n");

        call ProtocolInput.data((void *) &cancellationRequest);

        printf("\nEnd of test.\n");
    }

    command uint8_t dummyScheduler.schedulePeriodicTask(
                                         uint16_t taskId,
                                         uint8_t *taskInstance,
                                         uint8_t triggerPolicy,
                                         uint32_t executionPeriod,
                                         uint8_t periodScale,
                                         uint8_t outputDataType,
                                         uint8_t outputDataLength,
                                         uint16_t outputFlowId,
                                         uint8_t *errorCode) {
        printf("\n====> Scheduled instance %d of periodic task %d", *taskInstance, taskId);
        *taskInstance = taskId & 0x000F;
        *errorCode = 0x80;
        return 0;
    }

    command uint8_t dummyScheduler.scheduleInstantaneousTask(
                                              uint16_t taskId,
                                              uint8_t *taskInstance,
                                              uint8_t triggerPolicy,
                                              uint32_t discardPeriod,
                                              uint8_t discardPeriodScale,
                                              uint8_t outputDataType,
                                              uint8_t outputDataLength,
                                              uint16_t outputFlowId,
                                              uint8_t inputNumber,
                                              uint16_t *inputFlowIds,
                                              uint8_t *inputSizes,
                                              char *argumentString,
                                              uint8_t *errorCode) {
        printf("\n====> Scheduled instance %d of instantaneous task %d", *taskInstance, taskId);
        *taskInstance = taskId & 0x000F;
        *errorCode = 0x80;
        return 0;
    }

    command uint8_t dummyScheduler.scheduleTriggerTask(
                                        uint16_t taskId,
                                        uint8_t *taskInstance,
                                        uint16_t outputFlowId,
                                        uint8_t minuendRule,
                                        uint8_t subtrahendRule,
                                        uint8_t comparisonRule,
                                        uint8_t referenceRule,
                                        void *minuendInitialization,
                                        void *subtrahendInitialization,
                                        void *referenceInitialization,
                                        uint8_t *errorCode) {
        printf("\n====> Scheduled instance %d of trigger task %d", *taskInstance, taskId);
        *taskInstance = taskId & 0x000F;
        *errorCode = 0x80;
        return 0;
    }

    command void dummyScheduler.cancelSchedule(uint16_t taskId,
                                               uint8_t taskInstance) {
        printf("\n====> Cancelled instance %d of task %d", taskInstance, taskId);
    }

    command uint8_t dummySchedulerStatus.getPeriodicSchedulesStatus(void) {
        return TEST_SCHEDULER_STATUS;
    }

    command void *dummyTaskDescription.getDescription(uint16_t taskId) {
        descriptionResponse.pid = TASK_DESCRIPTION_RESPONSE_PID;
        descriptionResponse.tid = taskId;
        descriptionResponse.tti = taskId & 0x0003;
        descriptionResponse.msq = taskId & 0x000F;
        descriptionResponse.odf = taskId & 0x03FF;
        descriptionResponse.ipn = taskId & 0x000F;
        descriptionResponse.d   = taskId & 0x0001;
        descriptionResponse.a   = taskId & 0x0001;
        descriptionResponse.g   = taskId & 0x0001;
        descriptionResponse.s   = taskId & 0x0001;
        
        for (i = 0; i < descriptionResponse.ipn; i++) {
            descriptionResponse.iparam[i].ipq = taskId & 0x003F;
            descriptionResponse.iparam[i].ipf = taskId & 0x03FF;
        }

        return ((void *) &descriptionResponse);
    }

    command uint16_t *dummyTaskDescription.getLoadedTasks(
                                         uint8_t *numberOfLoadedTasks) {
        *numberOfLoadedTasks = sizeof(loadedTaskIds)/2;
        return loadedTaskIds;
    }

    command uint16_t dummyTaskStatus.getExecutionStatus(uint16_t taskId, 
                                                        uint8_t taskInstance) {
        return ((taskInstance * taskId) & 0x00FF);
    }

    command uint8_t dummyTaskStatus.getScheduleStatus(uint16_t taskId, 
                                                      uint8_t taskInstance) {
        /** Only odd instances are scheduled */
        return (taskInstance & 0x01);
    }

    command uint8_t dummyReceiver.getInputSchedulesStatus(void) {
        return TEST_RECEIVER_STATUS;
    }

    command uint8_t dummyEmitter.getOutputSchedulesStatus(void) {
        return TEST_EMITTER_STATUS;
    }

    event void ProtocolOutput.ready(uint16_t destinationFlowId, 
                                    uint8_t *protocolResponse,
                                    uint8_t responseLength) {
        /** Print test initialization */
        if (!started) {
            printf("Testing Protocol Processor");
            printf("\n\n1) Test node initialization.\n");
            printf("\nExpect Node Characteristcs Description Package to be sent.\n");
            started = 1;
        }

        printf("\n====> Sending package of pid %d (length: %2d) through flow 0x%4.4X.", (*protocolResponse) >> 4, responseLength, destinationFlowId);

        switch ((*protocolResponse) >> 4) {
            case NODE_CHARACTERISTICS_DESCRIPTION_PID:
                printNodeCharacteristicsDescription((void *) protocolResponse);
                break;

            case TASK_DESCRIPTION_RESPONSE_PID:
                printTaskDescriptionResponse((void *) protocolResponse);
                break;

            case PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID:
            case INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID:
            case TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID:
                printTaskSchedulingConfirmation((void *) protocolResponse);
                break;

            case TASK_STATISTICS_RESPONSE_PID:
                printTaskStatisticsResponse((void *) protocolResponse);
                break;

            case NODE_STATISTICS_RESPONSE_PID:
                printNodeStatisticsResponse((void *) protocolResponse);
                break;

            case SCHEDULED_TASK_CANCELLATION_RESPONSE_PID:
                printScheduledTaskCancellationResponse((void *) protocolResponse);
                break;

            default:
                printf("\nOutput error.");
                break;
        }
    }
}

