/**
 * @file TestEmitterC.nc
 * @brief File implementing configuration for the Emitter component's 
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define CONTROLLER_FLOW_ID 0x00CF
#define CONTROLLER_ID 0x0001

#define TASK_FLOW_ID 0x00DF
#define SOURCE_NODE_ID 0x0123

#define MAX_NUMBER_OF_SCHEDULINGS 20
#define NUMBER_OF_OUTPUT_SCHEDULINGS 30

/**
 * @brief Configuration for Emitter component's test unit.
 *
 * @note Assuming 20 is the size of output scheduler table.
 */
configuration TestEmitterC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestEmitterP;
    components EmitterC;

    TestEmitterP.Boot -> MainC.Boot;

    EmitterC.TaskOutput -> TestEmitterP.dummyTaskAPI;
    EmitterC.ProtocolOutput -> TestEmitterP.dummyProtocolOut;
    EmitterC.SDNSend -> TestEmitterP.dummySDN;

    TestEmitterP.EmitterScheduler -> EmitterC.EmitterScheduler;
    TestEmitterP.EmitterStatus -> EmitterC.EmitterStatus;
}

