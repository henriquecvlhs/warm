/**
 * @file TestEmitterP.nc
 * @brief File implementing module of Emitter component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "protocol.h"

/**
 * @brief Module that implements Emitter component's test unit.
 */
module TestEmitterP @safe() {
    provides {
        interface TaskOutput as dummyTaskAPI;
        interface ProtocolOutput as dummyProtocolOut;
        interface SDNSend as dummySDN;
    }

    uses {
        interface EmitterScheduler;
        interface EmitterStatus;

        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    int16_t outputBuffer;
    uint8_t result;
    uint8_t totalSchedulings;
    uint8_t i;
    uint16_t testOutputIds[] =  { 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                                  71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                                  81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };

    uint16_t testOutputInstances[] = {  0,  0,  1,  1,  1,  1,  2,  2,  2, 2,
                                        2,  2,  0,  0,  0,  0,  0,  4,  4, 4,
                                        1,  1,  1,  1,  2,  5,  3,  4,  4, 1 };

    uint16_t testOutputFlows[] = {  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                                   11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                   21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };

    NodeCharacteristicsDescription_t nodeCharacteristics;
    TaskDescriptionResponse_t descriptionResponse;
    TaskSchedulingConfirmation_t schedulingResponse;
    ScheduledTaskCancellationResponse_t cancellationResponse;
    TaskStatisticsResponse_t taskStatistics;
    NodeStatisticsResponse_t nodeStatistics;

    DataTransmission_t *dataPackage;

    event void Boot.booted() {
        /** Test init. */
        printf("Testing Receiver");

        /** Test sending protocol response packages */
        printf("\n\n1) Test sending protocol response packages.\n");

        printf("\n    Feeding Node Characteristics Description package.");
        nodeCharacteristics.pid = NODE_CHARACTERISTICS_DESCRIPTION_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &nodeCharacteristics,
                                      sizeof(NodeCharacteristicsDescription_t));

        printf("\n    Feeding Task Description Response package.");
        descriptionResponse.pid = TASK_DESCRIPTION_RESPONSE_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &descriptionResponse,
                                      sizeof(TaskDescriptionResponse_t));

        printf("\n    Feeding Task Scheduling Confirmation packages.");
        schedulingResponse.pid = PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &schedulingResponse,
                                      sizeof(TaskSchedulingConfirmation_t));

        schedulingResponse.pid = INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &schedulingResponse,
                                      sizeof(TaskSchedulingConfirmation_t));

        schedulingResponse.pid = TRIGGER_TASK_SCHEDULING_CONFIRMATION_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &schedulingResponse,
                                      sizeof(TaskSchedulingConfirmation_t));

        printf("\n    Feeding Scheduled Task Cancellation Response package.");
        cancellationResponse.pid = SCHEDULED_TASK_CANCELLATION_RESPONSE_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &cancellationResponse,
                                      sizeof(ScheduledTaskCancellationResponse_t));

        printf("\n    Feeding Task Statistics Response package.");
        taskStatistics.pid = TASK_STATISTICS_RESPONSE_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &taskStatistics,
                                      sizeof(TaskStatisticsResponse_t));

        printf("\n    Feeding Node Statistics Response package.");
        nodeStatistics.pid = NODE_STATISTICS_RESPONSE_PID;
        signal dummyProtocolOut.ready(CONTROLLER_FLOW_ID, 
                                      (void *) &nodeStatistics,
                                      sizeof(NodeStatisticsResponse_t));

        /** Test input scheduling */
        printf("\n\n2) Test output scheduling.\n");

        for (i = 0; i < NUMBER_OF_OUTPUT_SCHEDULINGS; i++) {
            printf("\n    Trying to schedule output for instance %d of Task %u, going through flow %u.", testOutputInstances[i], testOutputIds[i], testOutputFlows[i]);

            result = call EmitterScheduler.scheduleTaskOutput(testOutputIds[i],
                                                              testOutputInstances[i],
                                                              0,
                                                              3,
                                                              1,
                                                              testOutputFlows[i]);

            if ((i < MAX_NUMBER_OF_SCHEDULINGS) && !result) {
                printf("\n    Success.");
            }
            else if ((i >= MAX_NUMBER_OF_SCHEDULINGS) && result) {
                printf("\n    Success.");
            }
            else {
                printf("\n    Failure.");
            }
        }

        /** Test Receiver status */
        printf("\n\n3) Test Emitter status.\n");

        totalSchedulings = call EmitterStatus.getOutputSchedulesStatus();

        printf("\n    Current number of schedules is %d: ", totalSchedulings);
        if (totalSchedulings == MAX_NUMBER_OF_SCHEDULINGS) {
            printf("success!");
        }
        else {
            printf("failure!");
        }

        /** Test Cancellation of inputs */
        printf("\n\n4) Test output of Data and Trigger Signal Transmission packages.\n");
        printf("\n\n    Expect only %d data packages being sent.", MAX_NUMBER_OF_SCHEDULINGS);

        for (i = 0; i < NUMBER_OF_OUTPUT_SCHEDULINGS; i++) {
            outputBuffer = 10 - i;
            signal dummyTaskAPI.ready(testOutputIds[i],
                                      testOutputInstances[i],
                                      (void *) &outputBuffer);
        }

        /** Test Cancellation of inputs */
        printf("\n\n5) Test cancellation of outputs.\n");

        for (i = 0; i < MAX_NUMBER_OF_SCHEDULINGS; i++) {
            printf("\n    Cancelling output schedule for instance %d of Task %u.", testOutputInstances[i], testOutputIds[i]);

            call EmitterScheduler.cancelTaskOutput(testOutputIds[i],
                                                   testOutputInstances[i]);
        }

        totalSchedulings = call EmitterStatus.getOutputSchedulesStatus();

        printf("\n    Current number of schedules is %d: ", totalSchedulings);
        if (totalSchedulings == 0) {
            printf("success!");
        }
        else {
            printf("failure!");
        }

        printf("\nEnd of test.\n");
    }
    
    command void dummySDN.send(uint16_t flowId, void *payload, uint8_t length) {
        uint8_t packagePid = *((uint8_t *) payload) >> 4;

        printf("\n====> Sending package of PID %d through flow ID %u.", packagePid, flowId);

        if (packagePid == DATA_TRANSMISSION_PID) {
            dataPackage = (DataTransmission_t *) payload;
            printf("\n      Transmitting data: %d.", (int16_t) dataPackage->td);
        }
        else if (packagePid == TRIGGER_SIGNAL_TRANSMISSION_PID) {
            printf("\n      Transmitting trigger signal.");
        }
        else {
            printf("\n      Transmitting protocol response.");
        }
    }
}

