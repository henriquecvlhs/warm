/**
 * @file TestReceiverC.nc
 * @brief File implementing configuration for the Receiver component's 
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define CONTROLLER_FLOW_ID 0x00CF
#define CONTROLLER_ID 0x0001

#define TASK_FLOW_ID 0x00DF
#define SOURCE_NODE_ID 0x0123

#define MAX_NUMBER_OF_SCHEDULINGS 20
#define NUMBER_OF_INPUT_SCHEDULINGS 30

/**
 * @brief Configuration for Receiver component's test unit.
 *
 * @note Assuming 20 is the size of input scheduler table.
 */
configuration TestReceiverC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestReceiverP;
    components ReceiverC;

    TestReceiverP.Boot -> MainC.Boot;

    ReceiverC.TaskInput -> TestReceiverP.dummyTaskAPI;
    ReceiverC.ProtocolInput -> TestReceiverP.dummyProtocolIn;
    ReceiverC.SDNReceive -> TestReceiverP.dummySDN;

    TestReceiverP.InputScheduler -> ReceiverC.InputScheduler;
    TestReceiverP.ReceiverStatus -> ReceiverC.ReceiverStatus;
}

