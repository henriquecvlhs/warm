/**
 * @file TestReceiverP.nc
 * @brief File implementing module of Receiver component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "protocol.h"

/**
 * @brief Module that implements Receiver component's test unit.
 */
module TestReceiverP @safe() {
    provides {
        interface TaskInput as dummyTaskAPI;
        interface ProtocolInput as dummyProtocolIn;
        interface SDNReceive as dummySDN;
    }

    uses {
        interface InputScheduler;
        interface ReceiverStatus;

        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    int16_t inputBuffer;
    uint8_t result;
    uint8_t totalSchedulings;
    uint8_t i;
    uint16_t testInputIds[] =   { 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                                  71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                                  81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };

    uint16_t testInputInstances[] = {  0,  0,  1,  1,  1,  1,  2,  2,  2, 2,
                                       2,  2,  0,  0,  0,  0,  0,  4,  4, 4,
                                       1,  1,  1,  1,  2,  5,  3,  4,  4, 1 };

    uint16_t testInputFlows[] = {  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                                  11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                  21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };

    NodeAssociateConfirmation_t nodeAssociate;
    TaskDescriptionRequest_t descriptionRequest;
    PeriodicTaskSchedulingRequest_t periodicRequest;
    InstantaneousTaskSchedulingRequest_t instantaneousRequest;
    TriggerTaskSchedulingRequest_t triggerRequest;
    ScheduledTaskCancellationRequest_t cancellationRequest;
    TaskStatisticsRequest_t taskStatistics;
    NodeStatisticsRequest_t nodeStatistics;

    DataTransmission_t dataPackage;
    TriggerSignalTransmission_t triggerPackage;

    event void Boot.booted() {
        /** Test init. */
        printf("Testing Receiver");

        /** Test forwarding of protocol packages */
        printf("\n\n1) Test forwarding of protocol packages.\n");

        printf("\n    Feeding Node Associate Confirmation package.");
        nodeAssociate.pid = NODE_ASSOCIATE_CONFIRMATION_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &nodeAssociate,
                                sizeof(NodeAssociateConfirmation_t));

        printf("\n    Feeding Task Description Request package.");
        descriptionRequest.pid = TASK_DESCRIPTION_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &descriptionRequest,
                                sizeof(TaskDescriptionRequest_t));

        printf("\n    Feeding Periodic Task Scheduling Request package.");
        periodicRequest.pid = PERIODIC_TASK_SCHEDULING_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &periodicRequest,
                                sizeof(PeriodicTaskSchedulingRequest_t));

        printf("\n    Feeding Instantaneous Task Scheduling Request package.");
        instantaneousRequest.pid = INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &instantaneousRequest,
                                sizeof(InstantaneousTaskSchedulingRequest_t));

        printf("\n    Feeding Trigger Task Scheduling Request package.");
        triggerRequest.pid = TRIGGER_TASK_SCHEDULING_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &triggerRequest,
                                sizeof(TriggerTaskSchedulingRequest_t));

        printf("\n    Feeding Scheduled Task Cancellation Request package.");
        cancellationRequest.pid = SCHEDULED_TASK_CANCELLATION_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &cancellationRequest,
                                sizeof(ScheduledTaskCancellationRequest_t));

        printf("\n    Feeding Task Statistics Request package.");
        taskStatistics.pid = TASK_STATISTICS_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &taskStatistics,
                                sizeof(TaskStatisticsRequest_t));

        printf("\n    Feeding Node Statistics Request package.");
        nodeStatistics.pid = NODE_STATISTICS_REQUEST_PID;
        signal dummySDN.receive(CONTROLLER_ID, 
                                CONTROLLER_FLOW_ID, 
                                (void *) &nodeStatistics,
                                sizeof(NodeStatisticsRequest_t));

        /** Test input scheduling */
        printf("\n\n2) Test input scheduling.\n");

        for (i = 0; i < NUMBER_OF_INPUT_SCHEDULINGS; i++) {
            printf("\n    Trying to schedule input for parameter %d of instance %d of Task %u, coming from flow %u.", testInputInstances[i], testInputInstances[i], testInputIds[i], testInputFlows[i]);

            result = call InputScheduler.scheduleTaskInput(testInputIds[i],
                                                         testInputInstances[i],
                                                         testInputInstances[i],
                                                         testInputFlows[i]);

            if ((i < MAX_NUMBER_OF_SCHEDULINGS) && !result) {
                printf("\n    Success.");
            }
            else if ((i >= MAX_NUMBER_OF_SCHEDULINGS) && result) {
                printf("\n    Success.");
            }
            else {
                printf("\n    Failure.");
            }
        }

        /** Test Receiver status */
        printf("\n\n3) Test Receiver status.\n");

        totalSchedulings = call ReceiverStatus.getInputSchedulesStatus();

        printf("\n    Current number of schedules is %d: ", totalSchedulings);
        if (totalSchedulings >= MAX_NUMBER_OF_SCHEDULINGS) {
            printf("success!");
        }
        else {
            printf("failure!");
        }

        /** Test Cancellation of inputs */
        printf("\n\n4) Test input of Data and Trigger Signal Transmission packages.\n");
        printf("\n\n    Expect only %d packages being forwarded.", MAX_NUMBER_OF_SCHEDULINGS);

        for (i = 0; i < NUMBER_OF_INPUT_SCHEDULINGS; i++) {
            inputBuffer = 10 - i;
            dataPackage.pid = DATA_TRANSMISSION_PID;
            dataPackage.odt = 2;
            dataPackage.odl = 1;
            dataPackage.td = *((uint64_t *) &inputBuffer);
            signal dummySDN.receive(SOURCE_NODE_ID,
                                    testInputFlows[i],
                                    (void *) &dataPackage,
                                    sizeof(DataTransmission_t));
        }

        /** Test Cancellation of inputs */
        printf("\n\n5) Test cancellation of inputs.\n");

        for (i = 0; i < MAX_NUMBER_OF_SCHEDULINGS; i++) {
            printf("\n    Cancelling input schedule for instance %d of Task %u.", testInputInstances[i], testInputIds[i]);

            call InputScheduler.cancelTaskInput(testInputIds[i],
                                                testInputInstances[i]);
        }

        totalSchedulings = call ReceiverStatus.getInputSchedulesStatus();

        printf("\n    Current number of schedules is %d: ", totalSchedulings);
        if (totalSchedulings == 0) {
            printf("success!");
        }
        else {
            printf("failure!");
        }

        printf("\nEnd of test.\n");
    }
    
    command void dummyTaskAPI.data(uint16_t taskId, 
                                   uint8_t taskInstance, 
                                   uint8_t inputId,
                                   void *value) {
        printf("\n====> Forwarding received data %d as input %d to instance %d of Task %u.", *((int16_t *) value), inputId, taskInstance, taskId);
    }

    command void dummyTaskAPI.trigger(uint16_t taskId, uint8_t taskInstance) {
        printf("\n====> Forwarding trigger signal to instace %d of Task %u.", taskInstance, taskId);
    }

    command void dummyProtocolIn.data(void *protocolRequest) {
        uint8_t packagePid = (*((uint8_t *) protocolRequest) >> 4);
        printf("\n====> Forwarding to package of PID %d to Protocol Processor: ", packagePid);
        if ((packagePid != DATA_TRANSMISSION_PID) && 
            (packagePid != TRIGGER_SIGNAL_TRANSMISSION_PID)) {
            printf("Correct!");
        }
        else {
            printf("Wrong!");
        }
    }
}

