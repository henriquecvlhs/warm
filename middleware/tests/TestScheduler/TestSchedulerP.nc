/**
 * @file TestSchedulerP.nc
 * @brief File implementing module of Scheduler component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Scheduler component's test unit.
 */
module TestSchedulerP @safe() {
    provides {
        interface InputScheduler as dummyReceiver;
        interface EmitterScheduler as dummyEmitter;
        interface TaskScheduler as dummyTaskAPI;
    }

    uses {
        interface ProtocolScheduler;
        interface SchedulerStatus;
        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    uint8_t result;
    uint8_t i, j;
    uint8_t errorCode;
    uint8_t periodicTasksScheduled;
    uint16_t periodicTasksToTest[] = { 0, 15, 48, 5432, 124, 576 };
    uint16_t instantaneousTasksToTest[] = { 1, 16, 49, 2345, 421, 557 };
    uint8_t testInputSizes[] = { 1, 10, 3, 5, 9, 2, 4, 7, 1, 9 };
    uint16_t testInputFlows[] = { 1234, 5488, 3284, 3783, 3362, 
                                  4953, 2973, 7683, 8748, 6472 };

    event void Boot.booted() {
        /** Test init. */
        printf("Testing Scheduler");

        /** Schedule task instances */
        printf("\n\n1) Schedule different periodic task instances.\n");
        for (i = 0; i < sizeof(periodicTasksToTest)/2; i++) {
            for (j = 0; j < 10; j++) {
                result = call ProtocolScheduler.schedulePeriodicTask(
                                                        periodicTasksToTest[i],
                                                        &j,
                                                        TRIGGER_ONCE, 
                                                        1000 * (j + 1),
                                                        0,
                                                        j % 3,
                                                        j % 8,
                                                        1000 + j,
                                                        &errorCode);

                if (!result && (i < 2)) {
                    printf("\n    Test OK.");
                }
                else if (result && (i >= 2)) {
                    printf("\n    Test OK.");
                }
                else {
                    printf("\n    Test failed.");
                }
            }
        }

        printf("\n\n2) Schedule different instantaneous task instances.\n");
        for (i = 0; i < sizeof(instantaneousTasksToTest)/2; i++) {
            for (j = 0; j < 10; j++) {
                result = call ProtocolScheduler.scheduleInstantaneousTask(
                                                        instantaneousTasksToTest[i],
                                                        &j,
                                                        TRIGGER_ONCE, 
                                                        1000 * (j + 1),
                                                        0,
                                                        j % 3,
                                                        j % 8,
                                                        2000 + j,
                                                        i,
                                                        testInputFlows,
                                                        testInputSizes,
                                                        NULL,
                                                        &errorCode);

                if (!result) {
                    printf("\n    Test OK.");
                }
                else {
                    printf("\n    Test failed.");
                }
            }
        }

        printf("\n\n3) Test execution of instantaneous tasks.\n");
        printf("\nSimulate ready to run events for the first five instances\nof each scheduled instantaneous task. A single execution for\nsuch instances should be expected to happen.\n");
        printf("\nSimulation for task 1 instances' ready to run events will not\nbe done, since this task has no input and therefore its instances\nshould have been executed and cancelled at the moment they were\nscheduled.\n");

        for (i = 0; i < sizeof(instantaneousTasksToTest)/2; i++) {
            for (j = 1; j < 5; j++) {
                signal dummyTaskAPI.taskReadyToRun(instantaneousTasksToTest[i],
                                                   j,
                                                   INSTANTANEOUS_TASK_TYPE);
            }
        }

        printf("\n\n3) Test execution of periodic tasks.\n");

        periodicTasksScheduled = call SchedulerStatus.getPeriodicSchedulesStatus();
        printf("\nThere are currently %d periodic task instances scheduled. ", periodicTasksScheduled);

        if (periodicTasksScheduled == 20) {
            printf("(Test OK)\n");
        }
        else {
            printf("(Test failed)\n");
        }

        printf("\nSimulate ready to run events instances 0 through 4 of periodic task %d\nand for instances 5 through 9 of periodic task %d. Their execution should\nstart being called at regular intervals by the scheduler. Also simulating\nthis event forall instances of task %d. They should not have been scheduled\n(lack of space), and therefore should not be executed.\n", periodicTasksToTest[0], periodicTasksToTest[1], periodicTasksToTest[sizeof(periodicTasksToTest)/2 - 1]);
        
        printf("\nSimulating now. Executions should start 1 sec from this moment.\n\n");

        for (i = 0; i < 5; i++) {
            signal dummyTaskAPI.taskReadyToRun(periodicTasksToTest[0], i, PERIODIC_TASK_TYPE);
        }

        for (i = 5; i < 10; i++) {
            signal dummyTaskAPI.taskReadyToRun(periodicTasksToTest[1], i, PERIODIC_TASK_TYPE);
        }

        for (i = 0; i < 10; i++) {
            signal dummyTaskAPI.taskReadyToRun(periodicTasksToTest[sizeof(periodicTasksToTest)/2 - 1], i, PERIODIC_TASK_TYPE);
        }
    }

    void continueTesting(void) {

        printf("\n\n4) Cancellation of scheduled periodic task instances.\n");

        for (i = 0; i < 2; i++) {
            for (j = 0; j < 10; j++) {
                call ProtocolScheduler.cancelSchedule(periodicTasksToTest[i], j);
            }
        }
        
        printf("\nPeriodic task instances should have stoped being executed by the scheduler.");

        periodicTasksScheduled = call SchedulerStatus.getPeriodicSchedulesStatus();
        printf("\nThere are currently %d periodic task instances scheduled.", periodicTasksScheduled);

        if (periodicTasksScheduled) {
            printf("\n    Test failed.");
        }
        else {
            printf("\n    Test OK.");
        }

        printf("\n\nEnd of test.\n");
    }

    
    command uint8_t dummyReceiver.scheduleTaskInput(uint16_t taskId,
                                                    uint8_t taskInstance,
                                                    uint8_t parameterId,
                                                    uint16_t inputFlowId) {
        printf("\n====> Scheduled input for instance %d of task %d", taskInstance, taskId);
        printf("\n            input: parameter %d (flow: %d)", parameterId, inputFlowId);
        return 0;
    }

    command void dummyReceiver.cancelTaskInput(uint16_t taskId,
                                               uint8_t taskInstance) {
        printf("\n====> Cancelled input for instance %d of task %d", taskInstance, taskId);
    }

    command uint8_t dummyEmitter.scheduleTaskOutput(uint16_t taskId,
                                                    uint8_t taskInstance,
                                                    uint8_t triggerOutput,
                                                    uint8_t outputDataType,
                                                    uint8_t outputDataLength,
                                                    uint16_t outputFlowId) {
        printf("\n====> Scheduled output for instance %d of task %d", taskInstance, taskId);
        printf("\n            output: %d - %d (flow: %d)", outputDataType, outputDataLength, outputFlowId);
        return 0;
    }

    command void dummyEmitter.cancelTaskOutput(uint16_t taskId,
                                               uint8_t taskInstance) {
        printf("\n====> Cancelled output for instance %d of task %d", taskInstance, taskId);
    }

    command uint8_t dummyTaskAPI.schedulePeriodicTask(
                                             uint16_t taskId, 
                                             uint8_t *taskInstance,
                                             TaskTriggerPolicy_t triggerPolicy){
        printf("\n====> Scheduled instance %d of periodic task %d", *taskInstance, taskId);
        return 0;
    }


    command uint8_t dummyTaskAPI.scheduleInstantaneousTask(
                                              uint16_t taskId, 
                                              uint8_t *taskInstance,
                                              TaskTriggerPolicy_t triggerPolicy,
                                              uint8_t *inputSize,
                                              char *argumentString) {
        printf("\n====> Scheduled instance %d of instantaneous task %d", *taskInstance, taskId);
        return 0;
    }

    command uint8_t dummyTaskAPI.scheduleTriggerTask(
                                        uint16_t taskId, 
                                        uint8_t *taskInstance,
                                        TaskTriggerComparisonRule_t triggerRule,
                                        TaskTriggerStorageRule_t minuendRule,
                                        TaskTriggerStorageRule_t subtrahendRule,
                                        TaskTriggerStorageRule_t referenceRule,
                                        void *minuendInitialization,
                                        void *subtrahendInitialization,
                                        void *referenceInitialization) {
        printf("\n====> Scheduled instance %d of trigger task %d", *taskInstance, taskId);
        return 0;
    }

    command void dummyTaskAPI.cancelTask(uint16_t taskId, 
                                         uint8_t taskInstance) {
        printf("\n====> Cancelled instance %d of task %d", taskInstance, taskId);
    }

    command void dummyTaskAPI.runTask(uint16_t taskId, uint8_t taskInstance) {
        if ((taskId != periodicTasksToTest[0]) && (taskId != periodicTasksToTest[1])) {
            printf("\n");
        }
        printf("====> Run instance %d of task %d\n", taskInstance, taskId);

        if ((taskInstance == 9) && (taskId == periodicTasksToTest[1])) {
            continueTesting();
        }
    }

    command void dummyTaskAPI.discardTaskData(uint16_t taskId, 
                                              uint8_t taskInstance) {
        printf("\n====> Discarded data for instance %d of task %d", taskInstance, taskId);
    }
}

