/**
 * @file TestTaskAPIC.nc
 * @brief File implementing configuration for the Task API component's 
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define SENSE_TEMPERATURE_TASK_ID 0
#define HELPER_TASK_EXAMPLE_TASK_ID 4321

#define HELPER_TASK_EXAMPLE_INPUT_SIZE 5

/**
 * @brief Configuration for Task API component's test unit.
 *
 * @note This test unit tests the Task API component assuming tasks 
 *       "Sense Temperature" and "Helper Task Example" are loaded.
 *       In case these tasks aren't loaded, the tests will certainly fail.
 */
configuration TestTaskAPIC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestTaskAPIP;
    components TaskAPIC;

    TestTaskAPIP.Boot -> MainC.Boot;

    TestTaskAPIP.TaskScheduler -> TaskAPIC.TaskScheduler;
    TestTaskAPIP.TaskInput -> TaskAPIC.TaskInput;
    TestTaskAPIP.TaskOutput -> TaskAPIC.TaskOutput;
    TestTaskAPIP.TaskStatus -> TaskAPIC.TaskStatus;
    TestTaskAPIP.TaskDescription -> TaskAPIC.TaskDescription;
}

