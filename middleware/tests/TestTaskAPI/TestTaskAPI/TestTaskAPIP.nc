/**
 * @file TestTaskAPIP.nc
 * @brief File implementing module of Task API component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Task API component's test unit.
 */
module TestTaskAPIP @safe() {
    uses {
        interface TaskScheduler;
        interface TaskInput;
        interface TaskOutput;
        interface TaskStatus;
        interface TaskDescription;
        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    void *voidDescription;
    TaskDescriptionResponse_t description;
    void *inputData;
    uint8_t testedInstance;
    uint8_t result;
    uint8_t taskReady[] = { 0, 0, 0, 0 };
    uint8_t inputSizes[] = { 1, HELPER_TASK_EXAMPLE_INPUT_SIZE };
    uint8_t i;

    event void Boot.booted() {
        /** Test init. */
        printf("Testing Task API\n");

        /** Schedule task instances */
        printf("\n1) Schedule different task instances.\n");

        printf("    Schedule Sense Temperature task instances.\n");

        result = call TaskScheduler.schedulePeriodicTask(SENSE_TEMPERATURE_TASK_ID, 
                                                         &testedInstance, 
                                                         NO_TRIGGER);
        if (!result) {
            printf("        Successfully scheduled instance %d of task Sense Temperature.\n", testedInstance);
        }
        else {
            printf("        Failed to schedule an instance of task Sense Temperature.\n");
        }


        result = call TaskScheduler.schedulePeriodicTask(SENSE_TEMPERATURE_TASK_ID, 
                                                         &testedInstance, 
                                                         TRIGGER_ONCE);
        if (!result) {
            printf("        Successfully scheduled instance %d of task Sense Temperature.\n", testedInstance);
        }
        else {
            printf("        Failed to schedule an instance of task Sense Temperature.\n");
        }

        printf("    Schedule Helper Task Example task instances.\n");

        result = call TaskScheduler.scheduleInstantaneousTask(
                                                HELPER_TASK_EXAMPLE_TASK_ID, 
                                                &testedInstance, 
                                                NO_TRIGGER,
                                                inputSizes,
                                                NULL);
        if (!result) {
            printf("        Successfully scheduled instance %d of task Helper Task Example.\n", testedInstance);
        }
        else {
            printf("        Failed to schedule an instance of task Helper Task Example.\n");
        }

        result = call TaskScheduler.scheduleInstantaneousTask(
                                                HELPER_TASK_EXAMPLE_TASK_ID, 
                                                &testedInstance, 
                                                TRIGGER_ONCE,
                                                inputSizes,
                                                NULL);
        if (!result) {
            printf("        Successfully scheduled instance %d of task Helper Task Example.\n", testedInstance);
        }
        else {
            printf("        Failed to schedule an instance of task Helper Task Example.\n");
        }


        /** Feed input to task instances */
        printf("\n2) Feed input to task instances.\n");

        printf("    Trigger appropriate task instances.\n");
        call TaskInput.trigger(SENSE_TEMPERATURE_TASK_ID, 1);
        call TaskInput.trigger(HELPER_TASK_EXAMPLE_TASK_ID, 1);

        printf("    Feed input data to appropriate task instances.\n");
        *((uint8_t *) inputData) = 3;
        call TaskInput.data(HELPER_TASK_EXAMPLE_TASK_ID, 0, 0, inputData);
        *((uint8_t *) inputData) = 4;
        call TaskInput.data(HELPER_TASK_EXAMPLE_TASK_ID, 1, 0, inputData);
        for (i = 0; i < HELPER_TASK_EXAMPLE_INPUT_SIZE; i++) {
            *((int32_t *) inputData) = 7;
            call TaskInput.data(HELPER_TASK_EXAMPLE_TASK_ID, 0, 1, inputData);
            *((int32_t *) inputData) = 13;
            call TaskInput.data(HELPER_TASK_EXAMPLE_TASK_ID, 1, 1, inputData);
        }

        /** Check if task instances are now ready */
        if (taskReady[0]) {
            printf("    Test was successful for instance 0 of task 0.\n");
        }
        else {
            printf("    Test was unsuccessful for instance 0 of task 0.\n");
        }

        if (taskReady[1]) {
            printf("    Test was successful for instance 1 of task 0.\n");
        }
        else {
            printf("    Test was unsuccessful for instance 1 of task 0.\n");
        }

        if (taskReady[2]) {
            printf("    Test was successful for instance 0 of task 4321.\n");
        }
        else {
            printf("    Test was unsuccessful for instance 0 of task 4321.\n");
        }

        if (taskReady[3]) {
            printf("    Test was successful for instance 0 of task 4321.\n");
        }
        else {
            printf("    Test was unsuccessful for instance 0 of task 4321.\n");
        }

        /** Run task instances which are ready */
        printf("\n3) Run task instances.\n");

        if (taskReady[0]) {
            call TaskScheduler.runTask(SENSE_TEMPERATURE_TASK_ID, 0);
        }
    }

    void continue_tests(void) {
        /** Continue running task instances which are ready */
        if (taskReady[1]) {
            call TaskScheduler.runTask(SENSE_TEMPERATURE_TASK_ID, 1);
        }
    }

    void finish_tests(void) {
        /** Continue running task instances which are ready */
        if (taskReady[2]) {
            call TaskScheduler.runTask(HELPER_TASK_EXAMPLE_TASK_ID, 0);
        }

        if (taskReady[3]) {
            call TaskScheduler.runTask(HELPER_TASK_EXAMPLE_TASK_ID, 1);
        }

        /** Check task execution */
        if (call TaskStatus.getExecutionStatus(SENSE_TEMPERATURE_TASK_ID, 0)) {
            printf("    Instance 0 of task 0 was successfully run.\n");
        }
        else {
            printf("    Instance 0 of task 0 was unsuccessfully run.\n");
        }

        if (call TaskStatus.getExecutionStatus(SENSE_TEMPERATURE_TASK_ID, 1)) {
            printf("    Instance 1 of task 0 was successfully run.\n");
        }
        else {
            printf("    Instance 1 of task 0 was unsuccessfully run.\n");
        }

        if (call TaskStatus.getExecutionStatus(HELPER_TASK_EXAMPLE_TASK_ID, 0)) {
            printf("    Instance 0 of task 4321 was successfully run.\n");
        }
        else {
            printf("    Instance 0 of task 4321 was unsuccessfully run.\n");
        }

        if (call TaskStatus.getExecutionStatus(HELPER_TASK_EXAMPLE_TASK_ID, 1)) {
            printf("    Instance 1 of task 4321 was successfully run.\n");
        }
        else {
            printf("    Instance 1 of task 4321 was unsuccessfully run.\n");
        }

        /** Cancel task instances */
        printf("\n4) Cancelling task instances.\n");

        call TaskScheduler.cancelTask(SENSE_TEMPERATURE_TASK_ID, 0);
        call TaskScheduler.cancelTask(SENSE_TEMPERATURE_TASK_ID, 1);
        call TaskScheduler.cancelTask(HELPER_TASK_EXAMPLE_TASK_ID, 0);
        call TaskScheduler.cancelTask(HELPER_TASK_EXAMPLE_TASK_ID, 1);

        /** Check task scheduling status */
        if (call TaskStatus.getScheduleStatus(SENSE_TEMPERATURE_TASK_ID, 0)) {
            printf("    Instance 0 of task 0 was unsuccessfully cancelled.\n");
        }
        else {
            printf("    Instance 0 of task 0 was successfully cancelled.\n");
        }

        if (call TaskStatus.getScheduleStatus(SENSE_TEMPERATURE_TASK_ID, 1)) {
            printf("    Instance 1 of task 0 was unsuccessfully cancelled.\n");
        }
        else {
            printf("    Instance 1 of task 0 was successfully cancelled.\n");
        }

        if (call TaskStatus.getScheduleStatus(HELPER_TASK_EXAMPLE_TASK_ID, 0)) {
            printf("    Instance 0 of task 4321 was unsuccessfully cancelled.\n");
        }
        else {
            printf("    Instance 0 of task 4321 was successfully cancelled.\n");
        }

        if (call TaskStatus.getScheduleStatus(HELPER_TASK_EXAMPLE_TASK_ID, 1)) {
            printf("    Instance 1 of task 4321 was unsuccessfully cancelled.\n");
        }
        else {
            printf("    Instance 1 of task 4321 was successfully cancelled.\n");
        }

        /** Get task descriptions */
        printf("\n5) Getting task descriptions.\n");

        voidDescription = call TaskDescription.getDescription(SENSE_TEMPERATURE_TASK_ID);
        description = *((TaskDescriptionResponse_t *) voidDescription);
        printf("  Printing description of task 0:\n");
        printf("    PID = %d\n", description.pid);
        printf("    TID = %d\n", description.tid);
        printf("    TTI = %d\n", description.tti);
        printf("    MSQ = %d\n", description.msq);
        printf("    ODF = 0x%x\n", description.odf);
        printf("    IPN = %d\n", description.ipn);
        printf("    D   = %d\n", description.d);
        printf("    A   = %d\n", description.a);
        printf("    G   = %d\n", description.g);
        printf("    S   = %d\n", description.s);

        voidDescription = call TaskDescription.getDescription(HELPER_TASK_EXAMPLE_TASK_ID);
        description = *((TaskDescriptionResponse_t *) voidDescription);
        printf("  Printing description of task 4321:\n");
        printf("    PID = %d\n", description.pid);
        printf("    TID = %d\n", description.tid);
        printf("    TTI = %d\n", description.tti);
        printf("    MSQ = %d\n", description.msq);
        printf("    ODF = 0x%x\n", description.odf);
        printf("    IPN = %d\n", description.ipn);
        printf("    D   = %d\n", description.d);
        printf("    A   = %d\n", description.a);
        printf("    G   = %d\n", description.g);
        printf("    S   = %d\n", description.s);
        for (i = 0; i < 2; i++) {
            printf("    IPARAM[%d] : IPQ = %d\n", i, description.iparam[i].ipq);
            printf("    IPARAM[%d] : IPF = 0x%x\n", i, description.iparam[i].ipf);
        }
    }


    event void TaskScheduler.taskReadyToRun(uint16_t taskId,
                                            uint8_t taskInstance,
                                            TaskType_t taskType) {
        printf("    ----> Event: Instance %d of task of id %d is ready to run.\n", taskInstance, taskId);
        
        if (taskId == SENSE_TEMPERATURE_TASK_ID) {
            taskReady[taskInstance] = 1;
        }
        else if (taskId == HELPER_TASK_EXAMPLE_TASK_ID) {
            taskReady[2 + taskInstance] = 1;
        }
    }

    event void TaskOutput.ready(uint16_t taskId, 
                                uint8_t taskInstance,
                                void *value) {
        if (taskId == SENSE_TEMPERATURE_TASK_ID) {
            printf("    ----> Event: Instance %d of task of id %d has produced output data: %d.\n", taskInstance, taskId, *((int16_t *) value));

            if (taskInstance == 0) continue_tests();
            if (taskInstance == 1) finish_tests();
        }
        else if (taskId == HELPER_TASK_EXAMPLE_TASK_ID) {
            printf("    ----> Event: Instance %d of task of id %d has produced output data: %d.\n", taskInstance, taskId, *((int32_t *) value));
        }
    }
}

