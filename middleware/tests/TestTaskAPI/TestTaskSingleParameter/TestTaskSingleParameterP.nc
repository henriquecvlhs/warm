/**
 * @file TestTaskSingleParameterP.nc
 * @brief File implementing module of Task Single Parameter component's
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Task Single Parameter component's test 
 *        unit.
 */
module TestTaskSingleParameterP @safe() {
    uses {
        interface TaskParameterInput as parameterInput;
        interface TaskSingleParameterAccess<TEST_PARAMETER_TYPE> as parameterAccess;
        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    TEST_PARAMETER_TYPE testValue = 12;
    TEST_PARAMETER_TYPE testContent;
    uint8_t testBoolean = 0;
    void *inputData;
    int i;

    event void Boot.booted() {
        /** Test init. */
        call parameterInput.init();
        printf("Testing TaskInputParameter\n");

        /** Feed input values to a few instances. */
        printf("\n1) Feed values to a few task instances.\n");
        for (i = 0; i < TEST_INSTANCE_NUMBER/2; i++) {
            printf("    Feeding %d to instance ", testValue + i);
            printf("%d\n", i);
            *((TEST_PARAMETER_TYPE *) inputData) = testValue + i;
            call parameterInput.receive(i, inputData);
        }

        /** Check if input was done correctly and also test status and get methods. */
        printf("\n2) Check values fed to task instances.\n");
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            if ((call parameterInput.status(i)) == INPUT_STATUS_READY) {
                testContent = call parameterAccess.get(i);
                printf("    Getting value from instance %d: %d\n", i, testContent);
                if (testContent == (testValue + i)) {
                    printf("        Test OK!\n");
                }
                else {
                    printf("        Test Failure!\n");
                }
            }
            else {
                printf("    Instance %d not ready.\n", i);
                if (i >= TEST_INSTANCE_NUMBER/2) {
                    printf("        Test OK!\n");
                }
                else {
                    printf("        Test Failure!\n");
                }
            }
        }

        printf("\n3) Testing the discarding of data.\n");
        /** Discard data for every instance */
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            call parameterInput.discard(i);
        }
        /** Test if data was actually discarded */
        testBoolean = 1;
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            if ((call parameterInput.status(i)) != INPUT_STATUS_NOT_READY) {
                testBoolean = 0;
            }
        }
        /** Print test result */
        if (testBoolean) {
            printf("    All data successfully discarded.\n");
        }
        else {
            printf("    Failure while discarding data.\n");
        }

        printf("\n4) Testing the setting of data.\n");
        testBoolean = 0;
        call parameterAccess.set(TEST_INSTANCE_NUMBER/2, testValue);
        if ((call parameterInput.status(TEST_INSTANCE_NUMBER/2)) == INPUT_STATUS_READY) {
            if ((call parameterAccess.get(TEST_INSTANCE_NUMBER/2)) == testValue) {
                testBoolean = 1;
            }
        }
        /** Print test result */
        if (testBoolean) {
            printf("    Successfully set data.\n");
        }
        else {
            printf("    Failed to set data.\n");
        }

        /** Test storage policies */
        printf("\n5) Testing storage policies.\n");
        call parameterInput.discard(0);

        /** Test "Store Least Recent" policy */
        printf("    Testing \"Store Least Recent\" policy.\n");
        call parameterInput.configureStoragePolicy(STORE_LEAST_RECENT_INPUT);
        /** Feed two values and check if first is still stored */
        *((TEST_PARAMETER_TYPE *) inputData) = testValue;
        call parameterInput.receive(0, inputData);
        *((TEST_PARAMETER_TYPE *) inputData) = testValue + 1;
        call parameterInput.receive(0, inputData);
        if ((call parameterAccess.get(0)) != testValue) {
            printf("        Failed to keep least recent data!\n");
        }
        else {
            printf("        Successfully kept least recent data!\n");
        }

        /** Test "Store Most Recent" policy */
        printf("    Testing \"Store Most Recent\" policy.\n");
        call parameterInput.configureStoragePolicy(STORE_MOST_RECENT_INPUT);
        /** Feed two values and check if the last is now stored */
        *((TEST_PARAMETER_TYPE *) inputData) = testValue;
        call parameterInput.receive(0, inputData);
        *((TEST_PARAMETER_TYPE *) inputData) = testValue + 1;
        call parameterInput.receive(0, inputData);
        if ((call parameterAccess.get(0)) != testValue + 1) {
            printf("        Failed to keep most recent data!\n");
        }
        else {
            printf("        Successfully kept most recent data!\n");
        }
    }

    event void parameterInput.ready(uint8_t taskInstance) {
        /** Do nothing */
        printf("    ===> Input of instance %d is ready!\n", taskInstance);
        return;
    }
}

