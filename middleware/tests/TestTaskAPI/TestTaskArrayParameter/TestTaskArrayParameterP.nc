/**
 * @file TestTaskArrayParameterP.nc
 * @brief File implementing module of Task Array Parameter component's
 *        test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Task Array Parameter component's test 
 *        unit.
 */
module TestTaskArrayParameterP @safe() {
    uses {
        interface TaskParameterInput as arrayInput;
        interface TaskArrayParameterAccess<TEST_PARAMETER_TYPE> as arrayAccess;
        interface Boot;
    }
}
implementation {
    /** Test variables we need in booted but cannot declare there */
    uint8_t testSize = 8;
    TEST_PARAMETER_TYPE testValue = 12;
    TEST_PARAMETER_TYPE testContent;
    void *inputData;
    uint8_t testBoolean = 0;
    int i, j;

    event void Boot.booted() {
        /** Test init. */
        call arrayInput.init();
        printf("Testing TaskArrayInputParameter\n");

        /** Set size for different instances */
        printf("\n1) Configure different instance sizes.\n");
        for (i = 0; i < TEST_INSTANCE_NUMBER/2; i++) {
            printf("    Setting to %d size of instance %d\n", testSize + i, i);
            call arrayInput.setInstanceSize(i, testSize + i);
        }

        /** Check size of each instance */
        testBoolean = 1;
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            if (i < TEST_INSTANCE_NUMBER/2) {
                if ((call arrayAccess.getInstanceSize(i)) != testSize + i) testBoolean = 0;
                else if ((testSize + i) > TEST_ARRAY_SIZE) {
                    if ((call arrayAccess.getInstanceSize(i)) != TEST_ARRAY_SIZE) testBoolean = 0;
                }
            }
            else {
                if ((call arrayAccess.getInstanceSize(i)) != TEST_ARRAY_SIZE) testBoolean = 0;
            }
        }

        /** Print result of test 1 */
        if (testBoolean) {
            printf("    Successfully set instance sizes!\n");
        }
        else {
            printf("    Failed to set instance sizes!\n");
        }

        /** Check if parameters are properly filled. */
        printf("\n2) Feeding instances with %d input values each.\n", testSize + 2);
        printf("Note that, for some instances, more than one event may happen, since the parameter will receive more inputs than it can hold.\n");
        for (i = 0; i < TEST_INSTANCE_NUMBER/2; i++) {
            for (j = 1; j <= testSize + 2; j++) {
                *((TEST_PARAMETER_TYPE *) inputData) = (TEST_PARAMETER_TYPE) j;
                call arrayInput.receive(i, inputData);
            }
        }

        /** Check which become ready */
        testBoolean = 1;
        for (i = 0; i < TEST_INSTANCE_NUMBER/2; i++) {
            if ((call arrayAccess.getInstanceSize(i)) <= testSize + 2) {
                if ((call arrayInput.status(i)) != INPUT_STATUS_READY) testBoolean = 0;
            }
            else {
                if ((call arrayInput.status(i)) != INPUT_STATUS_NOT_READY) testBoolean = 0;
            }
        }

        /** Print result of test 2 */
        if (testBoolean) {
            printf("    Input data successfully received by instances!\n");
        }
        else {
            printf("    Instances failed to receive input data!\n");
        }

        printf("\n3) Testing the discarding of data.\n");
        /** Discard data for every instance */
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            call arrayInput.discard(i);
        }
        /** Test if data was actually discarded */
        testBoolean = 1;
        for (i = 0; i < TEST_INSTANCE_NUMBER; i++) {
            if ((call arrayInput.status(i)) != INPUT_STATUS_NOT_READY) {
                testBoolean = 0;
            }
        }
        /** Print test result */
        if (testBoolean) {
            printf("    All data successfully discarded.\n");
        }
        else {
            printf("    Failure while discarding data.\n");
        }

        printf("\n4) Testing the setting of data.\n");
        testBoolean = 0;
        call arrayAccess.set(TEST_INSTANCE_NUMBER/2, TEST_ARRAY_SIZE/2, testValue);
        if ((call arrayAccess.get(TEST_INSTANCE_NUMBER/2, TEST_ARRAY_SIZE/2)) == testValue) {
                testBoolean = 1;
        }
        /** Print test result */
        if (testBoolean) {
            printf("    Successfully set data.\n");
        }
        else {
            printf("    Failed to set data.\n");
        }

        /** Test storage policies */
        printf("\n5) Testing storage policies.\n");
        call arrayInput.setInstanceSize(0, TEST_ARRAY_SIZE/2);

        /** Test "Store Least Recent" policy */
        printf("    Testing \"Store Least Recent\" policy.\n");
        call arrayInput.configureStoragePolicy(STORE_LEAST_RECENT_INPUT);
        /** Feed one more value than max size */
        for (i = 0; i < (TEST_ARRAY_SIZE/2) + 1; i++) {
            *((TEST_PARAMETER_TYPE *) inputData) = testValue + i;
            call arrayInput.receive(0, inputData);
        }
        /** Check if any of the array values is equal to the last fed */
        testBoolean = 1;
        for (i = 0; i < (TEST_ARRAY_SIZE/2); i++) {
            if ((call arrayAccess.get(0, i)) == (testValue + TEST_ARRAY_SIZE/2)) testBoolean = 0;
        }
        /** Print results */
        if (testBoolean) {
            printf("        Success!\n");
        }
        else {
            printf("        Failure!\n");
        }
        
        /** Test "Store Most Recent" policy */
        printf("    Testing \"Store Most Recent\" policy.\n");
        call arrayInput.configureStoragePolicy(STORE_MOST_RECENT_INPUT);
        /** Feed one more value than max size */
        for (i = 0; i < (TEST_ARRAY_SIZE/2) + 1; i++) {
            *((TEST_PARAMETER_TYPE *) inputData) = testValue + i;
            call arrayInput.receive(0, inputData);
        }
        /** Check if first array value is equal to the last fed */
        if ((call arrayAccess.get(0, 0)) == (testValue + TEST_ARRAY_SIZE/2)) {
            printf("        Success!\n");
        }
        else {
            printf("        Failure!\n");
        }
    }

    event void arrayInput.ready(uint8_t taskInstance) {
        /** Do nothing */
        printf("    ===> Event: input of instance %d is ready!\n", taskInstance);
        return;
    }
}

