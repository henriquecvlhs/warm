/**
 * @file TestTaskArrayParameterC.nc
 * @brief File implementing configuration for the Task Array Parameter
 *        component's test unit.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#define TEST_INSTANCE_NUMBER 10
#define TEST_ARRAY_SIZE      12
#define TEST_PARAMETER_TYPE  uint32_t

/**
 * @brief Configuration for Task Array Parameter component's test unit.
 */
configuration TestTaskArrayParameterC {
}
implementation {
    components MainC; 
    components SerialPrintfC;
    components TestTaskArrayParameterP;
    components new TaskArrayParameterC(TEST_PARAMETER_TYPE, TEST_INSTANCE_NUMBER, TEST_ARRAY_SIZE) as arrayParameter;

    TestTaskArrayParameterP.Boot -> MainC.Boot;

    TestTaskArrayParameterP.arrayInput -> arrayParameter.TaskParameterInput;
    TestTaskArrayParameterP.arrayAccess -> arrayParameter.TaskArrayParameterAccess;
}

