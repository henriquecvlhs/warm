/**
 * @file BlinkLed.nc
 *
 * @brief Task that periodically blinks a LED.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Undefine all macros being defined here */
#undef TASK_ID
#undef TASK_TYPE
#undef NUMBER_OF_INSTANCES
#undef NUMBER_OF_INPUTS
#undef TASK_OUTPUT_TYPE

/** Task ID */
#define BLINK_LED_ID 4
#define TASK_ID BLINK_LED_ID

/** The type of this task */
#define TASK_TYPE PERIODIC_TASK_TYPE

/** Maximum number of simultaneous instances of this task */
#define NUMBER_OF_INSTANCES 10

/** Number of task input parameters */
#define NUMBER_OF_INPUTS 0 // No input

/** Output Data Type */
#define TASK_OUTPUT_TYPE void // No output

/**
 * @brief Configuration for Periodic Task Template component.
 */
configuration BlinkLedC {
    provides interface Task;
}
implementation {
    components BlinkLedP;

    components LedsC;
    BlinkLedP.Leds -> LedsC;

    Task = BlinkLedP.Task;
}
