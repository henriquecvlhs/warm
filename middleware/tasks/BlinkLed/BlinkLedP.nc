/**
 * @file BlinkLedP.nc
 *
 * @brief Task that periodically blinks a LED.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Periodic Task Template component.
 */
module PeriodicTaskTemplateP {
    provides interface Task;
    uses interface Leds;
}
implementation {
   
    PERIODIC_TASK_API_HELPER

    /** 
     * @brief Task initialization routine.
     *
     * This intialize any necessary variable related to the task
     * programming logic. It is called when the task component is 
     * initialized. It is up to the programmer to implement this, but
     * at least the task description should be initialized here.
     */
    void taskInit(void) {
        /** Initialize task description */
        INITIALIZE_TASK_DESCRIPTION(SUPPORT_NO_DATA, // No output
                                    // Task will not output data
                                    OUTPUT_TO_NETWORK_FALSE,
                                    // Task doesn't control an actuator
                                    CONTROL_ACTUATOR_TRUE,
                                    // Task doesn't aggregates data
                                    AGGREGATE_DATA_FALSE,
                                    // Task doesn't act as a sink
                                    DATA_SINK_FALSE);
    }

    /** 
     * @brief Task execution routine.
     *
     * This executes the task's routine. It is called every time the task
     * is run. This is what the programmer should be concerned with
     * implementing.
     *
     * @param taskInstance[in] Number identifying the task instance that 
     *                         will be run.
     * @param outputData[out]  Pointer to the value computed during task
     *                         execution that should be sent to the 
     *                         network.
     *
     * @return Boolean indicating if execution was successful.
     */
    uint8_t taskRoutine(uint8_t taskInstance) {

        // Implement task logic here
        call Leds.led0Toggle();

        return 1;
    }
}
