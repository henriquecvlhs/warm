/**
 * @file CompleteTaskExampleC
 *
 * This implements the module of a task that use no helpers from the
 * Task API.
 * This is useful for someone wanting to implement the Task interface in
 * a way other than the standard one.
 *
 * This is an instantaneous task that find, in an array of received values,
 * the first value that is repeated at least a certain number of times and 
 * then outputs this value to the network.
 * It only takes two parameters from the network:
 *  - Repeat: the number of times the value should be repeated.
 *  - Values: the array of values where we will search the repeated value.
 * If desired, pass a string argument of a maximum of 1 character meaning
 * an unsigned integer which will act like a filter: only repeat values 
 * higher than this number will be considered.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Complete Task Example component.
 */
module CompleteTaskExampleP {
    provides interface Task;

    uses {
        interface TaskParameterInput as input[uint8_t index];
        interface TaskSingleParameterAccess<REPEAT_INPUT_TYPE> as repeat;
        interface TaskArrayParameterAccess<VALUES_INPUT_TYPE> as values;
    }
}
implementation {
/************************************************************************
 * Auxiliary variables 
 ************************************************************************/

    /** Keep track of scheduled instances */
    uint8_t scheduleStatus[1 + (NUMBER_OF_INSTANCES - 1)/8];

    /** Keep track of trigger policies for each instance */
    TaskTriggerPolicy_t triggerPolicies[NUMBER_OF_INSTANCES];

    /** Keep track of trigger status for each instance */
    uint8_t triggerStatus[1 + (NUMBER_OF_INSTANCES - 1)/8];

    /** Keep track of execution status for each instance */
    uint16_t executionStatus[NUMBER_OF_INSTANCES];

#if (TASK_TYPE == INSTANTANEOUS_TASK_TYPE)
    /** Keep track of argument string for each instance */
    char argString[NUMBER_OF_INSTANCES][MAX_ARG_STRING_LENGTH];

    /** Keep track of argument string length for each instance */
    char argStringLength[NUMBER_OF_INSTANCES];
#endif

    /** Task description message, written according to protocol */
    TaskDescriptionResponse_t description;

/************************************************************************
 * Task programmer functions 
 ************************************************************************/

    /** 
     * @brief Task initialization routine.
     *
     * This intialize any necessary variable related to the task
     * programming logic. It is called when the task component is 
     * initialized. It is up to the programmer to implement this, but
     * at least the task description should be initialized here.
     */
    void taskInit(void) {
        /** Initialize task description */
        description.pid = TASK_DESCRIPTION_RESPONSE_PID;
        description.tid = TASK_ID;
        description.tti = TASK_TYPE;
        description.msq = NUMBER_OF_INSTANCES;
        description.odf = SUPPORT_INT |
                          SUPPORT_4_BYTE_LENGTH, // int32_t
        description.ipn = NUMBER_OF_INPUTS;
        description.d   = OUTPUT_TO_NETWORK_TRUE,
        description.a   = CONTROL_ACTUATOR_FALSE,
        description.g   = AGGREGATE_DATA_TRUE,
        description.s   = DATA_SINK_FALSE,
        description.iparam[REPEAT_INPUT_ID].ipq = 1;
        description.iparam[REPEAT_INPUT_ID].ipf = SUPPORT_UINT |
                                                  SUPPORT_1_BYTE_LENGTH;
        description.iparam[VALUES_INPUT_ID].ipq = VALUES_ARRAY_SIZE;
        description.iparam[VALUES_INPUT_ID].ipf = SUPPORT_INT |
                                                  SUPPORT_4_BYTE_LENGTH;
    }

    /** 
     * @brief Task execution routine.
     *
     * This executes the task's routine. It is called every time the task
     * is run. This is what the programmer should be concerned with
     * implementing.
     *
     * @param taskInstance[in] Number identifying the task instance that 
     *                         will be run.
     * @param outputData[out]  Pointer to the value computed during task
     *                         execution that should be sent to the 
     *                         network.
     *
     * @return Boolean indicating if execution was successful.
     */
    uint8_t taskRoutine(uint8_t taskInstance) { 
        TASK_OUTPUT_TYPE outputData;
        uint8_t i, j = 0;
        uint8_t repeatFilter;
        uint8_t valueCount;

        /** Get values from input parameters to temporary variables, so 
            we can work with them. This should be done because every time
            a value is read from a parameter it gets cleared. This is done
            so we know that the value has been used and that we can receive
            a new one from the network. */
        uint8_t valuesSize = call values.getInstanceSize(taskInstance);
        REPEAT_INPUT_TYPE curRepeat = call repeat.get(taskInstance);
        VALUES_INPUT_TYPE curValues[VALUES_ARRAY_SIZE];
        for (i = 0; i < valuesSize; i++) {
            curValues[i] = call values.get(taskInstance, i);
        }
        
        /** Check if repeat input is not null */
        if (curRepeat == 0) {
            /** Can't count values that were not even received */
            return 0;
        }

        /** Check if repeat input satisfies argument string */
        repeatFilter = (uint8_t) argString[taskInstance][0];
        if (argStringLength[taskInstance] != 0) {
            if (curRepeat <= repeatFilter) {
                /** We won't consider a success if this execution was 
                    blocked by filter */
                return 0;
            }
        }

        /** Search for value that repeats the specified number of times */
        for (i = 0; i < valuesSize; i++) {
            valueCount = 0;
            for (j = 0; j < valuesSize; j++) {
                if (curValues[i] == curValues[j]) {
                    valueCount++;
                }
            }

            /** Check if we have found the value we wanted */
            if (valueCount >= curRepeat) {
                outputData = curValues[i];
                /** Send output data to network */
                signal Task.outputReady(taskInstance, ((void *) &outputData));
                return 1;
            }
        }

        /** If we reach here, we clearly failed */
        return 0;
    }

/************************************************************************
 * Auxiliary functions 
 ************************************************************************/

    /** Check if task instance is ready to run */
    uint8_t checkIfReadyToRun(uint8_t taskInstance) {
        /** Check if input parameters are ready */
        uint8_t ready = 1;
        uint8_t i;
        for (i = 0; i < NUMBER_OF_INPUTS; i++) {
            if (call input.status[i](taskInstance) != INPUT_STATUS_READY) {
                ready = 0;
            }
        }
        
        /** Check trigger status */
        if (!GET_TRIGGER_STATUS(taskInstance)) {
            ready = 0;
        }

        return ready;
    }

/************************************************************************
 * Task command implementations 
 ************************************************************************/

    command void Task.run(uint8_t taskInstance) {
        /** We trust that the scheduler will only call this if the 
            task is truly ready to run */

        uint8_t success;

        /** Check if running scheduled instance */
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;

        /** Perform task */
        success = taskRoutine(taskInstance);

        /** Check execution status */
        if (success) {
            /** Increase execution counter */
            executionStatus[taskInstance]++;
        }
        
        /** Clear trigger if trigger always policy is set */
        if (triggerPolicies[taskInstance] == TRIGGER_ALWAYS) {
            CLEAR_TRIGGER_STATUS(taskInstance);
        }
    }

    command void Task.init(void) {
        uint8_t i;

        /** Initialize instances */
        for (i = 0; i < NUMBER_OF_INPUTS; i++) {
            call input.init[i]();
            CLEAR_SCHEDULE_STATUS(i);
            triggerPolicies[i] = NO_TRIGGER;
            SET_TRIGGER_STATUS(i);
            executionStatus[i] = 0;
#if (TASK_TYPE == INSTANTANEOUS_TASK_TYPE)
            argString[i][0] = 0x00;
            argStringLength[i] = 0;
#endif
        }

        /** Call Task API user initialization */
        taskInit();
    }

    command void Task.receiveData(uint8_t taskInstance, 
                                  uint8_t parameterId, 
                                  void *value) {
        /** Check if running scheduled instance */
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;

        call input.receive[parameterId](taskInstance, value);
    }

    command void Task.discardData(uint8_t taskInstance) {
        uint8_t i;
        for (i - 0; i < NUMBER_OF_INPUTS; i++) {
            call input.discard[i](taskInstance);
        }
    }

    command uint8_t Task.schedule(uint8_t *taskInstance, 
                                  TaskTriggerPolicy_t triggerPolicy) {
        /** Search for available task instance */
        uint8_t i = 0;
        uint8_t foundFreeInstance = 0;
        for (i = 0; i < NUMBER_OF_INSTANCES; i++) {
            if (GET_SCHEDULE_STATUS(i) == 0) {
                SET_SCHEDULE_STATUS(i);
                *taskInstance = i;
                foundFreeInstance = 1;
                break;
            }
        }

        /** Failure in case no instance is available */
        if (foundFreeInstance == 0) return 1;

        /** Configure trigger policy */
        triggerPolicies[*taskInstance] = triggerPolicy;

        /** Configure trigger status */
        if (triggerPolicy != NO_TRIGGER) {
            CLEAR_TRIGGER_STATUS(*taskInstance);
        }
        else {
            SET_TRIGGER_STATUS(*taskInstance);
        }

        /** Check if task is already ready to run */                
        if (checkIfReadyToRun(*taskInstance)) {                
            signal Task.readyToRun(*taskInstance, TASK_TYPE);       
        }                                                           

        return 0;
    }

    command void Task.configureMaxInput(uint8_t taskInstance,
                                        uint8_t parameterId,
                                        uint8_t inputMaxSize) {
        /** Check if running scheduled instance */
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;

        call input.setInstanceSize[parameterId](taskInstance, inputMaxSize);
    }

    command void Task.configureArgumentString(uint8_t taskInstance, 
                                              char *argumentString) {
#if (TASK_TYPE == INSTANTANEOUS_TASK_TYPE)
        uint8_t i;

        /** Check if running scheduled instance */
        if (!GET_SCHEDULE_STATUS(taskInstance)) return;
        if (argumentString == NULL) return;

        argStringLength[taskInstance] = 0;
        for (i = 0; i < MAX_ARG_STRING_LENGTH; i++) {
            argString[taskInstance][i] = argumentString[i];

            if (argumentString[i] == 0x00) break;

            argStringLength[taskInstance] += 1;
        }
        argString[taskInstance][i] = 0x00; // Make sure string ends
#endif
    }

    command void Task.cancel(uint8_t taskInstance) {
        CLEAR_SCHEDULE_STATUS(taskInstance);
        executionStatus[taskInstance] = 0;
#if (TASK_TYPE == INSTANTANEOUS_TASK_TYPE)
        argString[taskInstance][0] = 0x00;
        argStringLength[taskInstance] = 0;
#endif
    }

    command void Task.trigger(uint8_t taskInstance) {
        SET_TRIGGER_STATUS(taskInstance);

        if (checkIfReadyToRun(taskInstance)) {
            signal Task.readyToRun(taskInstance, TASK_TYPE);
        }
    }

    command void *Task.getDescription(void) {
        return ((void *) &description);
    }

    command uint16_t Task.getExecutionStatus(uint8_t taskInstance) {
        return executionStatus[taskInstance];
    }

    command uint8_t Task.isScheduled(uint8_t taskInstance) {
        return GET_SCHEDULE_STATUS(taskInstance);
    }

/************************************************************************
 * Task Parameter Input event implementations 
 ************************************************************************/

    event   void input.ready[uint8_t index](uint8_t taskInstance) {
        if (checkIfReadyToRun(taskInstance)) {
            signal Task.readyToRun(taskInstance, TASK_TYPE);
        }
    }

/************************************************************************
 * Task Parameter Input default command implementations 
 ************************************************************************/

    default command void 
    input.init[uint8_t index](void) {
    }

    default command void 
    input.setInstanceSize[uint8_t index](uint8_t taskInstance, 
                                         uint8_t size) {
    }

    default command void 
    input.receive[uint8_t index](uint8_t taskInstance,
                                 void *inputData) {
    }

    default command void 
    input.discard[uint8_t index](uint8_t taskInstance) {
    }

    default command TaskInputParameterStatus_t 
    input.status[uint8_t index](uint8_t taskInstance) {
        return INPUT_STATUS_NOT_READY;
    }

    default command void 
    input.configureStoragePolicy[uint8_t index](
                                    TaskInputParameterStoragePolicy_t policy) {
    }
}
