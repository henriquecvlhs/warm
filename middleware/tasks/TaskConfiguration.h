/**
 * @file TaskConfiguration.h
 *
 * @brief Header that carries loaded tasks' configuration.
 *
 * Configure loaded tasks here.
 *
 * @note Do not forget to add the path to loaded tasks to the Task API Makefile.
 * @note The ID defined by the enum here for a certain task must be equal to the
 *       task ID defined on the task configuration file.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __TASK_CONFIGURATION_H__
#define __TASK_CONFIGURATION_H__

#include "TaskAPI.h"

/**
 * Document loaded Task IDs here.
 */
enum TaskID {
    SENSE_TEMPERATURE_ID = 1,
    PRINT_RECEIVED_DATA_ID = 2,
    AVERAGE_INT_ID = 3,
    BLINK_LED_ID = 4,
    HELPER_TASK_EXAMPLE_ID = 4321,
    PERIODIC_TASK_TEMPLATE_ID = 1234,
    INSTANTANEOUS_TASK_TEMPLATE_ID = 5678,
};

/**
 * List loaded Task IDs here.
 */
#define LOADED_TASK_IDS_ARRAY                                                  \
    uint16_t loadedTaskIds[] = {                                               \
        SENSE_TEMPERATURE_ID,                                                  \
        PRINT_RECEIVED_DATA_ID,                                                \
        AVERAGE_INT_ID,                                                        \
        BLINK_LED_ID,                                                          \
    };


/**
 * Instantiate loaded tasks here.
 */
#define TASK_API_TASK_COMPONENTS                                               \
    components SenseTemperatureC;                                              \
    components PrintReceivedDataC;                                             \
    components AverageIntC;                                                    \
    components BlinkLedC;                                                      \

/**
 * Wire loaded tasks' interfaces here.
 */
#define TASK_API_TASK_INTERFACE_WIRINGS                                        \
    WIRE_TASK_INTERFACE(SENSE_TEMPERATURE_ID,                                  \
                        SenseTemperatureC);                                    \
    WIRE_TASK_INTERFACE(PRINT_RECEIVED_DATA_ID,                                \
                        PrintReceivedDataC);                                   \
    WIRE_TASK_INTERFACE(AVERAGE_INT_ID,                                        \
                        AverageIntC);                                          \
    WIRE_TASK_INTERFACE(BLINK_LED_ID,                                          \
                        BlinkLedC);                                            \

#endif // __TASK_CONFIGURATION_H__
