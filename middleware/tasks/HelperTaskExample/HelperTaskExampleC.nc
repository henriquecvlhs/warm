/**
 * @file HelperTaskExampleC
 *
 * This implements the configuration of a task that makes use of  
 * helpers from the Task API.
 * This is the same thing as the Complete Task Example, but implemented
 * with helpers from the Task API.
 *
 * This is an instantaneous task that find, in an array of received values,
 * the first value that is repeated at least a certain number of times and 
 * then outputs this value to the network.
 * It only takes two parameters from the network:
 *  - Repeat: the number of times the value should be repeated.
 *  - Values: the array of values where we will search the repeated value.
 * If desired, pass a string argument of a maximum of 1 character meaning
 * an unsigned integer which will act like a filter: only repeat values 
 * higher than this number will be considered.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Undefine all macros being defined here */
#undef TASK_ID
#undef TASK_TYPE
#undef NUMBER_OF_INSTANCES
#undef NUMBER_OF_INPUTS
#undef VALUES_ARRAY_SIZE
#undef VALUES_INPUT_ID
#undef REPEAT_INPUT_ID
#undef VALUES_INPUT_TYPE
#undef REPEAT_INPUT_TYPE
#undef TASK_OUTPUT_TYPE

/** Task ID */
#define TASK_ID 4321

/** The type of this task */
#define TASK_TYPE INSTANTANEOUS_TASK_TYPE

/** Maximum number of simultaneous instances of this task */
#define NUMBER_OF_INSTANCES 3

/** Number of task input parameters */
#define NUMBER_OF_INPUTS 2

/** Size for array input parameters */
#define VALUES_ARRAY_SIZE 10

/** Input Parameter IDs */
#define REPEAT_INPUT_ID 0
#define VALUES_INPUT_ID 1

/** Input Parameter types */
#define REPEAT_INPUT_TYPE uint8_t
#define VALUES_INPUT_TYPE int32_t

/** Output Data Type */
#define TASK_OUTPUT_TYPE int32_t

/**
 * @brief Configuration for Helper task Example component.
 */
configuration HelperTaskExampleC {
    provides interface Task;
}
implementation {
    components HelperTaskExampleP;
    components new TaskSingleParameterC(REPEAT_INPUT_TYPE, 
                                        NUMBER_OF_INSTANCES) as repeat;
    components new TaskArrayParameterC(VALUES_INPUT_TYPE, 
                                       NUMBER_OF_INSTANCES, 
                                       VALUES_ARRAY_SIZE) as values;

    Task = HelperTaskExampleP.Task;

    HelperTaskExampleP.input[REPEAT_INPUT_ID] -> repeat;
    HelperTaskExampleP.input[VALUES_INPUT_ID] -> values;

    HelperTaskExampleP.repeat -> repeat;
    HelperTaskExampleP.values -> values;
}
