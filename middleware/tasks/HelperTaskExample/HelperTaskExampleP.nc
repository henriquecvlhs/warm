/**
 * @file HelperTaskExampleP
 *
 * This implements the configuration of a task that makes use of  
 * helpers from the Task API.
 * This is the same thing as the Complete Task Example, but implemented
 * with helpers from the Task API.
 *
 * This is an instantaneous task that find, in an array of received values,
 * the first value that is repeated at least a certain number of times and 
 * then outputs this value to the network.
 * It only takes two parameters from the network:
 *  - Repeat: the number of times the value should be repeated.
 *  - Values: the array of values where we will search the repeated value.
 * If desired, pass a string argument of a maximum of 1 character meaning
 * an unsigned integer which will act like a filter: only repeat values 
 * higher than this number will be considered.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Helper Task Example component.
 */
module HelperTaskExampleP {
    provides interface Task;

    uses {
        interface TaskParameterInput as input[uint8_t index];
        interface TaskSingleParameterAccess<REPEAT_INPUT_TYPE> as repeat;
        interface TaskArrayParameterAccess<VALUES_INPUT_TYPE> as values;
    }
}
implementation {
   
    INSTANTANEOUS_TASK_API_HELPER

    /** 
     * @brief Task initialization routine.
     *
     * This initialize any necessary variable related to the task
     * programming logic. It is called when the task component is 
     * initialized. It is up to the programmer to implement this, but
     * at least the task description should be initialized here.
     */
    void taskInit(void) {
        /** Initialize task description */
        INITIALIZE_TASK_DESCRIPTION(SUPPORT_INT |
                                    SUPPORT_4_BYTE_LENGTH,
                                    OUTPUT_TO_NETWORK_TRUE,
                                    CONTROL_ACTUATOR_FALSE,
                                    AGGREGATE_DATA_TRUE,
                                    DATA_SINK_FALSE);

        INITILIZE_TASK_INPUT_DESCRIPTION(REPEAT_INPUT_ID, 
                                         1,                      // size
                                         SUPPORT_UINT |
                                         SUPPORT_1_BYTE_LENGTH); // uint8_t
        INITILIZE_TASK_INPUT_DESCRIPTION(VALUES_INPUT_ID, 
                                         VALUES_ARRAY_SIZE,      // size
                                         SUPPORT_INT |
                                         SUPPORT_4_BYTE_LENGTH); // int32_t
    }

    /** 
     * @brief Task execution routine.
     *
     * This executes the task's routine. It is called every time the task
     * is run. This is what the programmer should be concerned with
     * implementing.
     *
     * @param taskInstance[in] Number identifying the task instance that 
     *                         will be run.
     * @param outputData[out]  Pointer to the value computed during task
     *                         execution that should be sent to the 
     *                         network.
     *
     * @return Boolean indicating if execution was successful.
     */
    uint8_t taskRoutine(uint8_t taskInstance) {
        TASK_OUTPUT_TYPE outputData;
        uint8_t i, j = 0;
        uint8_t repeatFilter;
        uint8_t valueCount;

        /** Get values from input parameters to temporary variables, so 
            we can work with them. This should be done because every time
            a value is read from a parameter it gets cleared. This is done
            so we know that the value has been used and that we can receive
            a new one from the network. */
        uint8_t valuesSize = call values.getInstanceSize(taskInstance);
        REPEAT_INPUT_TYPE curRepeat = call repeat.get(taskInstance);
        VALUES_INPUT_TYPE curValues[VALUES_ARRAY_SIZE];
        for (i = 0; i < valuesSize; i++) {
            curValues[i] = call values.get(taskInstance, i);
        }
        
        /** Check if repeat input is not null */
        if (curRepeat == 0) {
            /** Can't count values that were not even received */
            return 0;
        }

        /** Check if repeat input satisfies argument string */
        repeatFilter = (uint8_t) argString[taskInstance][0];
        if (argStringLength[taskInstance] != 0) {
            if (curRepeat <= repeatFilter) {
                /** We won't consider a success if this execution was 
                    blocked by filter */
                return 0;
            }
        }

        /** Search for value that repeats the specified number of times */
        for (i = 0; i < valuesSize; i++) {
            valueCount = 0;
            for (j = 0; j < valuesSize; j++) {
                if (curValues[i] == curValues[j]) {
                    valueCount++;
                }
            }

            /** Check if we have found the value we wanted */
            if (valueCount >= curRepeat) {
                outputData = curValues[i];
                OUTPUT_TO_NETWORK(taskInstance, outputData);
                return 1;
            }
        }

        /** If we reach here, we clearly failed */
        return 0;
    }
}
