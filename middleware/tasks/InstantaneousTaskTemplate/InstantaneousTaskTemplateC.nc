/**
 * @file InstantaneousTaskTemplateC.nc
 *
 * @brief This is a template for the implementation of Instantaneous Tasks.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Undefine all macros being defined here */
#undef TASK_ID
#undef TASK_TYPE
#undef NUMBER_OF_INSTANCES
#undef NUMBER_OF_INPUTS
#undef ARRAY_SIZE
#undef ARRAY_INPUT_ID
#undef SINGLE_INPUT_ID
#undef ARRAY_INPUT_TYPE
#undef SINGLE_INPUT_TYPE
#undef TASK_OUTPUT_TYPE

/** Task ID */
#define INSTANTANEOUS_TASK_TEMPLATE_ID 5678
#define TASK_ID INSTANTANEOUS_TASK_TEMPLATE_ID

/** The type of this task */
#define TASK_TYPE INSTANTANEOUS_TASK_TYPE

/** Maximum number of simultaneous instances of this task */
#define NUMBER_OF_INSTANCES 3 // Change to desired value

/** Number of task input parameters */
#define NUMBER_OF_INPUTS 2 // Change to desired value

/** Input Parameter IDs */
// Define input parameter IDs here
#define SINGLE_INPUT_ID 0
#define ARRAY_INPUT_ID  1

/** Size for array input parameters */
// Define size for array input parameters here
#define ARRAY_SIZE 10

/** Input Parameter types */
// Define input parameter types here
#define SINGLE_INPUT_TYPE uint8_t
#define ARRAY_INPUT_TYPE  uint8_t

/** Output Data Type */
#define TASK_OUTPUT_TYPE uint8_t // Change to desired type

/**
 * @brief Configuration for Instantaneous Task Template component.
 */
configuration InstantaneousTaskTemplateC {
    provides interface Task;
}
implementation {
    components InstantaneousTaskTemplateP;

    // Declare input parameter components here
    components new TaskSingleParameterC(SINGLE_INPUT_TYPE,
                                        NUMBER_OF_INSTANCES) as single;

    components new TaskArrayParameterC(ARRAY_INPUT_TYPE,
                                       NUMBER_OF_INSTANCES,
                                       ARRAY_SIZE) as array;

    Task = InstantaneousTaskTemplateP.Task;

    InstantaneousTaskTemplateP.input[SINGLE_INPUT_ID] -> single;
    InstantaneousTaskTemplateP.input[ARRAY_INPUT_ID] -> array;

    InstantaneousTaskTemplateP.single -> single;
    InstantaneousTaskTemplateP.array -> array;
}
