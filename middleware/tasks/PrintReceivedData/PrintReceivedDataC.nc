/**
 * @file PrintReceivedDataC.nc
 *
 * This tasks basically prints to serial everything that arrives 
 * from network.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/** Undefine all macros being defined here */
#undef TASK_ID
#undef TASK_TYPE
#undef NUMBER_OF_INSTANCES
#undef NUMBER_OF_INPUTS
#undef PRINT_RECEIVED_DATA_ID
#undef DATA_INPUT_ID
#undef DATA_INPUT_TYPE

/** Task ID */
#define PRINT_RECEIVED_DATA_ID 2
#define TASK_ID PRINT_RECEIVED_DATA_ID

/** The type of this task */
#define TASK_TYPE INSTANTANEOUS_TASK_TYPE

/** Maximum number of simultaneous instances of this task */
#define NUMBER_OF_INSTANCES 10

/** Number of task input parameters */
#define NUMBER_OF_INPUTS 1

/** Input Parameter IDs */
#define DATA_INPUT_ID 0

/** Input Parameter types */
#define DATA_INPUT_TYPE int32_t

/**
 * @brief Configuration for Print Received Data task component.
 */
configuration PrintReceivedDataC {
    provides interface Task;
}
implementation {
    components PrintReceivedDataP;

    components new TaskSingleParameterC(DATA_INPUT_TYPE,
                                        NUMBER_OF_INSTANCES) as data;

    components PrintfC;

    Task = PrintReceivedDataP.Task;

    PrintReceivedDataP.input[DATA_INPUT_ID] -> data;

    PrintReceivedDataP.data -> data;
}
