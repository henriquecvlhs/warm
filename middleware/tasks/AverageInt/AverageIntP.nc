/**
 * @file AverageIntP.nc
 *
 * @brief This task averages data coming from the network and outputs it.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaskAPI.h"

/**
 * @brief Module that implements Average Integer task component.
 */
module AverageIntP {
    provides interface Task;

    uses {
        interface TaskParameterInput as input[uint8_t index];
        interface TaskArrayParameterAccess<TERM_INPUT_TYPE> as terms;
    }
}
implementation {
   
    INSTANTANEOUS_TASK_API_HELPER

    TASK_OUTPUT_TYPE averageOutput;

    /** 
     * @brief Task initialization routine.
     *
     * This intialize any necessary variable related to the task
     * programming logic. It is called when the task component is 
     * initialized. It is up to the programmer to implement this, but
     * at least the task description should be initialized here.
     */
    void taskInit(void) {
        /** Initialize task description */
        INITIALIZE_TASK_DESCRIPTION(SUPPORT_INT |
                                    SUPPORT_2_BYTE_LENGTH, // int16_t
                                    OUTPUT_TO_NETWORK_TRUE,
                                    CONTROL_ACTUATOR_FALSE,
                                    AGGREGATE_DATA_TRUE,
                                    DATA_SINK_FALSE);

        INITILIZE_TASK_INPUT_DESCRIPTION(TERM_INPUT_ID, 
                                         NUMBER_OF_TERMS,        // size
                                         SUPPORT_INT |
                                         SUPPORT_2_BYTE_LENGTH); // int16_t
    }

    /** 
     * @brief Task execution routine.
     *
     * This executes the task's routine. It is called every time the task
     * is run. This is what the programmer should be concerned with
     * implementing.
     *
     * @param taskInstance[in] Number identifying the task instance that 
     *                         will be run.
     * @param outputData[out]  Pointer to the value computed during task
     *                         execution that should be sent to the 
     *                         network.
     *
     * @return Boolean indicating if execution was successful.
     */
    uint8_t taskRoutine(uint8_t taskInstance) {
        uint8_t i;

        uint8_t numberOfTerms = call terms.getInstanceSize(taskInstance);

        /** Sum terms in order to compute average */
        averageOutput = 0;
        for (i = 0; i < numberOfTerms; i++) {
            averageOutput += call terms.get(taskInstance, i);
        }

        /** Compute average by dividing sum result */
        averageOutput /= numberOfTerms;

        return 1;
    }
}
