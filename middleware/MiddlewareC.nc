/**
 * @file MiddlewareC.nc
 * @brief File implementing WARM's Middleware component configuration.
 *
 * Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
 * Escola Politecnica da Universidade de Sao Paulo.
 *
 * This file is part of the WARM (WSN Application development and Resource
 * Management) frameowrk.
 * 
 * WARM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * WARM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with WARM. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Configuration for WARM's middleware component.
 */
configuration MiddlewareC {
}
implementation {
    /** Middleware components */
    components ReceiverC;
    components ProtocolProcessorC;
    components SchedulerC;
    components TaskAPIC;
    components EmitterC;
    components SDNAbstractionLayerC;

    /** Connections */

    /** Between Receiver and Scheduler */
    SchedulerC.InputScheduler -> ReceiverC.InputScheduler;

    /** Between Receiver and Task API */
    ReceiverC.TaskInput -> TaskAPIC.TaskInput;

    /** Between Receiver and Protocol Processor */
    ReceiverC.ProtocolInput -> ProtocolProcessorC.ProtocolInput;
    ProtocolProcessorC.ReceiverStatus -> ReceiverC.ReceiverStatus;

    /** Between Scheduler and Protocol Processor */
    ProtocolProcessorC.ProtocolScheduler -> SchedulerC.ProtocolScheduler;
    ProtocolProcessorC.SchedulerStatus -> SchedulerC.SchedulerStatus;

    /** Between Protocol Processor and Task API */
    ProtocolProcessorC.TaskDescription -> TaskAPIC.TaskDescription;
    ProtocolProcessorC.TaskStatus -> TaskAPIC.TaskStatus;

    /** Between Emitter and Protocol Processor */
    EmitterC.ProtocolOutput -> ProtocolProcessorC.ProtocolOutput;
    ProtocolProcessorC.EmitterStatus -> EmitterC.EmitterStatus;

    /** Between Scheduler and Emitter */
    SchedulerC.EmitterScheduler -> EmitterC.EmitterScheduler;

    /** Between Task API and Emitter */
    EmitterC.TaskOutput -> TaskAPIC.TaskOutput;

    /** Between Scheduler and Task API */
    SchedulerC.TaskScheduler -> TaskAPIC.TaskScheduler;

    /** Between SDN Abstraction Layer and Receiver */
    ReceiverC.SDNReceive -> SDNAbstractionLayerC.SDNReceive;

    /** Between SDN Abstraction Layer and Emitter */
    EmitterC.SDNSend -> SDNAbstractionLayerC.SDNSend;
}
