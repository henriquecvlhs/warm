#!/bin/bash
################################################################################
# @file tinyos_docker.sh
# @brief Script for building and running docker based development environment.
#
# Copyright (c) 2016 Departamento de Computacao e Sistemas Digitais da 
# Escola Politecnica da Universidade de Sao Paulo.
#
# This file is part of the WARM (WSN Application development and Resource
# Management) frameowrk.
# 
# WARM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# WARM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with WARM. If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
#
# This script is meant for building and running a docker based environment
# for TinyOS application programming.
#
# The script uses a Dockerfile to build a docker image which will share
# with its host a folder and its usb devices.
#
# The shared folder will be mounted as the user's home folder.
#
# Usage:
# 1) To build the docker based environment use option "build":
#    $ tinyos_docker build
# 2) To run the built environment, use option "run":
#    $ tinyos_docker run path/to/shared/folder
#
################################################################################

if ! type "docker" &> /dev/null; then
	echo "ERROR: You must first have docker installed on your system."
	exit 1
fi

MACHINE="warm"
USER="warmdev"
COMMAND="$1"
SHARED_FOLDER="$2"

if [ "$COMMAND" == "build" ]; then
	echo "Building docker image."

    # Generate tinyos repository sources list
	touch tinyprod-debian.list
	echo "deb http://tinyprod.net/repos/debian wheezy main" >> tinyprod-debian.list
	echo "deb http://tinyprod.net/repos/debian msp430-46 main" >> tinyprod-debian.list
	
	docker build -t "$MACHINE" . && rm tinyprod-debian.list && exit 0
	rm tinyprod-debian.list
	exit 1
fi

if [ "$COMMAND" == "run" ]; then
	if [ ! "$SHARED_FOLDER" ]; then
		echo "ERROR: Missing argument. Usage: tinyos_docker run path/to/shared/folder"
		exit 1
	fi

	echo "Running programming environment."

	# Create directory for shared folder, if it does not exist
	if [ ! -d "$SHARED_FOLDER" ]; then
		mkdir "$SHARED_FOLDER"
	fi

    # Get shared folder absolute path
	curdir="$(pwd)"
	cd "$SHARED_FOLDER"
    SHARED_FOLDER="$(pwd)"

	# Download tinyos repository if needed
    if [ ! $(find tinyos* -type d -prune) ]; then
		wget http://github.com/tinyos/tinyos-release/archive/tinyos-2_1_2.tar.gz
		tar xf tinyos-2_1_2.tar.gz
        mv tinyos-release-tinyos-2_1_2 tinyos
        rm tinyos-2_1_2.tar.gz
	fi

	# Place environment variables
	if [ ! -f tinyos.env ]; then
		touch tinyos.env
		echo "export TOSROOT=\"/home/$USER/tinyos\"" >> tinyos.env
		echo 'export TOSDIR="$TOSROOT/tos"' >> tinyos.env
		echo 'export CLASSPATH=$CLASSPATH:$TOSROOT/support/sdk/java' >> tinyos.env 
		echo 'export MAKERULES="$TOSROOT/support/make/Makerules"' >> tinyos.env
		echo 'export PYTHONPATH=$PYTHONPATH:$TOSROOT/support/sdk/python' >> tinyos.env
		cp ~/.bashrc ./.bashrc
		echo "source tinyos.env" >> .bashrc
	fi

	cd "$curdir"

	docker run -t -i --privileged -v /dev/bus/usb:/dev/bus/usb -v "$SHARED_FOLDER":/home/"$USER" "$MACHINE" /bin/bash && exit 0
	exit 1
fi

echo "ERROR: Missing argument. Usage: tinyos_docker build"
echo "                                tinyos_docker run path/to/shared/folder"
exit 1
